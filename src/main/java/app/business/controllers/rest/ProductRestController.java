package app.business.controllers.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import app.business.services.OrganizationService;
import app.business.services.ProductService;
import app.business.services.ProductTypeService;
import app.entities.Organization;
import app.entities.Product;
import app.entities.ProductType;

@RestController
@RequestMapping("/api")

public class ProductRestController {
	@Autowired
	ProductService productService;
	
	@Autowired
	ProductTypeService productTypeService;
	
	@Autowired
	OrganizationService organizationService;
	
	@Transactional
	@RequestMapping(value = "/gettypes", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getProductTypes(@RequestParam String abbr) {
	
		Organization organization = organizationService.getOrganizationByAbbreviation(abbr);
		List <ProductType> productTypes = productTypeService.getAllByOrganisationSortedBySequence(organization);
		JSONObject responseJsonObject = new JSONObject();
		Iterator <ProductType> iterator = productTypes.iterator();
		JSONArray array = new JSONArray();
		while(iterator.hasNext()) {
			ProductType productType = iterator.next();
			JSONObject jsonObject = new JSONObject();
			
			List<Product> products =  productService.getProductListByType(productType);
			int pages = 0;
			if (products.size() % 10 == 0)
			{
				pages = products.size()/10;
			}
			else {
				pages = (products.size()/10)+1;
			}
			try {
				jsonObject.put("productType", productType.getName());
				jsonObject.put("sequence", productType.getSequence());
				jsonObject.put("pages", pages);
				array.put(jsonObject);
			}
			catch (JSONException e) {
				e.printStackTrace();
			}
		}
		try {
			responseJsonObject.put("typeInfo", array);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();		
	}
	
	@RequestMapping(value="/paginate", method=RequestMethod.POST, produces = "application/json")
	public @ResponseBody String sendProductsByPage(@RequestBody String requestBody) {
		JSONArray response = new JSONArray();
		System.out.println("called");
		JSONObject object = null;
		
		JSONObject finalresp = new JSONObject();
		
		int pageNumber=0;
		
		String productType=null, abbr=null;
		ProductType productTypeName;
		
		try{
		     object = new JSONObject(requestBody);
		     pageNumber = object.getInt("pageNumber");
		     productType = object.getString("productType");
		     abbr = object.getString("abbr");
		}
		catch (JSONException e)
		{
			System.out.println(e.toString());
		}
		Organization organization = organizationService.getOrganizationByAbbreviation(abbr);
		//System.out.println(pageNumber+productTypeId);
		//get the productType using the productTypeId
		productTypeName = productTypeService.getProductTypeByNameAndOrg(productType, organization);
		
		List<Product> pr3 = new ArrayList<Product>();
		List<Product> pr2  = productService.getAllProductListSortedByName();
		for(int i=0; i< pr2.size();i++)
		{
		if(productTypeName == pr2.get(i).getProductType())
		{
			pr3.add(pr2.get(i));
		}
		}
		
		//assuming 10 records are fetched per page 
		List<Product> productPage = new ArrayList<Product>();
		for(int a=(pageNumber-1)*10; a<(pageNumber)*10; a++)
		{
			if(a<pr3.size()){
			
		       productPage.add(pr3.get(a));
		       try{
		    	    JSONObject temp = new JSONObject();
		    	    temp.put("productId", pr3.get(a).getProductId());
					temp.put("name", pr3.get(a).getName());
					temp.put("description", pr3.get(a).getDescription());
					temp.put("imageUrl", pr3.get(a).getImageUrl());
					temp.put("audioUrl", pr3.get(a).getAudioUrlWav());
					temp.put("unitRate", pr3.get(a).getUnitRate());
					temp.put("quantity", pr3.get(a).getQuantity());
					temp.put("status", pr3.get(a).getStatus());
					response.put(temp);					
				}
				catch(JSONException e){
				      
			}
		}
		}
	 try{
	 finalresp.put("products", response);
	 }
	 catch(JSONException e)
	 {
		 
	 }
	 return finalresp.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/cartUpdate", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String updateCart(@RequestBody String requestBody) {
		JSONObject jsonObject = null;
		JSONObject responseJsonObject = new JSONObject();
		JSONArray responseArray = new JSONArray();
		JSONArray productArray = null;
		String abbr = null;
		Organization organization = null;
		try {
			jsonObject  = new JSONObject(requestBody);
			productArray = jsonObject.getJSONArray("products");
			abbr = jsonObject.getString("abbr");
			organization = organizationService.getOrganizationByAbbreviation(abbr);
		}
		catch(JSONException e) {
			e.printStackTrace();
		}
		System.out.println(productArray.length());
		for (int i=0; i<productArray.length();++i) {
			try {
				JSONObject prod = productArray.getJSONObject(i);
				int changed =0;
				System.out.println(prod.toString());
				float price = Float.parseFloat(prod.getString("price"));
				int productId = Integer.parseInt(prod.getString("id"));
				int quantity = Integer.parseInt(prod.getString("stockquantity"));
				String imageUrl = prod.getString("imageurl");
				String audioUrl = prod.getString("audiourl");
				String productName = prod.getString("pname");
				Product product = productService.getProductById(productId);
		
				if (product.getUnitRate() == price && product.getQuantity() == quantity && product.getAudioUrl().equals(audioUrl)
						&& product.getImageUrl().equals(imageUrl) && product.getName().equals(productName)) {
					changed = 0;
				}
				else {
					changed =1;
				}
				
				JSONObject response = new JSONObject();
				response.put("id", String.valueOf(product.getProductId()));
				if (product.getAudioUrlWav() == null)
					response.put("audiourl","null");
				else
					response.put("audiourl",product.getAudioUrlWav());
				response.put("status", String.valueOf(product.getStatus()));
				response.put("changed", String.valueOf(changed));
				if (product.getImageUrl() == null)
					response.put("imageurl", "null");
				else
					response.put("imageurl", product.getImageUrl());
				response.put("name", product.getName());
				response.put("unitprice",String.valueOf(product.getUnitRate()));
				response.put("stockquantity",String.valueOf(product.getQuantity()));
				response.put("pname", product.getName());
				responseArray.put(response);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		try {
			responseJsonObject.put("stockmanagement", organization.getStockManagement());
			responseJsonObject.put("products", responseArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return responseJsonObject.toString();
	}
}
