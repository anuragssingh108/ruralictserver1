package app.business.controllers.rest;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.transaction.Transactional;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import app.business.services.FeedbackService;
import app.business.services.OrganizationMembershipService;
import app.business.services.OrganizationService;
import app.business.services.UserPhoneNumberService;
import app.business.services.UserService;
import app.entities.Feedback;
import app.entities.Organization;
import app.entities.OrganizationMembership;
import app.entities.User;
import app.util.SendContent;

@RestController
@RequestMapping("/api")
public class FeedbackRestController {
	
	@Autowired
	OrganizationService organizationService;
	
	@Autowired
	FeedbackService feedbackService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	UserPhoneNumberService userPhoneNumberService;
	
	@Autowired
	OrganizationMembershipService organizationMembershipService;
	
	@Transactional
	@RequestMapping(value = "/feedback",method = RequestMethod.POST )
	public @ResponseBody String submitFeedback(@RequestBody String requestBody) {
		JSONObject object = null;
		JSONObject responseJsonObject = new JSONObject();
		String orgabbr=null, content=null, phonenumber=null;
		try {
			object = new JSONObject(requestBody);
			orgabbr = object.getString("abbr");
			content = object.getString("content");
			phonenumber = object.getString("phonenumber");
		} catch (JSONException e) {
			e.printStackTrace();
			try {
				responseJsonObject.put("response", "Error in uploading");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		User user = userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser();
		Feedback feedback = new Feedback(content, orgabbr, user.getUserId());
		feedbackService.addFeedback(feedback);
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		List<OrganizationMembership> list =  organizationMembershipService.getOrganizationMembershipListByIsAdmin(organization, true);
		Iterator <OrganizationMembership> iterator = list.iterator();
		while(iterator.hasNext()) {
		String email = iterator.next().getUser().getEmail();
		String subject = "User Feedback";
		String body = "<html><head></head><body><b>"+user.getName()+"</b> has submitted feedback.<br /><br /><br />"+
				"<b>Timestamp: </b>" + String.valueOf(new Timestamp((new Date()).getTime())) + "<br />" +"<b>Email ID: </b>"+user.getEmail()+ "<br />" + "<b>Phonenumber: </b>"+user.getUserPhoneNumbers().get(0).getPhoneNumber()+
				"<br /><b>Feedback: </b><br /><br /><p>"+content+"</p><br /><br /><br /></body></html>";
		SendContent.sendMail(email, subject, body);
		}
		try {
			responseJsonObject.put("response","success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();

	}

}
