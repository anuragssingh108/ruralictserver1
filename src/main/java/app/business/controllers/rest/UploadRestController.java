package app.business.controllers.rest;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import app.business.services.OrganizationService;
import app.business.services.ProductService;
import app.business.services.UserPhoneNumberService;
import app.business.services.UserService;
import app.data.repositories.UserRepository;
import app.entities.Organization;
import app.entities.Product;
import app.entities.User;
import app.util.FFMPEGWrapper;
import app.util.Utils;

@RestController
@RequestMapping("/api")
public class UploadRestController {
	
	@Autowired
	ProductService productService;
	
	@Autowired
	OrganizationService organizationService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	UserPhoneNumberService userPhoneNumberService;
	
	@Autowired
	UserRepository userRepository;
	
	@Transactional
	@RequestMapping(value = "/uploadpicture", method=RequestMethod.POST, produces = "application/json")
	public @ResponseBody String handleImageUpload(HttpServletRequest request, @RequestParam(value="orgabbr") String orgabbr, @RequestParam(value="productName") String productName ) {
	Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
	Product product = productService.getProductByNameAndOrg(productName, organization);
	JSONObject responseJsonObject = new JSONObject();
	MultipartHttpServletRequest mRequest;
	mRequest = (MultipartHttpServletRequest) request;
	Iterator<String> itr = mRequest.getFileNames();
	
	//only one iteration i.e itr.next() as it has only one file
	MultipartFile mFile = mRequest.getFile(itr.next());
	String fileName = mFile.getOriginalFilename();
	Random randomint = new Random();

	File temp = Utils.saveFile("temp.jpg", Utils.getImageDir(), mFile);
	File serverFile = new File(Utils.getImageDir() +File.separator+ fileName);
	int flag=1;
	do{
		System.out.println("Product: "+product.getName());
		try 
		{
			Files.copy(temp.toPath(), serverFile.toPath());
			flag=1;
		}	
		catch (FileAlreadyExistsException e)
		{
			System.out.println("File already exist. Renaming file and trying again.");
			fileName = fileName.substring(0,fileName.length()-4);
			fileName = fileName + "_" + Integer.toString(randomint.nextInt()) + ".jpg";
			serverFile = new File(Utils.getImageDir() +File.separator+ fileName);
			flag=0;
		}
		catch (IOException e) {
			e.printStackTrace();
			flag=1;
		}
	}while(flag==0);
	
	
	String url = Utils.getImageDirURL() + fileName;
	String currentUrl = product.getImageUrl();
	if(currentUrl != null){
		//Clean up
		int x = currentUrl.lastIndexOf(File.separator);
		String location = currentUrl.substring(x+1, currentUrl.length());
		location = Utils.getImageDir()+File.separator+location;
		System.out.println("location: "+location);
		File file = new File(location);
		if(file.exists())
			file.delete();
	}
	System.out.println("file name image: "+fileName);
	product.setImageUrl(url);
	productService.addProduct(product);
	System.out.println(url);
	if (temp.exists())
		temp.delete();
	try {
		responseJsonObject.put("response", "Image upload successful");
	} catch (JSONException e) {
		e.printStackTrace();
	}
	return responseJsonObject.toString();
    
}
	@Transactional
	@RequestMapping(value = "/uploadaudio", method=RequestMethod.POST, produces = "application/json")
	public @ResponseBody String handleAudioUpload(HttpServletRequest request, @RequestParam(value="orgabbr") String orgabbr, @RequestParam(value="productName") String productName ) {
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		Product product = productService.getProductByNameAndOrg(productName, organization);
		JSONObject responseJsonObject = new JSONObject();
		MultipartHttpServletRequest mRequest;
		mRequest = (MultipartHttpServletRequest) request;
		Iterator<String> itr = mRequest.getFileNames();
		FFMPEGWrapper ffmpegWrapper = new FFMPEGWrapper(
				"/home/ruralivrs/Lokavidya/extras/ffmpeglinux/ffmpeg");
		//only one iteration i.e itr.next() as it has only one file
		MultipartFile mFile = mRequest.getFile(itr.next());
		String fileName = mFile.getOriginalFilename();	
		String name = fileName.substring(0, fileName.lastIndexOf('.'));
		String wavPath = Utils.getAudioDir() +File.separator+ name+".wav";
		Random randomint = new Random();

		File temp = Utils.saveFile("temp.mp3", Utils.getAudioDir(), mFile);
		System.out.println("product call: "+product.getName());
		File serverFile = new File(Utils.getAudioDir() +File.separator+ fileName);
		int flag=1;
		do{
			try 
			{
				Files.copy(temp.toPath(), serverFile.toPath());
				System.out.println("copied");
				flag=1;
			}	
			catch (FileAlreadyExistsException e)
			{
				System.out.println("File already exist. Renaming file and trying again.");
				fileName = fileName.substring(0,fileName.length()-4);
				fileName = fileName + "_" + Integer.toString(randomint.nextInt()) + ".mp3";
				serverFile = new File(Utils.getAudioDir() +File.separator+ fileName);
				flag=0;
			}
			catch (IOException e) {
				e.printStackTrace();
				flag=1;
			}
		}while(flag==0);
		System.out.println("file name audio:" +fileName);
		String url = Utils.getAudioDirURL() + fileName;
		String currentUrl = product.getAudioUrl();
		if(currentUrl != null){
			//Clean up
			int x = currentUrl.lastIndexOf(File.separator);
			String location = currentUrl.substring(x+1, currentUrl.length());
			location = Utils.getAudioDir()+File.separator+location;
			System.out.println("location: "+location);
			File file = new File(location);
			if(file.exists())
				file.delete();
		}		
		product.setAudioUrl(url);

		System.out.println(url);

		ffmpegWrapper.convertMp3ToWav(serverFile.getAbsolutePath(), wavPath);
	
		
		String wavUrl = Utils.getAudioDirURL()+name+".wav";
		product.setAudioUrlWav(wavUrl);
		productService.addProduct(product);
		System.out.println(".wav URL:" +wavUrl);
		System.out.println("Done converting");
		if(temp.exists())
			temp.delete();
		try {
			responseJsonObject.put("response", "Audio upload successful");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/converttowav", method=RequestMethod.POST, produces = "application/json")
	public @ResponseBody String convert() {
		
		List<Product> list = productService.getAllProductList();
		Iterator <Product> iterator = list.iterator();
		FFMPEGWrapper ffmpegWrapper = new FFMPEGWrapper(
				"/home/ruralivrs/Lokavidya/extras/ffmpeglinux/ffmpeg");
		while(iterator.hasNext()) {
			Product prod = iterator.next();
			if (prod.getAudioUrl() != null && prod.getAudioUrlWav() == null){
				String name = prod.getAudioUrl().substring(prod.getAudioUrl().lastIndexOf('/')+1, prod.getAudioUrl().lastIndexOf('.'));
				System.out.println(name);
		//		String wavName = name+".wav";
				String wavPath = Utils.getAudioDir() +File.separator+name+".wav";
				String mp3Path = Utils.getAudioDir() +File.separator+name+".mp3";
				ffmpegWrapper.convertMp3ToWav(mp3Path, wavPath);
				String wavUrl = Utils.getAudioDirURL()+name+".wav";
				prod.setAudioUrlWav(wavUrl);
				productService.addProduct(prod);
			}
		}
		return "success";
	}
	
	
	@RequestMapping(value="/profpicupload", method=RequestMethod.POST, produces = "application/json")
	public @ResponseBody String uploadProfilePicture(@RequestParam ("phonenumber") String phonenumber, HttpServletRequest request) {
		//Organization organization = organizationService.getOrganizationByAbbreviation(org);
		
		User user = userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser();
		System.out.println("User phonenumber: "+userPhoneNumberService.getUserPhoneNumber(phonenumber).getPhoneNumber());
		JSONObject responseJsonObject = new JSONObject();
		MultipartHttpServletRequest mRequest;
		mRequest = (MultipartHttpServletRequest) request;
		Iterator<String> itr = mRequest.getFileNames();
		
		//only one iteration i.e itr.next() as it has only one file
		MultipartFile mFile = mRequest.getFile(itr.next());
		String fileName = mFile.getOriginalFilename();
		Random randomint = new Random();

		File temp = Utils.saveFile("temp.jpg", Utils.getImageDir(), mFile);
		File serverFile = new File(Utils.getImageDir() +File.separator+ fileName);
		int flag=1;
		do{
			try 
		{
			Files.copy(temp.toPath(), serverFile.toPath());
			flag=1;
		}	
		catch (FileAlreadyExistsException e)
		{
			System.out.println("File already exist. Renaming file and trying again.");
			fileName = fileName.substring(0,fileName.length()-4);
			fileName = fileName + "_" + Integer.toString(randomint.nextInt()) + ".jpg";
			serverFile = new File(Utils.getImageDir() +File.separator+ fileName);
			flag=0;
		}
		catch (IOException e) {
			e.printStackTrace();
			flag=1;
		}
	}while(flag==0);
	String url = Utils.getImageDirURL() + fileName;
	String currentUrl = user.getProfilePic();
	if(currentUrl != null){
		//Clean up
		int x = currentUrl.lastIndexOf(File.separator);
		String location = currentUrl.substring(x+1, currentUrl.length());
		location = Utils.getImageDir()+File.separator+location;
		System.out.println("location: "+location);
		File file = new File(location);
		if(file.exists())
			file.delete();
	}
	System.out.println("file name image: "+fileName);
	System.out.println(url);
	user.setProfilePic(url);
	userRepository.save(user);
	System.out.println("Saved.. ");
	if (temp.exists())
		temp.delete();
	try {
		responseJsonObject.put("response", "Image upload successful");
		responseJsonObject.put("url", url);
	} catch (JSONException e) {
		e.printStackTrace();
	}
	return responseJsonObject.toString();
	
}
	
}
