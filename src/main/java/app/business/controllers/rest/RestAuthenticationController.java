package app.business.controllers.rest;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.plivo.helper.api.client.RestAPI;
import com.plivo.helper.api.response.message.MessageResponse;
import com.plivo.helper.exception.PlivoException;

import app.business.services.EnquiryService;
import app.business.services.GcmTokensService;
import app.business.services.GroupMembershipService;
import app.business.services.OrganizationMembershipService;
import app.business.services.OrganizationService;
import app.business.services.ProductService;
import app.business.services.ProductTypeService;
import app.business.services.ReferralService;
import app.business.services.SmsApiKeysService;
import app.business.services.UserPhoneNumberService;
import app.business.services.UserService;
import app.business.services.message.MessageService;
import app.data.repositories.GroupMembershipRepository;
import app.data.repositories.GroupRepository;
import app.data.repositories.OrganizationMembershipRepository;
import app.data.repositories.OrganizationRepository;
import app.data.repositories.UserPhoneNumberRepository;
import app.data.repositories.UserRepository;
import app.data.repositories.VersionCheckRepository;
import app.entities.Enquiry;
import app.entities.GcmTokens;
import app.entities.Group;
import app.entities.GroupMembership;
import app.entities.Organization;
import app.entities.OrganizationMembership;
import app.entities.Product;
import app.entities.ProductType;
import app.entities.Referral;
import app.entities.SmsApiKeys;
import app.entities.User;
import app.entities.UserPhoneNumber;
import app.entities.VersionCheck;
import app.entities.message.Message;
import app.util.GcmRequest;
import app.util.SendContent;
import app.util.SendMail;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@RestController
@RequestMapping("/app")
public class RestAuthenticationController {

	@Autowired
	UserPhoneNumberRepository userPhoneNumberRepository;

	@Autowired
	OrganizationRepository organizationRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	OrganizationMembershipRepository organizationMemberRepository;

	@Autowired
	GroupMembershipRepository groupMembershipRepository;

	@Autowired
	GroupRepository groupRepository;

	@Autowired
	GroupMembershipService groupMembershipService;

	@Autowired
	UserRepository userRepository;

	@Autowired
	OrganizationService organizationService;

	@Autowired
	OrganizationMembershipService organizationMembershipService;

	@Autowired
	UserPhoneNumberService userPhoneNumberService;

	@Autowired
	VersionCheckRepository versionCheckRepository;

	@Autowired
	GcmTokensService gcmTokensService;

	@Autowired
	UserService userService;

	@Autowired
	MessageService messageService;

	@Autowired
	SmsApiKeysService smsApiKeysService;

	@Autowired
	ReferralService referralService;

	@Autowired
	ProductTypeService productTypeService;

	@Autowired
	EnquiryService enquiryService;

	@Autowired
	ProductService productService;

	private final String lokacartId = "lokacart@cse.iitb.ac.in";

	public HashMap<String, Integer> dashBoardLocal(String orgabbr) throws ParseException {

		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		Group g = organizationService.getParentGroup(organization);
		List<Message> messageapppro = messageService.getMessageListByOrderStatus(g, "binary", "processed");
		List<Message> messageappnew = messageService.getMessageListByOrderStatus(g, "binary", "saved");
		List<Message> messageappcan = messageService.getMessageListByOrderStatus(g, "binary", "cancelled");
		HashMap<String, Integer> dashmap = new HashMap<String, Integer>();
		dashmap.put("saved", messageappnew.size());
		dashmap.put("processed", messageapppro.size());
		dashmap.put("cancelled", messageappcan.size());

		List<OrganizationMembership> membershipListpending = organizationMembershipService
				.getOrganizationMembershipListByStatus(organization, 0);
		List<OrganizationMembership> membershipListapproved = organizationMembershipService
				.getOrganizationMembershipListByStatus(organization, 1);
		dashmap.put("totalUsers", membershipListapproved.size());
		dashmap.put("pendingUsers", membershipListpending.size());
		int todayUsers = 0;
		for (OrganizationMembership membership : membershipListpending) {

			User user = membership.getUser();

			try {
				Timestamp time = user.getTime();

				Calendar cal = Calendar.getInstance();
				cal.clear(Calendar.HOUR_OF_DAY);
				cal.clear(Calendar.AM_PM);
				cal.clear(Calendar.MINUTE);
				cal.clear(Calendar.SECOND);
				cal.clear(Calendar.MILLISECOND);
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				java.util.Date dateWithoutTime = sdf.parse(sdf.format(new java.util.Date()));
				if (time.after(dateWithoutTime)) {
					todayUsers = todayUsers + 1;
				}
			} catch (NullPointerException | ParseException e) {
				System.out.println("User name not having his timestamp recorded is: " + user.getName()
						+ " having userID: " + user.getUserId());
			}
		}
		dashmap.put("newUsersToday", todayUsers);
		return dashmap;
	}

	public List<String> getTargetDevices(Organization organization) {
		List<OrganizationMembership> organizationMembership = organizationMembershipService
				.getOrganizationMembershipListByIsAdmin(organization, true);
		List<String> phoneNumbers = new ArrayList<String>();
		Iterator<OrganizationMembership> membershipIterator = organizationMembership.iterator();
		while (membershipIterator.hasNext()) {
			OrganizationMembership membership = membershipIterator.next();
			User user = membership.getUser();
			phoneNumbers.add(userPhoneNumberService.getUserPrimaryPhoneNumber(user).getPhoneNumber());
		}
		Iterator<String> iterator = phoneNumbers.iterator();
		List<String> androidTargets = new ArrayList<String>();
		while (iterator.hasNext()) {
			String number = iterator.next();
			try {
				List<GcmTokens> gcmTokens = gcmTokensService.getListByPhoneNumber(number);
				Iterator<GcmTokens> iter = gcmTokens.iterator();
				while (iter.hasNext()) {

					androidTargets.add(iter.next().getToken());
				}
			} catch (Exception e) {
				System.out.println("no token for number: " + number);
			}
		}
		return androidTargets;
	}

	@Transactional
	@RequestMapping(value = "/convert", method = RequestMethod.GET)
	public @ResponseBody String getOrgName(@RequestParam(value = "org") String org) {
		JSONObject responseJsonObject = new JSONObject();
		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		try {
			responseJsonObject.put("response", organization.getName());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}

	@Transactional
	@RequestMapping(value = "/resetlink", method = RequestMethod.GET)

	public void sendResetLink(@RequestParam(value = "username") String username,
			@RequestParam(value = "phonenumber") String phonenumber) {

	}
	
	@Transactional
	@RequestMapping(value = "/versioncheckadmin", method = RequestMethod.GET)
	public String checkVersionAdmin(@RequestParam(value = "version") String version) {
		int id = 2;
		float appVersion = Float.parseFloat(version);
		JSONObject responseJsonObject = new JSONObject();
		VersionCheck currentVersion = versionCheckRepository.findOne(id);
		float curVersion = currentVersion.getVersion();
		int curMandatory = currentVersion.getMandatory();
		if (curVersion <= appVersion) {
			try {
				responseJsonObject.put("response", "0");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else if ((curVersion > appVersion) && (curMandatory == 0)) {
			try {
				responseJsonObject.put("response", "1");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else if ((curVersion > appVersion) && (curMandatory == 1)) {
			try {
				responseJsonObject.put("response", "2");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return responseJsonObject.toString();
	}

	
	@Transactional
	@RequestMapping(value = "/versionchecknew", method = RequestMethod.POST)
	public String checkVersionNew(@RequestBody String requestBody) {
		int id = 1;
		JSONObject object = null;
		VersionCheck currentVersion = versionCheckRepository.findOne(id);
		JSONObject responseJsonObject = new JSONObject();
		String phonenumber = null;
		float curVersion = currentVersion.getVersion();
		int curMandatory = currentVersion.getMandatory();
		float appVersion = 0;
		int flag = 0;
		try {
			object = new JSONObject(requestBody);
			appVersion = Float.parseFloat(object.getString("version"));
			try {
				phonenumber = object.getString("phonenumber");
				responseJsonObject.put("phonenumber", phonenumber);
				flag = 1; // the phone number case
			} catch (Exception e) {
				// do nothing, flag =0
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		if (curVersion <= appVersion) {
			try {
				responseJsonObject.put("update", "0");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else if ((curVersion > appVersion) && (curMandatory == 0)) {
			try {
				responseJsonObject.put("update", "1");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else if ((curVersion > appVersion) && (curMandatory == 1)) {
			try {
				responseJsonObject.put("update", "2");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		if (flag == 1) {
			User user = userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser();
			int details = 0;
			List<OrganizationMembership> organizationMembershipList = user.getOrganizationMemberships();
			Iterator<OrganizationMembership> membershipIterator = organizationMembershipList.iterator();
			JSONArray memberArray = new JSONArray();
			while (membershipIterator.hasNext()) {
				OrganizationMembership membership = membershipIterator.next();
				JSONObject jsonObject = new JSONObject();
				try {
					jsonObject.put("abbr", membership.getOrganization().getAbbreviation());
					jsonObject.put("organization", membership.getOrganization().getName());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				memberArray.put(jsonObject);
			}
			try {
				responseJsonObject.put("memberships", memberArray);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			if (user.getEmail() == null || user.getLastname() == null || user.getName() == null
					|| user.getAddress() == null || user.getName() == null || user.getPincode() == null) {
				details = 1;
			}
			try {
				if (user.getEmail() == null)
					responseJsonObject.put("email", "null");
				else
					responseJsonObject.put("email", user.getEmail());

				if (user.getLastname() == null)
					responseJsonObject.put("lastname", "null");
				else
					responseJsonObject.put("lastname", user.getLastname());

				if (user.getName() == null)
					responseJsonObject.put("name", "null");
				else
					responseJsonObject.put("name", user.getName());

				if (user.getAddress() == null)
					responseJsonObject.put("address", "null");
				else
					responseJsonObject.put("address", user.getAddress());

				if (user.getPincode() == null)
					responseJsonObject.put("pincode", "null");
				else
					responseJsonObject.put("pincode", user.getPincode());
				
				if (user.getProfilePic() == null)
					responseJsonObject.put("profilepic", "null");
				else
					responseJsonObject.put("profilepic", user.getProfilePic());
				responseJsonObject.put("flag", Integer.toString(details));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return responseJsonObject.toString();
	}

	@Transactional
	@RequestMapping(value = "/versioncheck", method = RequestMethod.GET)
	public String checkVersion(@RequestParam(value = "version") String version) {
		int id = 1;
		float appVersion = Float.parseFloat(version);
		JSONObject responseJsonObject = new JSONObject();
		VersionCheck currentVersion = versionCheckRepository.findOne(id);
		float curVersion = currentVersion.getVersion();
		int curMandatory = currentVersion.getMandatory();
		if (curVersion <= appVersion) {
			try {
				responseJsonObject.put("response", "0");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else if ((curVersion > appVersion) && (curMandatory == 0)) {
			try {
				responseJsonObject.put("response", "1");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else if ((curVersion > appVersion) && (curMandatory == 1)) {
			try {
				responseJsonObject.put("response", "2");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return responseJsonObject.toString();
	}

	// Code for account recovery
	@Transactional
	@RequestMapping(value = "/recoverotp", method = RequestMethod.POST)
	public @ResponseBody String otpRecovery(@RequestBody String requestBody) {
		JSONObject jsonObject = null;
		JSONObject responseJsonObject = new JSONObject();
		String phonenumber = null;
		User user = null;
		try {
			jsonObject = new JSONObject(requestBody);
			phonenumber = jsonObject.getString("phonenumber");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		if (userPhoneNumberService.findPreExistingPhoneNumber(phonenumber) == false) {
			// Send OTP
			user = userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser();
			OrganizationMembership membership =  organizationMembershipService.getOrganizationMembershipByUserAndIsAdmin(user, true);
			if (membership != null)
			{
				try {
					responseJsonObject.put("otp", "null");
					responseJsonObject.put("text", "User is an administrator. Please use a consumer account.");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return responseJsonObject.toString();
			}
			Organization billingOrganization = null;
			try {
				billingOrganization = organizationMembershipService.getLatestOrganizationMembership(user)
						.getOrganization();
			} catch (Exception e) {
				billingOrganization = organizationService.getOrganizationByAbbreviation("Test2");
			}
			if (billingOrganization == null) {
				billingOrganization = organizationService.getOrganizationByAbbreviation("Test2");
			}
			SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(billingOrganization);
			String authId = smsApiKeys.getAuthId();
			String authToken = smsApiKeys.getAuthToken();
			String sourceNumber = smsApiKeys.getSourceNumber();
			RestAPI api = new RestAPI(authId, authToken, "v1");
			String otp = randomString(4);
			LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
			parameters.put("src", sourceNumber);
			parameters.put("dst", phonenumber);
			parameters.put("text", "Hello from Lokacart! \nYour OTP is: " + otp);
			parameters.put("method", "GET");
			try {
				MessageResponse msgResponse = api.sendMessage(parameters);
				System.out.println(msgResponse);
				System.out.println("Api ID : " + msgResponse.apiId);
				System.out.println("Message : " + msgResponse.message);
				responseJsonObject.put("otp", otp);
				responseJsonObject.put("text", "Otp has been sent to your phone");

				if (msgResponse.serverCode == 202) {
					System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());
				} else {
					System.out.println(msgResponse.error);
				}
			} catch (PlivoException e) {
				System.out.println(e.getLocalizedMessage());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		} else {
			try {
				responseJsonObject.put("otp", "null");
				responseJsonObject.put("text", "User does not exist. Please get a referral");
				return responseJsonObject.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return null;
		}

	}

	// Code to recover details
	@Transactional
	@RequestMapping(value = "/recoverdetails", method = RequestMethod.POST)
	public @ResponseBody String detailsRecovery(@RequestBody String requestBody) {

		JSONObject jsonObject = null;
		JSONObject responseJsonObject = new JSONObject();
		String phonenumber = null;
		float appVersion = 0;
		int id = 1, details = 0;
		User user = null;

		try {
			jsonObject = new JSONObject(requestBody);
			phonenumber = jsonObject.getString("phonenumber");
			appVersion = Float.parseFloat(jsonObject.getString("version"));
			VersionCheck currentVersion = versionCheckRepository.findOne(id);
			float curVersion = currentVersion.getVersion();
			int curMandatory = currentVersion.getMandatory();
			if (curVersion <= appVersion) {
				try {
					responseJsonObject.put("update", "0");
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} else if ((curVersion > appVersion) && (curMandatory == 0)) {
				try {
					responseJsonObject.put("update", "1");
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} else if ((curVersion > appVersion) && (curMandatory == 1)) {
				try {
					responseJsonObject.put("update", "2");
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		user = userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser();
		if (user.getEmail() == null || user.getLastname() == null || user.getName() == null || user.getAddress() == null
				|| user.getName() == null || user.getPincode() == null) {
			details = 1;
		}
		try {
			responseJsonObject.put("response", "success");
			responseJsonObject.put("flag", Integer.toString(details));
			if (user.getName() != null)
				responseJsonObject.put("name", user.getName());
			else
				responseJsonObject.put("name", "null");
			responseJsonObject.put("email", user.getEmail());
			if (user.getAddress() != null)
				responseJsonObject.put("address", user.getAddress());
			else
				responseJsonObject.put("address", "null");

			if (user.getLastname() != null)
				responseJsonObject.put("lastname", user.getLastname());
			else
				responseJsonObject.put("lastname", "null");

			if (user.getPincode() != null)
				responseJsonObject.put("pincode", user.getPincode());
			else
				responseJsonObject.put("pincode", "null");
			
			if (user.getProfilePic() == null)
				responseJsonObject.put("profilepic", "null");
			else
				responseJsonObject.put("profilepic", user.getProfilePic());
			
			responseJsonObject.put("phonenumber", user.getUserPhoneNumbers().get(0).getPhoneNumber());
			responseJsonObject.put("email", user.getEmail());
			responseJsonObject.put("token", user.getPassToken());

			List<OrganizationMembership> list = user.getOrganizationMemberships();
			Iterator<OrganizationMembership> iterator = list.iterator();
			System.out.println(list.size());
			JSONArray array = new JSONArray();
			while (iterator.hasNext()) {
				OrganizationMembership membership = iterator.next();
				JSONObject object = new JSONObject();
				object.put("organization", membership.getOrganization().getName());
				object.put("abbr", membership.getOrganization().getAbbreviation());
				array.put(object);
			}
			responseJsonObject.put("orglist", array);
			responseJsonObject.put("version", Float.toString(versionCheckRepository.findOne(1).getVersion()));
			responseJsonObject.put("mandatory", Integer.toString(versionCheckRepository.findOne(1).getMandatory()));
			// responseJsonObject.put(", arg1)
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}

	@Transactional
	@RequestMapping(value = "/numberverify", method = RequestMethod.POST)
	public String numberVerify(@RequestBody String requestBody) throws Exception {
		JSONObject responseJsonObject = new JSONObject();
		JSONObject jsonObject = null;
		String phonenumberOld = null, phonenumberNew = null;
		String orgabbr = null;
		try {
			jsonObject = new JSONObject(requestBody);
			phonenumberOld = jsonObject.getString("phonenumber_old");
			phonenumberNew = jsonObject.getString("phonenumber_new");
			// orgabbr = jsonObject.getString("orgabbr");
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Organization organizationBilled =
		// organizationService.getOrganizationByAbbreviation(orgabbr);
		UserPhoneNumber currentUser = userPhoneNumberService.getUserPhoneNumber(phonenumberOld);
		Organization billingOrganization = organizationMembershipService
				.getLatestOrganizationMembership(currentUser.getUser()).getOrganization();
		// UserPhoneNumber userPhoneNumber =
		// userPhoneNumberRepository.findByPhoneNumber(phonenumberNew);
		if (userRepository.findByuserPhoneNumbers_phoneNumber(phonenumberNew) != null) {
			responseJsonObject.put("text", "Phone number entered already exists.");
			responseJsonObject.put("otp", "null");
			return responseJsonObject.toString();
		}
		if (phonenumberNew != null) {
			/* TODO Move to Util class. No time */
			SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(billingOrganization);
			String authId = smsApiKeys.getAuthId();
			String authToken = smsApiKeys.getAuthToken();
			String sourceNumber = smsApiKeys.getSourceNumber();
			RestAPI api = new RestAPI(authId, authToken, "v1");
			String otp = randomString(4);
			LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
			parameters.put("src", sourceNumber);
			parameters.put("dst", phonenumberNew);
			parameters.put("text", "Hello from Lokacart! \nYour OTP is: " + otp);
			parameters.put("method", "GET");
			try {
				MessageResponse msgResponse = api.sendMessage(parameters);
				System.out.println(msgResponse);
				System.out.println("Api ID : " + msgResponse.apiId);
				System.out.println("Message : " + msgResponse.message);
				responseJsonObject.put("otp", otp);
				responseJsonObject.put("text", "Otp has been sent to your phone");

				if (msgResponse.serverCode == 202) {
					System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());
				} else {
					System.out.println(msgResponse.error);
				}
			} catch (PlivoException e) {
				System.out.println(e.getLocalizedMessage());
			}
			return responseJsonObject.toString();
		} else {
			try {
				responseJsonObject.put("otp", "null");
				responseJsonObject.put("text", "null");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}

	}

	@Transactional
	@RequestMapping(value = "/otp", method = RequestMethod.POST)
	public String otp(@RequestBody String requestBody) throws Exception {
		JSONObject responseJsonObject = new JSONObject();
		HashMap<String, String> response = new HashMap<String, String>();
		JSONObject jsonObject = null;
		String phonenumber = null;
		String orgabbr = null;
		String email = null;
		try {
			jsonObject = new JSONObject(requestBody);
			phonenumber = jsonObject.getString("phonenumber");
			email = jsonObject.getString("email");
			orgabbr = jsonObject.getString("orgabbr");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		Organization organizationBilled = organizationService.getOrganizationByAbbreviation(orgabbr);
		UserPhoneNumber userPhoneNumber = userPhoneNumberRepository.findByPhoneNumber(phonenumber);
		if (userRepository.findByEmail(email).size() != 0) {
			responseJsonObject.put("text", "Email entered already exists.");
			responseJsonObject.put("otp", "null");
			return responseJsonObject.toString();
		}
		if (userRepository.findByuserPhoneNumbers_phoneNumber(phonenumber) != null) {
			responseJsonObject.put("text", "Phone number entered already exists.");
			responseJsonObject.put("otp", "null");
			return responseJsonObject.toString();
		}
		List<Organization> orglist = organizationRepository.findAll();
		JSONArray orgArray = new JSONArray();
		for (Organization organization : orglist) {
			try {
				if (!organization.getName().equals("Testing") && !organization.getName().equals("TestOrg1")
						&& !organization.getName().equals("Testorg3")) {
					JSONObject org = new JSONObject();
					org.put("name", organization.getName());
					org.put("org_id", organization.getOrganizationId());
					org.put("abbr", organization.getAbbreviation());
					org.put("ph_no", organization.getIvrNumber());
					orgArray.put(org);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		/*
		 * if(userPhoneNumber==null) { String otp=randomString(4); int status=0;
		 * if((status=SendMail.sendMail(email, "Cottage Industry App OTP" ,
		 * "Your OTP is: " + otp ))==1) response.put("text",
		 * "Otp has been sent to your email"); //IVRUtils.sendSMS(phonenumber,
		 * otp, null , null); response.put("otp",otp); try {
		 * responseJsonObject.put("otp", otp);
		 * responseJsonObject.put("organizations",orgArray); if(status==1)
		 * responseJsonObject.put("text", "Otp has been sent to your email"); }
		 * catch (JSONException e) { e.printStackTrace(); } return
		 * responseJsonObject.toString(); }
		 */
		if (phonenumber != null) {
			/* TODO Move to Util class. No time */
			SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(organizationBilled);
			// String authId = "MANZBINJLKOTI0NWVHZD";
			String authId = smsApiKeys.getAuthId();
			// String authToken = "OWU2NDYyOTEzOGJkODBhZWU1ZjI0MjhlZDgxMWMw";
			String authToken = smsApiKeys.getAuthToken();
			String sourceNumber = smsApiKeys.getSourceNumber();
			RestAPI api = new RestAPI(authId, authToken, "v1");
			String otp = randomString(4);
			LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
			// parameters.put("src", "919322727415");
			parameters.put("src", sourceNumber);
			parameters.put("dst", phonenumber);
			parameters.put("text", "Hello from Lokacart! \nYour OTP is: " + otp);
			parameters.put("method", "GET");
			try {
				MessageResponse msgResponse = api.sendMessage(parameters);
				System.out.println(msgResponse);
				System.out.println("Api ID : " + msgResponse.apiId);
				System.out.println("Message : " + msgResponse.message);
				responseJsonObject.put("otp", otp);
				responseJsonObject.put("organizations", orgArray);
				responseJsonObject.put("text", "Otp has been sent to your phone");

				if (msgResponse.serverCode == 202) {
					System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());
				} else {
					System.out.println(msgResponse.error);
				}
			} catch (PlivoException e) {
				System.out.println(e.getLocalizedMessage());
			}
			return responseJsonObject.toString();

		} else if (phonenumber == null && email != null) {
			String otp = randomString(4);
			int status = 0;
			if ((status = SendMail.sendMail(email, "Lokacart: OTO", "Your OTP is: " + otp)) == 1)
				response.put("text", "Otp has been sent to your email");
			// IVRUtils.sendSMS(phonenumber, otp, null , null);
			response.put("otp", otp);
			try {
				responseJsonObject.put("otp", otp);
				responseJsonObject.put("organizations", orgArray);
				if (status == 1)
					responseJsonObject.put("text", "Otp has been sent to your email");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		} else {
			response.put("otp", null);
			try {
				responseJsonObject.put("otp", "null");
				responseJsonObject.put("text", "null");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}

	}

	@RequestMapping(value = "/register", method = RequestMethod.POST, produces = "application/json")
	@Transactional
	public HashMap<String, String> registration(@RequestBody String requestBody) {
		/*
		 * Add organization membership, user, organization
		 */
		HashMap<String, String> response = new HashMap<String, String>();
		JSONObject jsonObject = null;
		String phonenumber = null;
		String address = null;
		String password = null;
		String name = null;
		String email = null;
		String pincode = null, lastname = null;
		JSONArray orgListJsonArray = null;
		User user = new User();
		UserPhoneNumber userPhoneNumber = new UserPhoneNumber();
		List<UserPhoneNumber> userPhoneNumbers = new ArrayList<UserPhoneNumber>();
		// List<Organization> orgList= new ArrayList<Organization>();
		try {
			jsonObject = new JSONObject(requestBody);
			phonenumber = jsonObject.getString("phonenumber");
			address = jsonObject.getString("address");
			password = jsonObject.getString("password");
			name = jsonObject.getString("name");
			email = jsonObject.getString("email");
			try {
				pincode = jsonObject.getString("pincode");
				lastname = jsonObject.getString("lastname");
			} catch (Exception e) {
				System.out.println("No pincode or lastname");
			}
			// orgListJsonArray=jsonObject.getJSONArray("orglist");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		/*
		 * Check if exists
		 */
		User usercheck = userRepository.findByuserPhoneNumbers_phoneNumber(phonenumber);
		if (usercheck != null) {
			response.put("Status", "Failure");
			response.put("Error", "Number Exists");
			return response;
		}
		List<User> userCheckList = userRepository.findByEmail(email);
		if (userCheckList.size() > 0) {
			response.put("Status", "Failure");
			response.put("Error", "Email Exists");
			return response;
		}
		/*
		 * for (int i=0;i<orgListJsonArray.length();i++) { try { JSONObject org
		 * = orgListJsonArray.getJSONObject(i); int org_id=org.getInt("org_id");
		 * //Adding organization Organization organization=
		 * organizationRepository.findOne(org_id); if(organization==null) {
		 * response.put("Status", "Failure"); response.put("Error",
		 * "Organization with Id "+org_id+" does not exists"); return response;
		 * } } catch (JSONException e) { e.printStackTrace(); } }
		 */

		// Organization organization=new Organization();
		user.setAddress(address);
		user.setCallLocale("en");
		user.setEmail(email);
		user.setSha256Password(passwordEncoder.encode(password));
		user.setName(name);
		if (pincode != null)
			user.setPincode(pincode);
		if (lastname != null)
			user.setLastname(lastname);
		// java.util.Date date= new java.util.Date();
		// Timestamp currentTimestamp= new Timestamp(date.getTime());
		// user.setTime(currentTimestamp);
		user = userRepository.save(user);
		/*
		 * List<OrganizationMembership> organizationMemberships= new
		 * ArrayList<OrganizationMembership>(); List<GroupMembership>
		 * groupMemberships= new ArrayList<GroupMembership>(); for (int
		 * i=0;i<orgListJsonArray.length();i++) { try { JSONObject org =
		 * orgListJsonArray.getJSONObject(i); int org_id=org.getInt("org_id");
		 * //Adding organization organization=
		 * organizationRepository.findOne(org_id); if(organization==null) {
		 * response.put("Status", "Failure"); response.put("Error",
		 * "Organization with Id "+org_id+" does not exists"); return response;
		 * } orgList.add(organization); OrganizationMembership
		 * organizationMembership = new OrganizationMembership();
		 * organizationMembership.setOrganization(organization);
		 * organizationMembership.setUser(user);
		 * organizationMembership.setIsAdmin(false);
		 * organizationMembership.setIsPublisher(false);
		 * organizationMembership.setStatus(0);
		 * organizationMembership=organizationMemberRepository.save(
		 * organizationMembership);
		 * organizationMemberships.add(organizationMembership); } catch
		 * (JSONException e) { e.printStackTrace(); } }
		 */
		// user.setGroupMemberships(groupMemberships);
		// user.setOrganizationMemberships(organizationMemberships);
		user.setTextbroadcastlimit(0);
		user.setVoicebroadcastlimit(0);
		user = userRepository.save(user);
		System.out.println("User phone number is being saved");
		phonenumber = "91" + phonenumber;
		userPhoneNumber.setPhoneNumber(phonenumber);
		userPhoneNumber.setPrimary(true);
		userPhoneNumber.setUser(user);
		userPhoneNumber = userPhoneNumberRepository.save(userPhoneNumber);
		userPhoneNumbers.add(userPhoneNumber);
		user.setUserPhoneNumbers(userPhoneNumbers);
		System.out.println("User phone number is  saved");
		userRepository.save(user);
		// for(Organization org: orgList)
		// {
		// groupMembershipService.addParentGroupMembership(org, user);
		// }
		response.put("Status", "Success");
		return response;
	}

	@RequestMapping(value = "/orgsave", method = RequestMethod.POST, produces = "application/json")
	@Transactional
	public HashMap<String, String> orgselection(@RequestBody String requestBody) {
		/*
		 * Add organization membership, user, organization
		 */

		/* TODO may need to add the join_count */
		HashMap<String, String> response = new HashMap<String, String>();
		JSONObject jsonObject = null;
		String phonenumber = null;
		String email = null;
		JSONArray orgListJsonArray = null;

		List<Organization> orgList = new ArrayList<Organization>();
		try {
			jsonObject = new JSONObject(requestBody);
			phonenumber = "91" + jsonObject.getString("phonenumber");
			// address=jsonObject.getString("address");
			// password=jsonObject.getString("password");
			// name=jsonObject.getString("name");
			// email=jsonObject.getString("email");
			orgListJsonArray = jsonObject.getJSONArray("orglist");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		/*
		 * Check if exists
		 */
		System.out.println("User phone no is: " + phonenumber);
		User user = userRepository.findByuserPhoneNumbers_phoneNumber(phonenumber);
		if (user == null) {
			response.put("Status", "Failure");
			response.put("Error", "Number doesn't Exists");
			return response;
		}
		List<User> userCheckList = userRepository.findByEmail(email);
		if (userCheckList.size() == 0) {
			response.put("Status", "Failure");
			response.put("Error", "Email doesn't exist");
			return response;
		}

		Organization organization = new Organization();
		// user.setAddress(address);
		// user.setCallLocale("en");
		// user.setEmail(email);
		// user.setSha256Password(passwordEncoder.encode(password));
		// user.setName(name);
		java.util.Date date = new java.util.Date();
		Timestamp currentTimestamp = new Timestamp(date.getTime());
		user.setTime(currentTimestamp);
		user = userRepository.save(user);
		List<OrganizationMembership> organizationMemberships = new ArrayList<OrganizationMembership>();
		List<GroupMembership> groupMemberships = new ArrayList<GroupMembership>();
		for (int i = 0; i < orgListJsonArray.length(); i++) {
			try {
				JSONObject org = orgListJsonArray.getJSONObject(i);
				int org_id = org.getInt("org_id");
				// Adding organization
				organization = organizationRepository.findOne(org_id);
				if (organization == null) {
					response.put("Status", "Failure");
					response.put("Error", "Organization with Id " + org_id + " does not exists");
					return response;
				}
				orgList.add(organization);
				OrganizationMembership organizationMembership = new OrganizationMembership();
				organizationMembership.setOrganization(organization);
				organizationMembership.setUser(user);
				organizationMembership.setIsAdmin(false);
				organizationMembership.setIsPublisher(false);
				if (organization.getAutoApprove() == true) {
					organizationMembership.setStatus(1);
				} else {
					organizationMembership.setStatus(0);
				}
				organizationMembership = organizationMemberRepository.save(organizationMembership);
				organizationMemberships.add(organizationMembership);

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		// user.setGroupMemberships(groupMemberships);
		user.setOrganizationMemberships(organizationMemberships);
		// user.setTextbroadcastlimit(0);
		// user.setVoicebroadcastlimit(0);
		// user=userRepository.save(user);
		// phonenumber="91"+phonenumber;
		// userPhoneNumber.setPhoneNumber(phonenumber);
		// userPhoneNumber.setPrimary(true);
		// userPhoneNumber.setUser(user);
		// userPhoneNumber=userPhoneNumberRepository.save(userPhoneNumber);
		// userPhoneNumbers.add(userPhoneNumber);
		// user.setUserPhoneNumbers(userPhoneNumbers);
		userRepository.save(user);
		for (Organization org : orgList) {
			groupMembershipService.addParentGroupMembership(org, user);
		}
		for (int i = 0; i < orgListJsonArray.length(); i++) {
			try {
				JSONObject org = orgListJsonArray.getJSONObject(i);
				int org_id = org.getInt("org_id");
				// Adding organization
				Organization organizationNew = organizationRepository.findOne(org_id);
				if (organizationNew == null) {
					response.put("Status", "Failure");
					response.put("Error", "Organization with Id " + org_id + " does not exists");
					return response;
				}
				String orgabbr = organizationNew.getAbbreviation();
				List<String> androidTargets = getTargetDevices(organizationNew);
				if (androidTargets.size() > 0) {
					GcmRequest gcmRequest = new GcmRequest();
					if (organizationNew.getAutoApprove() == false) {
						gcmRequest.broadcast(user.getName() + " would like to be a member", "New Member Request",
								androidTargets, 1, user.getUserId());
					}
					HashMap<String, Integer> dashData = null;
					try {
						dashData = dashBoardLocal(orgabbr);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					gcmRequest.broadcast(androidTargets, orgabbr, dashData);
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		response.put("Status", "Success");
		return response;
	}

	@Transactional
	@RequestMapping(value = "/forgotpassword", method = RequestMethod.POST)
	public String forgotpassword(@RequestBody String requestBody) {
		JSONObject responseJsonObject = new JSONObject();
		HashMap<String, String> response = new HashMap<String, String>();
		JSONObject jsonObject = null;
		String phonenumber = null;
		try {
			jsonObject = new JSONObject(requestBody);
			phonenumber = jsonObject.getString("phonenumber");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		UserPhoneNumber userPhoneNumber = userPhoneNumberRepository.findByPhoneNumber(phonenumber);
		if (userPhoneNumber == null) {
			try {
				responseJsonObject.put("otp", "null");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		User user = userPhoneNumber.getUser();
		if (organizationMembershipService.getOrganizationMembershipByUserAndIsAdmin(user, true) == null) {
			try {
				responseJsonObject.put("otp", "null");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		Organization billingOrganization = organizationMembershipService.getLatestOrganizationMembership(user)
				.getOrganization();
		if (userPhoneNumber != null) {
			String otp = randomString(4);
			int status = 0;
			/*
			 * String email=userPhoneNumber.getUser().getEmail();
			 * if((status=SendMail.sendMail(email, "Cottage Industry App OTP" ,
			 * "Your OTP is: " + otp ))==1)
			 */
			status = 1;
			/* TODO move to Util class. No time */
			SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(billingOrganization);
			String authId = smsApiKeys.getAuthId();
			String authToken = smsApiKeys.getAuthToken();
			String sourceNumber = smsApiKeys.getSourceNumber();
			// String authId = "MANZBINJLKOTI0NWVHZD";
			// String authToken = "OWU2NDYyOTEzOGJkODBhZWU1ZjI0MjhlZDgxMWMw";
			RestAPI api = new RestAPI(authId, authToken, "v1");

			LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
			parameters.put("src", sourceNumber);
			parameters.put("dst", phonenumber);
			parameters.put("text", "Hello from Lokacart! \nYour OTP is: " + otp);
			parameters.put("method", "GET");
			try {
				MessageResponse msgResponse = api.sendMessage(parameters);
				System.out.println(msgResponse);
				System.out.println("Api ID : " + msgResponse.apiId);
				System.out.println("Message : " + msgResponse.message);
				if (msgResponse.serverCode == 202) {
					System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());
					response.put("text", "OTP SMS requested");

				} else {
					System.out.println(msgResponse.error);
				}
			} catch (PlivoException e) {
				System.out.println(e.getLocalizedMessage());
			}
			// IVRUtils.sendSMS(phonenumber, otp, null , null);
			response.put("otp", otp);
			try {
				responseJsonObject.put("otp", otp);

			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		} else {
			response.put("otp", null);
			try {
				responseJsonObject.put("otp", "null");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}

	}

	@Transactional
	@RequestMapping(value = "/loginform", method = RequestMethod.POST)
	public String loginForm(@RequestBody String requestBody) {
		JSONObject responseJsonObject = new JSONObject();
		JSONObject jsonObject = null;
		String email = null, address = null, pincode = null, lastname = null, firstname = null, phonenumber = null;
		try {
			jsonObject = new JSONObject(requestBody);
			phonenumber = jsonObject.getString("phonenumber");
		} catch (Exception e) {
			e.printStackTrace();
			try {
				responseJsonObject.put("response", "failure");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}

			return responseJsonObject.toString();
		}
		UserPhoneNumber userPhoneNumber = userPhoneNumberService.getUserPhoneNumber(phonenumber);
		User user = userPhoneNumber.getUser();
		try {
			address = jsonObject.getString("address");
			user.setAddress(address);
		} catch (Exception e) {
		}
		try {
			pincode = jsonObject.getString("pincode");
			user.setPincode(pincode);
		} catch (Exception e) {
		}

		try {
			email = jsonObject.getString("email");
			user.setEmail(email);
		} catch (Exception e) {

		}

		try {
			lastname = jsonObject.getString("lastname");
			user.setLastname(lastname);
		} catch (Exception e) {

		}
		try {
			firstname = jsonObject.getString("firstname");
			user.setName(firstname);
		} catch (JSONException e) {

		}
		userService.addUser(user);
		try {
			responseJsonObject.put("response", "success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}

	@Transactional
	@RequestMapping(value = "/changenumber", method = RequestMethod.POST)
	public String changenumber(@RequestBody String requestBody) {
		JSONObject responseJsonObject = new JSONObject();
		JSONObject jsonObject = null;
		String phonenumber_old = null;
		String phonenumber_new = null;
		try {
			jsonObject = new JSONObject(requestBody);
			phonenumber_old = jsonObject.getString("phonenumber");
			phonenumber_new = jsonObject.getString("phonenumber_new");
			System.out.println("phonenumber_old is :" + phonenumber_old);
			System.out.println("phonenumber_new is :" + phonenumber_new);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		try {
			UserPhoneNumber userPhoneNumber = userPhoneNumberRepository.findByPhoneNumber(phonenumber_old);
			User user = userPhoneNumber.getUser();
			try {
				if (userPhoneNumberRepository.findByPhoneNumber(phonenumber_new) != null) {
					responseJsonObject.put("status", "new number already present.");
					return responseJsonObject.toString();
				}
				System.out.println("Setting new number");
				userPhoneNumber.setPhoneNumber(phonenumber_new);
				userPhoneNumberService.addUserPhoneNumber(userPhoneNumber);
				System.out.println("Added to DB");
				userPhoneNumberService.setPrimaryPhoneNumberByUser(user, userPhoneNumber);
				System.out.println("Set to primary");
				responseJsonObject.put("status", "success");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} catch (NullPointerException e) {
			try {
				responseJsonObject.put("status", "user phone number not present");
			} catch (JSONException e1) {
				e.printStackTrace();
			}
		}
		return responseJsonObject.toString();
	}

	@Transactional
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(@RequestBody String requestBody) {
		// Check if exists
		JSONObject responseJsonObject = new JSONObject();
		JSONObject jsonObject = null;
		String phonenumber = null;
		String password = null;
		try {
			jsonObject = new JSONObject(requestBody);
			phonenumber = jsonObject.getString("phonenumber");
			password = jsonObject.getString("password");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		User user = userRepository.findByuserPhoneNumbers_phoneNumber(phonenumber);

		if (user != null) {
			/*
			 * Check if the user is approved
			 * 
			 */
			if (password == null) {
				try {
					responseJsonObject.put("Status", "Error");
					responseJsonObject.put("Error", "Password is null.");
					responseJsonObject.put("Authentication", "failure");
					responseJsonObject.put("registered", "true");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return responseJsonObject.toString();
			}
			if (user.getSha256Password() == null) {
				try {
					responseJsonObject.put("Status", "Error");
					responseJsonObject.put("Error", "User Password is null. Set the password");
					responseJsonObject.put("Authentication", "failure");
					responseJsonObject.put("registered", "true");
				} catch (JSONException e) {

					e.printStackTrace();
				}
				return responseJsonObject.toString();
			}
			if (passwordEncoder.matches(password, user.getSha256Password())) {
				List<OrganizationMembership> organizationMemberships = user.getOrganizationMemberships();
				List<Organization> organizationList = organizationService.getAllOrganizationList();
				JSONArray detailsArray = new JSONArray();
				try {
					if (user.getEmail() == null)
						detailsArray.put("email");
					else
						responseJsonObject.put("email", user.getEmail());

					if (user.getAddress() == null)
						detailsArray.put("address");
					else
						responseJsonObject.put("address", user.getAddress());

					if (user.getPincode().equals("0"))
						detailsArray.put("pincode");
					else
						responseJsonObject.put("pincode", user.getPincode());

					if (user.getLastname().equals("0"))
						detailsArray.put("lastname");
					else
						responseJsonObject.put("lastname", user.getLastname());

					if (user.getName() == null)
						detailsArray.put("firstname");
					else
						responseJsonObject.put("firstname", user.getName());

					UserPhoneNumber phoneNumber = userPhoneNumberService.getUserPrimaryPhoneNumber(user);
					if (phonenumber == null)
						detailsArray.put("phonenumber");
					else
						responseJsonObject.put("phonenumber", phoneNumber.getPhoneNumber());
					
					if (user.getProfilePic() == null)
						detailsArray.put("profilepic");
					else
						responseJsonObject.put("profilepic", user.getProfilePic());

					responseJsonObject.put("formFields", detailsArray);

				} catch (JSONException e) {
					e.printStackTrace();
				}
				if (organizationList.size() == 0) {
					try {
						/*
						 * JSONArray orgArray= new JSONArray();
						 * if(user.getEmail() == null) orgArray.put("email");
						 * 
						 * else responseJsonObject.put("email",
						 * user.getEmail());
						 * 
						 * if(user.getAddress() == null)
						 * orgArray.put("address"); else
						 * responseJsonObject.put("address", user.getAddress());
						 * 
						 * if(user.getPincode() == null)
						 * orgArray.put("pincode"); else
						 * responseJsonObject.put("pincode", user.getPincode());
						 */

						responseJsonObject.put("Authentication", "success");
						responseJsonObject.put("Error", "No organization has accepted the user");
						responseJsonObject.put("organizations", "null");
					} catch (Exception e) {
						e.printStackTrace();
					}
					return responseJsonObject.toString();
				}

				JSONArray orgArray = new JSONArray();
				for (Organization organization : organizationList) {
					try {

						if (!organization.getName().equals("Testing") && !organization.getName().equals("TestOrg1")
								&& !organization.getName().equals("Testorg3") && organizationMembershipService
										.getUserOrganizationMembership(user, organization) != null) {
							if (organizationMembershipService.getOrganizationMembershipStatus(user,
									organization) == 1) {
								JSONObject org = new JSONObject();

								org.put("name", organization.getName());
								org.put("org_id", organization.getOrganizationId());
								org.put("abbr", organization.getAbbreviation());
								org.put("ph_no", organization.getIvrNumber());
								org.put("contact", organization.getContact());
								if (organizationMembershipService.getUserOrganizationMembership(user,
										organization) == null) {
									org.put("status", "Rejected");
									orgArray.put(org);
									continue;
								}
								if (organizationMembershipService.getOrganizationMembershipStatus(user,
										organization) == 1)
									org.put("status", "Accepted");
								// organizationList.add(organizationMembership.getOrganization());
								else if (organizationMembershipService.getOrganizationMembershipStatus(user,
										organization) == 0)
									org.put("status", "Pending");
								else
									org.put("status", "Rejected");
								orgArray.put(org);

							}
						}
					}

					catch (JSONException e) {
						e.printStackTrace();
					}
				}

				try {
					responseJsonObject.put("Authentication", "success");
					// responseJsonObject.put("email", user.getEmail());
					responseJsonObject.put("organizations", orgArray);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return responseJsonObject.toString();
			} else {
				try {
					responseJsonObject.put("Authentication", "failure");
					responseJsonObject.put("registered", "true");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return responseJsonObject.toString();
			}
		} else {
			try {
				responseJsonObject.put("Authentication", "failure");
				responseJsonObject.put("registered", "false");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}

	}

	// authentication for admin app.
	@Transactional
	@RequestMapping(value = "/loginAdmin", method = RequestMethod.POST)
	public String loginAdmin(@RequestBody String requestBody) {
		// Check if exists
		JSONObject responseJsonObject = new JSONObject();
		JSONObject jsonObject = null;
		String phonenumber = null;
		String password = null;
		try {
			jsonObject = new JSONObject(requestBody);
			phonenumber = jsonObject.getString("phonenumber");
			password = jsonObject.getString("password");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		User user = userRepository.findByuserPhoneNumbers_phoneNumber(phonenumber);

		if (user != null) {
			/*
			 * Check if the user is approved
			 * 
			 */
			if (password == null) {
				try {
					responseJsonObject.put("Status", "Error");
					responseJsonObject.put("Error", "Password is null.");
					responseJsonObject.put("Authentication", "failure");
					responseJsonObject.put("registered", "true");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return responseJsonObject.toString();
			}
			if (user.getSha256Password() == null) {
				try {
					responseJsonObject.put("Status", "Error");
					responseJsonObject.put("Error", "User Password is null. Set the password");
					responseJsonObject.put("Authentication", "failure");
					responseJsonObject.put("registered", "true");
				} catch (JSONException e) {

					e.printStackTrace();
				}
				return responseJsonObject.toString();
			}
			if (passwordEncoder.matches(password, user.getSha256Password())) {
				List<OrganizationMembership> organizationMemberships = user.getOrganizationMemberships();
				for (OrganizationMembership orgm : organizationMemberships) {
					if (orgm.getIsAdmin()) {
						try {
							responseJsonObject.put("Authentication", "success");
							responseJsonObject.put("email", user.getEmail());
							JSONObject org = new JSONObject();
							org.put("name", orgm.getOrganization().getName());
							org.put("org_id", orgm.getOrganization().getOrganizationId());
							org.put("abbr", orgm.getOrganization().getAbbreviation());
							org.put("ph_no", orgm.getOrganization().getIvrNumber());
							responseJsonObject.put("organization", org);
						} catch (Exception e) {
							e.printStackTrace();
						}
						return responseJsonObject.toString();
					}

				}
				try {
					responseJsonObject.put("Authentication", "failure");
					responseJsonObject.put("registered", "true");
					responseJsonObject.put("Error", "Registered user is not an admin.");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return responseJsonObject.toString();
			} else {
				try {
					responseJsonObject.put("Authentication", "failure");
					responseJsonObject.put("registered", "true");
					responseJsonObject.put("Error", "null");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return responseJsonObject.toString();
			}
		} else {
			try {
				responseJsonObject.put("Authentication", "failure");
				responseJsonObject.put("registered", "false");
				responseJsonObject.put("Error", "null");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}

	}

	@Transactional
	@RequestMapping(value = "/changepassword", method = RequestMethod.POST)
	public HashMap<String, String> changePassword(@RequestBody String requestBody) {
		HashMap<String, String> response = new HashMap<String, String>();
		JSONObject jsonObject = null;
		String password = null;
		String phonenumber = null;
		try {
			jsonObject = new JSONObject(requestBody);
			phonenumber = jsonObject.getString("phonenumber");
			password = jsonObject.getString("password");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		User user = userRepository.findByuserPhoneNumbers_phoneNumber(phonenumber);
		if (user == null) {
			response.put("Status", "Error");
			response.put("Error", "No user with the phone number:" + phonenumber + " exists.");
			return response;
		}
		// AuthenticatedUser authuser=Utils.getSecurityPrincipal();

		password = passwordEncoder.encode(password);
		user.setSha256Password(password);
		userRepository.save(user);
		response.put("Status", "Success");
		return response;
	}

	// Util functions
	static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	static Random rnd = new Random();

	String randomString(int len) {
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		return sb.toString();
	}

	@Transactional
	@RequestMapping(value = "/passwordrequest", method = RequestMethod.POST)
	public @ResponseBody String requestPassword(@RequestBody String requestBody) {
		JSONObject responseJsonObject = new JSONObject();
		System.out.println("Inside password request.");

		String email = null, number = null, password = null;
		try {
			System.out.println(requestBody);
			JSONObject object = new JSONObject(requestBody);
			email = object.getString("email");
			number = object.getString("phonenumber");
			password = object.getString("password");
		} catch (Exception e) {
			e.printStackTrace();
			try {
				responseJsonObject.put("response", "failure");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		User user = userService.getUserFromEmail(email);
		UserPhoneNumber userPhoneNumber = userPhoneNumberService.getUserPhoneNumber(number);
		if (user.getUserId() == userPhoneNumber.getUser().getUserId()) {
			System.out.println("match");
			String otp = randomString(4);
			String hashTempPassword = passwordEncoder.encode(password);
			java.util.Date date = new java.util.Date();
			Timestamp currentTimestamp = new Timestamp(date.getTime());
			user.setTime(currentTimestamp);
			user.setPassTemp(hashTempPassword);
			user.setPassOtp(otp);
			user.setPassTime(currentTimestamp);
			userService.addUser(user);

			int id = user.getUserId();
			try {
				System.out.println("http://ruralict.cse.iitb.ac.in/RuralIvrs/app/approvepassword/" + id + "/" + otp);
				SendContent.sendMail(email, "Lokacart: Password Reset Request",
						"<html><head></head><body>We have received a request to reset your password. This request is valid for 24 hours.<br /> To approve, click on the link below :\n\n\t\t  <a href = \"http://ruralict.cse.iitb.ac.in/RuralIvrs/app/approvepassword/"
								+ id + "/" + otp + "\"> Update my password </a> <br /><br />"
								+ "Link doesn't work? Copy the following link to your browser address bar: <br /><br /> <a href = \"http://ruralict.cse.iitb.ac.in/RuralIvrs/app/approvepassword/"
								+ id + "/" + otp + "\"> http://ruralict.cse.iitb.ac.in/RuralIvrs/app/approvepassword/"
								+ id + "/" + otp + "</a> <br /><br /><br /> "
								+ "You received this email, because it was requested by a user. This is part of the procedure to create a new password on the system. If you DID NOT request a new password then please ignore this email and your password will remain the same.<br /><br />"
								+ "Thank you, <br />The RuralIVRS Team</body></html>");
				responseJsonObject.put("repsonse",
						"An email has been sent to the mentioned email ID. Please follow the instructions in the email to complete your request");
				return responseJsonObject.toString();
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Cannot send email");
				try {
					responseJsonObject.put("response", "Cannot process request");
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
				return responseJsonObject.toString();

			}
		} else {
			try {
				System.out.println("No match... ");
				responseJsonObject.put("response",
						"Cannot find account with entered credentials. Please enter the correct phonenumber and email ID");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();

		}
	}

	@Transactional
	@RequestMapping(value = "/enquiry", method = RequestMethod.POST)
	public @ResponseBody String postEnquiry(@RequestBody String requestBody) {
		JSONObject jsonObject;
		JSONObject jsonResponseObject = new JSONObject();
		Enquiry enquiry = null;
		String email = null, name = null, phonenumber = null, message = null, address1 = null, address2 = null;
		try {
			jsonObject = new JSONObject(requestBody);
			email = jsonObject.getString("email");
			name = jsonObject.getString("name");
			message = jsonObject.getString("message");
			phonenumber = jsonObject.getString("phonenumber");
			address1 = jsonObject.getString("address1");
			address2 = jsonObject.getString("address2");
			enquiry = new Enquiry(email, name, phonenumber, address1 + ", " + address2, message);
			enquiryService.addEnquiry(enquiry);
			System.out.println(jsonObject.toString());
		} catch (JSONException e) {
		}
		/*
		 * List <Organization> organizationList =
		 * organizationService.getAllOrganizationList(); Iterator <Organization>
		 * iterator = organizationList.iterator(); while (iterator.hasNext()) {
		 * //notify all admins Organization organization = iterator.next();
		 * String url =
		 * "http://ruralict.cse.iitb.ac.in/ruralict/app/user/"+organization.
		 * getAbbreviation()+"/"+enquiry.getId(); SmsApiKeys smsApiKeys =
		 * smsApiKeysService.getApiKeyByOrganization(organization); String
		 * authId = smsApiKeys.getAuthId(); String authToken =
		 * smsApiKeys.getAuthToken(); String sourceNumber =
		 * smsApiKeys.getSourceNumber(); List<OrganizationMembership> adminList
		 * =
		 * organizationMembershipService.getOrganizationMembershipListByIsAdmin(
		 * organization, true);
		 * 
		 * Iterator <OrganizationMembership> adminIter = adminList.iterator();
		 * while (adminIter.hasNext()) { OrganizationMembership
		 * organizationMembership = adminIter.next(); String destNumber =
		 * organizationMembership.getUser().getUserPhoneNumbers().get(0).
		 * getPhoneNumber(); RestAPI api = new RestAPI(authId, authToken, "v1");
		 * 
		 * LinkedHashMap<String, String> parameters = new LinkedHashMap<String,
		 * String>(); parameters.put("src", sourceNumber); parameters.put("dst",
		 * destNumber ); parameters.put("text", "Hello from Lokacart! \n"
		 * +enquiry.getName()+
		 * " is interested in Lokacart. Click here to add to your organization: "
		 * +url); parameters.put("method", "GET"); try { MessageResponse
		 * msgResponse = api.sendMessage(parameters);
		 * System.out.println(msgResponse); System.out.println("Api ID : " +
		 * msgResponse.apiId); System.out.println("Message : " +
		 * msgResponse.message); if (msgResponse.serverCode == 202) {
		 * System.out.println("Message UUID : " +
		 * msgResponse.messageUuids.get(0).toString());
		 * 
		 * } else { System.out.println(msgResponse.error); } } catch
		 * (PlivoException e) { System.out.println(e.getLocalizedMessage()); } }
		 * 
		 * }
		 */
		try {
			SendContent.sendMail(lokacartId, "New Enquiry from App",
					"<html><head></head><body><b>" + name
							+ "</b> would like to know more about Lokacart. <br /><br /><br />" + "<b>Timestamp: </b>"
							+ String.valueOf(new Timestamp((new Date()).getTime())) + "<br />" + "<b>Email ID: </b>"
							+ email + "<br />" + "<b>Phonenumber: </b>" + (phonenumber != null ? phonenumber : "N/A")
							+ "<br /><br /><b>Address: </b><br />" + address1 + "<br />" + address2
							+ "<br /><br /><b>Message: </b><br />" + message + "</body></html>");
			RestAPI api = new RestAPI("MANZBINJLKOTI0NWVHZD", "OWU2NDYyOTEzOGJkODBhZWU1ZjI0MjhlZDgxMWMw", "v1");

			LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
			parameters.put("src", "919322727415");
			parameters.put("dst", phonenumber);
			parameters.put("text",
					"Hello from Lokacart! \nThank you for your interest. We shall get back to you soon. For more info please contact our support team on 91-022-2576-4974 (Mon-Fri)");
			parameters.put("method", "GET");
			try {
				MessageResponse msgResponse = api.sendMessage(parameters);
				System.out.println("SMS sending");
				System.out.println(msgResponse);
				System.out.println("Api ID : " + msgResponse.apiId);
				System.out.println("Message : " + msgResponse.message);
				if (msgResponse.serverCode == 202) {
					System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());

				} else {
					System.out.println(msgResponse.error);
				}
			} catch (PlivoException e) {
				System.out.println(e.getLocalizedMessage());
			}

		} catch (Exception e) {
			e.printStackTrace();
			try {
				jsonResponseObject.put("response", "Failed to send enquiry");
				return jsonResponseObject.toString();
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		try {
			jsonResponseObject.put("response", "Enquiry sent");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}

	@RequestMapping(value = "/user/{orgabbr}/{enquiryId}", method = RequestMethod.GET)
	public String adminUserInvite(@PathVariable String orgabbr, @PathVariable int enquiryId) {
		Enquiry enquiry = enquiryService.getEnquiry(enquiryId);
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		String API_KEY = "AIzaSyBU0JpWx49ZIJHw5lF-lBJeAnL5ZpXktL0";
		String phonenumber = enquiry.getPhonenumber();
		String email = enquiry.getEmail();
		String address = enquiry.getAddress();
		String name = enquiry.getName();
		String message = enquiry.getMessage();
		List<OrganizationMembership> listMember = organizationMembershipService
				.getOrganizationMembershipListByIsAdmin(organization, true);
		String refreeEmail = listMember.get(0).getUser().getEmail();
		Random random = new Random();
		String password = null, generatedUrl = null, shortendUrl = null;
		JSONObject branchResponse = null, shortenerResponse = null;
		JSONObject responseJsonObject = new JSONObject();
		List<UserPhoneNumber> userPhoneNumbers = new ArrayList<UserPhoneNumber>();
		if ((userPhoneNumberService.findPreExistingPhoneNumber(phonenumber) == true)
				&& (userService.getUserFromEmail(email) == null)) {
			User user = new User();
			user.setEmail(email);
			userService.addUser(user);
			System.out.println("user with only email added");

			UserPhoneNumber userPhoneNumber = new UserPhoneNumber();
			userPhoneNumber.setPhoneNumber(phonenumber);
			userPhoneNumber.setPrimary(true);
			userPhoneNumber.setUser(user);
			userPhoneNumberService.addUserPhoneNumber(userPhoneNumber);
			userPhoneNumbers.add(userPhoneNumber);
			user.setUserPhoneNumbers(userPhoneNumbers);
			java.util.Date date = new java.util.Date();
			Timestamp currentTimestamp = new Timestamp(date.getTime());
			user.setTime(currentTimestamp);
			user.setTextbroadcastlimit(0);
			user.setVoicebroadcastlimit(0);
			user.setWebLocale("en");
			user.setCallLocale("en");
			if (address != null)
				user.setAddress(address);
			if (name != null)
				user.setName(name);
			int i = email.indexOf("@");
			String fname = email.substring(0, i);
			// password = fname+random.nextInt(1000);
			password = Integer.toString(random.nextInt(10));
			for (int count = 0; count < 9; ++count)
				password = password + random.nextInt(10);
			user.setPassToken(password);
			user.setSha256Password(passwordEncoder.encode(password));
			userService.addUser(user);
			System.out.println("user with only password added");

			User refUser = userService.getUserFromEmail(refreeEmail);
			System.out.println("User name: " + refUser.getName());
			Referral referral = new Referral();
			referral.setEmail(email);
			referral.setPhonenumber(phonenumber);
			referral.setOrganization(organization);
			referral.setUser(refUser); // This is the referee
			String referralCode = String.valueOf(random.nextInt(999999));
			Referral tempReferral = referralService.getByReferralCode(referralCode);
			while (tempReferral != null) {
				referralCode = String.valueOf(random.nextInt(999999));
				tempReferral = referralService.getByReferralCode(referralCode);
			}
			referral.setReferralCode(referralCode);
			String BRANCH_KEY = "key_live_nam1TdTbBvxAcKO49YOKDnlhvzlFSU8z";
			String android_deeplink = "lokacart://";
			String android_uri_scheme = "lokacart://";
			String android_package_name = "com.mobile.ict.cart";
			String android_url = "https://play.google.com/store/apps/details?id=com.mobile.ict.cart";
			// String reqBody =
			// "{\n\"branch_key\":\""+BRANCH_KEY+"\",\n\"data\":\"{\"phonenumber\":\""+phonenumber+"\",\"email\":\""+email+"\",\"password\":\""+password+"\",\"referralCode\":\""+referralCode+"\"}\"";
			String reqBody = "{\n\"branch_key\":\"" + BRANCH_KEY + "\",\n\"data\":\"{\\\"phonenumber\\\":\\\""
					+ phonenumber + "\\\", \\\"email\\\":\\\"" + email + "\\\", \\\"referralCode\\\":\\\""
					+ referralCode + "\\\", \\\"details\\\":\\\"1\\\", \\\"token\\\":\\\"" + user.getPassToken()
					+ "\\\"}\"\n    \n}";

			/*
			 * String reqBody = "{\n\"branch_key\":\""+BRANCH_KEY+
			 * "\",\n\"data\":\"{\\\"phonenumber\\\":\\\""+phonenumber+
			 * "\\\", \\\"email\\\":\\\""+email+
			 * "\\\", \\\"referralCode\\\":\\\""+referralCode+
			 * "\\\", \\\"password\\\":\\\""+ password+
			 * "\\\", \\\"details\\\":\\\"1\\\", \\\"token\\\":\\\""
			 * +user.getPassToken()+"\\\",\\\"organization\\\":\\\""+
			 * destOrganization.getName()+ "\\\", \\\"abbr\\\":\\\""
			 * +destOrganization.getAbbreviation()+"\\\"}\"\n    \n}";
			 */
			System.out.println("body: " + reqBody);

			OkHttpClient client = new OkHttpClient();
			MediaType mediaType = MediaType.parse("application/json");
			okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType, reqBody);
			Request request = new Request.Builder().url("https://api.branch.io/v1/url").post(body)
					.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache")
					.addHeader("postman-token", "c366fc2b-523b-bcba-0a8b-bda6e43a2118").build();
			try {
				Response response = client.newCall(request).execute();
				System.out.println(response.toString());
				String jsonData = response.body().string();
				branchResponse = new JSONObject(jsonData);
				generatedUrl = branchResponse.getString("url");
				System.out.println("URL: " + generatedUrl);
				referral.setLink(generatedUrl);
				referralService.addReferral(referral);

			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			String reqBodyUrl = "{\n    \"longUrl\": \"http://ruralict.cse.iitb.ac.in/ruralict/app/code/" + referralCode
					+ "\"\n\n}";
			OkHttpClient clientNew = new OkHttpClient();
			MediaType mediaTypeNew = MediaType.parse("application/json");
			okhttp3.RequestBody bodyNew = okhttp3.RequestBody.create(mediaTypeNew, reqBodyUrl);
			Request requestNew = new Request.Builder()
					.url("https://www.googleapis.com/urlshortener/v1/url?key=" + API_KEY).post(bodyNew)
					.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache")
					.addHeader("postman-token", "71e06008-92c4-c2a3-5c6a-e718f5bc3e5b").build();
			try {
				Response response = clientNew.newCall(requestNew).execute();
				System.out.println(response.toString());
				String jsonData = response.body().string();
				shortenerResponse = new JSONObject(jsonData);
				shortendUrl = shortenerResponse.getString("id");
				System.out.println("Shortened URL: " + shortendUrl);

			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(organization);
			String authId = smsApiKeys.getAuthId();
			String authToken = smsApiKeys.getAuthToken();
			String sourceNumber = smsApiKeys.getSourceNumber();
			RestAPI api = new RestAPI(authId, authToken, "v1");

			LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
			parameters.put("src", sourceNumber);
			parameters.put("dst", phonenumber);
			parameters.put("text", "Hello from Lokacart! \nYou have been referred to " + organization.getName() + " by "
					+ refUser.getName() + "\nClick here to accept and use: " + shortendUrl);
			parameters.put("method", "GET");
			try {
				MessageResponse msgResponse = api.sendMessage(parameters);
				System.out.println(msgResponse);
				System.out.println("Api ID : " + msgResponse.apiId);
				System.out.println("Message : " + msgResponse.message);
				if (msgResponse.serverCode == 202) {
					System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());

				} else {
					System.out.println(msgResponse.error);
				}
			} catch (PlivoException e) {
				System.out.println(e.getLocalizedMessage());
			}

			try {
				responseJsonObject.put("response", "success");
			} catch (JSONException e) {
				e.printStackTrace();
			}

		} else {
			int flag = 0;
			User user = userService.getUserFromEmail(email);
			UserPhoneNumber userPhoneNumber = userPhoneNumberService.getUserPhoneNumber(phonenumber);
			if (user == null || userPhoneNumber == null) {
				try {
					responseJsonObject.put("response", "Email ID and Phone number mismatch");
					responseJsonObject.put("error", "Email ID and Phone number mismatch");

				} catch (JSONException e) {
					e.printStackTrace();
				}
				return responseJsonObject.toString();
			}
			if (user == userPhoneNumber.getUser()) {
				// Existing user
				int details = 0;
				if (user.getEmail() == null || user.getLastname() == null || user.getName() == null
						|| user.getAddress() == null || user.getName() == null || user.getPincode() == null) {
					details = 1;
				}
				List<OrganizationMembership> list = user.getOrganizationMemberships();
				Iterator<OrganizationMembership> iterator = list.iterator();
				while (iterator.hasNext()) {
					OrganizationMembership organizationMembership = iterator.next();
					if (organizationMembership.getIsAdmin() == true)
						flag = 1;
				}
				if (flag == 1) {
					try {
						responseJsonObject.put("response", "failure");
						responseJsonObject.put("reason", "Admin accounts cannot be used on the consumer app");
						return responseJsonObject.toString();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (user.getPassToken() == null) {
					// Resetting password
					password = Integer.toString(random.nextInt(10));
					for (int count = 0; count < 9; ++count)
						password = password + random.nextInt(10);
					user.setPassToken(password);
					user.setSha256Password(passwordEncoder.encode(password));
					userService.addUser(user);
					System.out.println("pass token: " + password);
				}
				List<OrganizationMembership> memberList = user.getOrganizationMemberships();
				if (memberList.isEmpty() == false) {
					Iterator<OrganizationMembership> memberIterator = memberList.iterator();
					while (memberIterator.hasNext()) {
						OrganizationMembership organizationMembership = memberIterator.next();
						if (organization == organizationMembership.getOrganization()) {
							try {
								responseJsonObject.put("response", "Already a member");
							} catch (JSONException e) {
								e.printStackTrace();
							}
							return responseJsonObject.toString();
						}
					}
				}
				User refUser = userService.getUserFromEmail(refreeEmail);
				Referral referral = new Referral();
				referral.setEmail(email);
				referral.setPhonenumber(phonenumber);
				referral.setOrganization(organization);
				referral.setUser(refUser);
				String referralCode = String.valueOf(random.nextInt(999999));
				String android_url = "https://play.google.com/store/apps/details?id=com.mobile.ict.cart";
				String android_uri_scheme = "lokacart://";
				String android_package_name = "com.mobile.ict.cart";
				Referral tempReferral = referralService.getByReferralCode(referralCode);
				while (tempReferral != null) {
					referralCode = String.valueOf(random.nextInt(999999));
					tempReferral = referralService.getByReferralCode(referralCode);
				}
				referral.setReferralCode(referralCode);
				String BRANCH_KEY = "key_live_nam1TdTbBvxAcKO49YOKDnlhvzlFSU8z";
				String android_deeplink = "lokacart://";
				String reqBody = "{\n\"branch_key\":\"" + BRANCH_KEY + "\",\n\"data\":\"{\\\"phonenumber\\\":\\\""
						+ phonenumber + "\\\", \\\"email\\\":\\\"" + email + "\\\", \\\"referralCode\\\":\\\""
						+ referralCode + "\\\", \\\"details\\\":\\\"" + details + "\\\", \\\"token\\\":\\\""
						+ user.getPassToken() + "\\\"}\"\n    \n}";

				System.out.println("body: " + reqBody);

				OkHttpClient client = new OkHttpClient();
				MediaType mediaType = MediaType.parse("application/json");
				okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType, reqBody);
				Request request = new Request.Builder().url("https://api.branch.io/v1/url").post(body)
						.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache")
						.addHeader("postman-token", "c366fc2b-523b-bcba-0a8b-bda6e43a2118").build();
				/*
				 * MediaType mediaType = MediaType.parse("application/json");
				 * okhttp3.RequestBody body =
				 * okhttp3.RequestBody.create(mediaType,
				 * "{\n\"branch_key\":\"key_live_feebgAAhbH9Tv85H5wLQhpdaefiZv5Dv\",\n\"data\":\"{\\\"name\\\": \\\"Alex\\\"}\"\n    \n}"
				 * ); Request request = new Request.Builder()
				 * .url("https://api.branch.io/v1/url") .post(body)
				 * .addHeader("content-type", "application/json")
				 * .addHeader("cache-control", "no-cache")
				 * .addHeader("postman-token",
				 * "7a4da102-dc81-31b9-3028-aedee4a30025") .build();
				 */
				try {
					Response response = client.newCall(request).execute();
					System.out.println(response.toString());
					String jsonData = response.body().string();
					branchResponse = new JSONObject(jsonData);
					generatedUrl = branchResponse.getString("url");
					System.out.println("URL: " + generatedUrl);
					referral.setLink(generatedUrl);
					referralService.addReferral(referral);

				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (JSONException e) {
					e.printStackTrace();
				}

				String reqBodyUrl = "{\n    \"longUrl\": \"http://ruralict.cse.iitb.ac.in/ruralict/app/code/"
						+ referralCode + "\"\n\n}";
				OkHttpClient clientNew = new OkHttpClient();
				MediaType mediaTypeNew = MediaType.parse("application/json");
				okhttp3.RequestBody bodyNew = okhttp3.RequestBody.create(mediaTypeNew, reqBodyUrl);
				Request requestNew = new Request.Builder()
						.url("https://www.googleapis.com/urlshortener/v1/url?key=" + API_KEY).post(bodyNew)
						.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache")
						.addHeader("postman-token", "71e06008-92c4-c2a3-5c6a-e718f5bc3e5b").build();
				try {
					Response response = clientNew.newCall(requestNew).execute();
					System.out.println(response.toString());
					String jsonData = response.body().string();
					shortenerResponse = new JSONObject(jsonData);
					shortendUrl = shortenerResponse.getString("id");
					System.out.println("Shortened URL: " + shortendUrl);

				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (JSONException e) {
					e.printStackTrace();
				}

				SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(organization);
				String authId = smsApiKeys.getAuthId();
				String authToken = smsApiKeys.getAuthToken();
				String sourceNumber = smsApiKeys.getSourceNumber();
				RestAPI api = new RestAPI(authId, authToken, "v1");

				LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
				parameters.put("src", sourceNumber);
				parameters.put("dst", phonenumber);
				parameters.put("text", "Hello from Lokacart! \nYou have been referred to " + organization.getName()
						+ " by " + refUser.getName() + "\nClick here to accept and use: " + shortendUrl);
				parameters.put("method", "GET");
				System.out.println("http://ruralict.cse.iitb.ac.in/ruralict/app/code/" + referralCode);
				try {
					MessageResponse msgResponse = api.sendMessage(parameters);
					System.out.println(msgResponse);
					System.out.println("Api ID : " + msgResponse.apiId);
					System.out.println("Message : " + msgResponse.message);
					if (msgResponse.serverCode == 202) {
						System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());

					} else {
						System.out.println(msgResponse.error);
					}
				} catch (PlivoException e) {
					System.out.println(e.getLocalizedMessage());
				}

				try {
					responseJsonObject.put("response", "success");
				} catch (JSONException e) {
					e.printStackTrace();
				}

			} else {
				// send error
				try {
					System.out.println("here");
					responseJsonObject.put("response", "Email ID and Phone number mismatch");
					responseJsonObject.put("error", "Email ID and Phone number mismatch");
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}

		return responseJsonObject.toString();
	}

	@Transactional
	@RequestMapping(value = "/membershiptest", method = RequestMethod.GET)
	public void test(@RequestParam int userId) {
		User user = userService.getUser(userId);
		Organization billedOrganization = organizationMembershipService.getLatestOrganizationMembership(user)
				.getOrganization();
		System.out.println(
				"Organization: " + billedOrganization.getName() + " ID: " + billedOrganization.getOrganizationId());
	}

	@Transactional
	@RequestMapping(value = "/approvepassword/{userId}/{otp}", method = RequestMethod.GET)
	public void approveChange(@PathVariable int userId, @PathVariable String otp, HttpServletResponse response) {
		User user = null;
		try {
			user = userService.getUser(userId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String storedOtp = null;
		long otpTime = 0;
		try {
			storedOtp = user.getPassOtp();
			Timestamp otpStamp = user.getPassTime();
			// response.setContentType("text/html");
			// ClassPathResource res = new
			// ClassPathResource("templates/success.html");
			// try{
			// BufferedReader br = new BufferedReader(new InputStreamReader(
			// res.getInputStream()));
			// String cur, page = "";
			// while ((cur = br.readLine()) != null) {
			// page += cur;
			// }
			// response.getWriter().print(page);
			// br.close();
			// }
			// catch(IOException e) {
			// e.printStackTrace();
			// }

			otpTime = otpStamp.getTime();
		} catch (Exception e) {
			try {
				response.sendRedirect("/invalid");
				return;
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		if (otp.equals(storedOtp)) {
			java.util.Date date = new java.util.Date();
			Timestamp currentTimestamp = new Timestamp(date.getTime());
			long currentTime = currentTimestamp.getTime();
			if ((currentTime - otpTime) >= 86400000) {
				System.out.println("Link expired");
				try {
					response.sendRedirect("/expire");
					return;
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {

				user.setSha256Password(user.getPassTemp());
				user.setPassOtp(null);
				user.setPassTime(null);
				user.setPassTemp(null);
				userService.addUser(user);
				System.out.println("Password updated");
				// File file = new
				// File("/src/main/resources/templates/success.html");
				try {
					response.sendRedirect("/success");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		} else {
			System.out.println("Invalid link");
			try {
				response.sendRedirect("/invalid");
				return;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Transactional
	@RequestMapping(value = "/acceptreferral", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String acceptReferral(@RequestBody String responseBody) {

		JSONObject responseJsonObject = new JSONObject();
		JSONObject jsonObject;
		int details = 0;
		List<OrganizationMembership> organizationMemberships = new ArrayList<OrganizationMembership>();
		Organization organization = null;
		User user = null;
		String referralCode = null;
		int id = 1;
		float appVersion = 0;
		try {
			jsonObject = new JSONObject(responseBody);
			referralCode = jsonObject.getString("referralcode");
			appVersion = Float.parseFloat(jsonObject.getString("version"));
			VersionCheck currentVersion = versionCheckRepository.findOne(id);
			float curVersion = currentVersion.getVersion();
			int curMandatory = currentVersion.getMandatory();
			if (curVersion <= appVersion) {
				try {
					responseJsonObject.put("update", "0");
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} else if ((curVersion > appVersion) && (curMandatory == 0)) {
				try {
					responseJsonObject.put("update", "1");
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} else if ((curVersion > appVersion) && (curMandatory == 1)) {
				try {
					responseJsonObject.put("update", "2");
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
			try {
				responseJsonObject.put("response", "failure");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		Referral referral = referralService.getByReferralCode(referralCode);
		if (referral == null) {
			try {
				responseJsonObject.put("response", "No invite available");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		try {
			organization = referral.getOrganization();
			user = userService.getUserFromEmail(referral.getEmail());
			if (user.getEmail() == null || user.getLastname() == null || user.getName() == null
					|| user.getAddress() == null || user.getName() == null || user.getPincode() == null) {
				details = 1;
			}
			List<OrganizationMembership> list = user.getOrganizationMemberships();
			if (list.isEmpty() == false) {
				Iterator<OrganizationMembership> iterator = list.iterator();
				while (iterator.hasNext()) {
					OrganizationMembership organizationMembership = iterator.next();
					if (organization == organizationMembership.getOrganization()) {
						try {
							responseJsonObject.put("response", "Already a member");
							responseJsonObject.put("flag", Integer.toString(details));
							responseJsonObject.put("organization", organization.getName());
							responseJsonObject.put("abbr", organization.getAbbreviation());
							if (user.getName() != null)
								responseJsonObject.put("name", user.getName());
							else
								responseJsonObject.put("name", "null");
							responseJsonObject.put("email", user.getEmail());
							if (user.getAddress() != null)
								responseJsonObject.put("address", user.getAddress());
							else
								responseJsonObject.put("address", "null");

							if (user.getLastname() != null)
								responseJsonObject.put("lastname", user.getLastname());
							else
								responseJsonObject.put("lastname", "null");

							if (user.getPincode() != null)
								responseJsonObject.put("pincode", user.getPincode());
							else
								responseJsonObject.put("pincode", "null");
							
							if (user.getProfilePic() == null)
								responseJsonObject.put("profilepic", "null");
							else
								responseJsonObject.put("profilepic", user.getProfilePic());
							
							responseJsonObject.put("phonenumber", user.getUserPhoneNumbers().get(0).getPhoneNumber());
							responseJsonObject.put("email", user.getEmail());
						} catch (JSONException e) {
							e.printStackTrace();
						}
						return responseJsonObject.toString();
					}
				}
			}
			OrganizationMembership organizationMembership = new OrganizationMembership();
			organizationMembership.setOrganization(organization);
			organizationMembership.setUser(user);
			organizationMembership.setIsAdmin(false);
			organizationMembership.setIsPublisher(false);
			organizationMembership.setStatus(1);
			organizationMembership = organizationMemberRepository.save(organizationMembership);
			organizationMemberships.add(organizationMembership);
			user.setOrganizationMemberships(organizationMemberships);
			userService.addUser(user);
			groupMembershipService.addParentGroupMembership(organization, user);
		} catch (Exception e) {
			e.printStackTrace();
			try {
				responseJsonObject.put("response", "Error in approval");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		// int details = 0;

		try {
			responseJsonObject.put("response", "success");
			responseJsonObject.put("flag", Integer.toString(details));
			responseJsonObject.put("organization", organization.getName());
			responseJsonObject.put("abbr", organization.getAbbreviation());
			if (user.getName() != null)
				responseJsonObject.put("name", user.getName());
			else
				responseJsonObject.put("name", "null");
			responseJsonObject.put("email", user.getEmail());
			if (user.getAddress() != null)
				responseJsonObject.put("address", user.getAddress());
			else
				responseJsonObject.put("address", "null");

			if (user.getLastname() != null)
				responseJsonObject.put("lastname", user.getLastname());
			else
				responseJsonObject.put("lastname", "null");

			if (user.getPincode() != null)
				responseJsonObject.put("pincode", user.getPincode());
			else
				responseJsonObject.put("pincode", "null");
			
			if (user.getProfilePic() == null)
				responseJsonObject.put("profilepic", "null");
			else
				responseJsonObject.put("profilepic", user.getProfilePic());

			responseJsonObject.put("phonenumber", user.getUserPhoneNumbers().get(0).getPhoneNumber());
			responseJsonObject.put("email", user.getEmail());

			List<OrganizationMembership> list = user.getOrganizationMemberships();
			Iterator<OrganizationMembership> iterator = list.iterator();
			System.out.println(list.size());
			JSONArray array = new JSONArray();
			while (iterator.hasNext()) {
				OrganizationMembership membership = iterator.next();
				JSONObject object = new JSONObject();
				object.put("organization", membership.getOrganization().getName());
				object.put("abbr", membership.getOrganization().getAbbreviation());
				array.put(object);
			}
			responseJsonObject.put("orglist", array);
			responseJsonObject.put("version", Float.toString(versionCheckRepository.findOne(1).getVersion()));
			responseJsonObject.put("mandatory", Integer.toString(versionCheckRepository.findOne(1).getMandatory()));
			// responseJsonObject.put(", arg1)
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}

	@Transactional
	@RequestMapping(value = "/code/{referralCode}", method = RequestMethod.GET)
	public void getBranchLink(@PathVariable String referralCode, HttpServletResponse response) {
		System.out.println(referralCode);

		Referral referral = referralService.getByReferralCode(referralCode);
		if (referral == null) {
			try {
				response.sendRedirect("http://ruralict.cse.iitb.ac.in/Downloads/LokacartReferral/index.html");
				return;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println(referral.getId());
		System.out.println(referral.getLink());
		try {
			response.sendRedirect(referral.getLink());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Transactional
	@RequestMapping(value = "/refer", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String newReferral(@RequestBody String requestBody) {
		int noEmail=0;

		JSONObject responseJsonObject = new JSONObject();
		JSONObject jsonObject = null, branchResponse = null, shortenerResponse = null;
		String API_KEY = "AIzaSyBU0JpWx49ZIJHw5lF-lBJeAnL5ZpXktL0";
		String phonenumber = null, email = null, orgabbr = null, refreeEmail = null, password = null,
				generatedUrl = null, address = null, name = null, lastname = null;
		String shortendUrl = null;
		Random random = new Random();
		List<UserPhoneNumber> userPhoneNumbers = new ArrayList<UserPhoneNumber>();
		Organization destOrganization = null;
		try {
			jsonObject = new JSONObject(requestBody);
			System.out.println("JSON Object refer: " + jsonObject);
			phonenumber = jsonObject.getString("phonenumber");
			System.out.println(phonenumber);
			
			orgabbr = jsonObject.getString("abbr");
			System.out.println(orgabbr);
			try {
				refreeEmail = jsonObject.getString("refemail");
			} catch (Exception e) {
				// fetch
				Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
				System.out.println(organization.getName());
				List<OrganizationMembership> list = organizationMembershipService
						.getOrganizationMembershipListByIsAdmin(organization, true);
				refreeEmail = list.get(0).getUser().getEmail();
			}
			try {
				email = jsonObject.getString("email");
				System.out.println(email);
			} catch (Exception e11) {
				noEmail=1;
				email=phonenumber+"@gmail.com";
				// Do nothing
			}

			try {
				address = jsonObject.getString("address");
			} catch (Exception e1) {
				// Do nothing
			}
			try {
				name = jsonObject.getString("name");
			} catch (Exception e2) {
				// Do nothing
			}
			try {
				lastname = jsonObject.getString("lastname");
			} catch (Exception e3) {
				// Do nothing
			}
		} catch (JSONException e) {
			e.printStackTrace();
			try {
				System.out.println("JSON exception caught");
				responseJsonObject.put("response", "failure");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		System.out.println("JSON passed");

		destOrganization = organizationService.getOrganizationByAbbreviation(orgabbr);
		if ((userPhoneNumberService.findPreExistingPhoneNumber(phonenumber) == true)
				&& (userService.getUserFromEmail(email) == null)) {
			// New user scenario
			System.out.println("inside");
			User user = new User();
			user.setEmail(email);
			userService.addUser(user);
			System.out.println("user with only email added");

			UserPhoneNumber userPhoneNumber = new UserPhoneNumber();
			userPhoneNumber.setPhoneNumber(phonenumber);
			userPhoneNumber.setPrimary(true);
			userPhoneNumber.setUser(user);
			userPhoneNumberService.addUserPhoneNumber(userPhoneNumber);
			userPhoneNumbers.add(userPhoneNumber);
			user.setUserPhoneNumbers(userPhoneNumbers);
			java.util.Date date = new java.util.Date();
			Timestamp currentTimestamp = new Timestamp(date.getTime());
			user.setTime(currentTimestamp);
			user.setTextbroadcastlimit(0);
			user.setVoicebroadcastlimit(0);
			user.setWebLocale("en");
			user.setCallLocale("en");
			if (address != null)
				user.setAddress(address);
			if (name != null)
				user.setName(name);
			if (lastname != null)
				user.setLastname(lastname);
			String fname;
			if(noEmail!=1)
			{
				int i = email.indexOf("@");
				fname = email.substring(0, i);
			 
			}
			else
			{
				fname=" ";
			}
			password = fname+random.nextInt(1000);
			password = Integer.toString(random.nextInt(10));
			for (int count = 0; count < 9; ++count)
				password = password + random.nextInt(10);
			user.setPassToken(password);
			user.setSha256Password(passwordEncoder.encode(password));
			userService.addUser(user);
			System.out.println("user with only password added");

			User refUser = userService.getUserFromEmail(refreeEmail);
			System.out.println("User name: " + refUser.getName());
			Referral referral = new Referral();
			referral.setEmail(email);
			referral.setPhonenumber(phonenumber);
			referral.setOrganization(destOrganization);
			referral.setUser(refUser); // This is the referee
			String referralCode = String.valueOf(random.nextInt(999999));
			Referral tempReferral = referralService.getByReferralCode(referralCode);
			while (tempReferral != null) {
				referralCode = String.valueOf(random.nextInt(999999));
				tempReferral = referralService.getByReferralCode(referralCode);
			}
			referral.setReferralCode(referralCode);
			String BRANCH_KEY = "key_live_nam1TdTbBvxAcKO49YOKDnlhvzlFSU8z";
			String android_deeplink = "lokacart://";
			String android_uri_scheme = "lokacart://";
			String android_package_name = "com.mobile.ict.cart";
			String android_url = "https://play.google.com/store/apps/details?id=com.mobile.ict.cart";
			// String reqBody =
			// "{\n\"branch_key\":\""+BRANCH_KEY+"\",\n\"data\":\"{\"phonenumber\":\""+phonenumber+"\",\"email\":\""+email+"\",\"password\":\""+password+"\",\"referralCode\":\""+referralCode+"\"}\"";
			String reqBody = "{\n\"branch_key\":\"" + BRANCH_KEY + "\",\n\"data\":\"{\\\"phonenumber\\\":\\\""
					+ phonenumber + "\\\", \\\"email\\\":\\\"" + email + "\\\", \\\"referralCode\\\":\\\""
					+ referralCode + "\\\", \\\"details\\\":\\\"1\\\", \\\"token\\\":\\\"" + user.getPassToken()
					+ "\\\"}\"\n    \n}";

			/*
			 * String reqBody = "{\n\"branch_key\":\""+BRANCH_KEY+
			 * "\",\n\"data\":\"{\\\"phonenumber\\\":\\\""+phonenumber+
			 * "\\\", \\\"email\\\":\\\""+email+
			 * "\\\", \\\"referralCode\\\":\\\""+referralCode+
			 * "\\\", \\\"password\\\":\\\""+ password+
			 * "\\\", \\\"details\\\":\\\"1\\\", \\\"token\\\":\\\""
			 * +user.getPassToken()+"\\\",\\\"organization\\\":\\\""+
			 * destOrganization.getName()+ "\\\", \\\"abbr\\\":\\\""
			 * +destOrganization.getAbbreviation()+"\\\"}\"\n    \n}";
			 */
			System.out.println("body: " + reqBody);

			OkHttpClient client = new OkHttpClient();
			MediaType mediaType = MediaType.parse("application/json");
			okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType, reqBody);
			Request request = new Request.Builder().url("https://api.branch.io/v1/url").post(body)
					.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache")
					.addHeader("postman-token", "c366fc2b-523b-bcba-0a8b-bda6e43a2118").build();
			try {
				Response response = client.newCall(request).execute();
				System.out.println(response.toString());
				String jsonData = response.body().string();
				branchResponse = new JSONObject(jsonData);
				generatedUrl = branchResponse.getString("url");
				System.out.println("URL: " + generatedUrl);
				referral.setLink(generatedUrl);
				referralService.addReferral(referral);

			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			String reqBodyUrl = "{\n    \"longUrl\": \"http://ruralict.cse.iitb.ac.in/ruralict/app/code/" + referralCode
					+ "\"\n\n}";
			OkHttpClient clientNew = new OkHttpClient();
			MediaType mediaTypeNew = MediaType.parse("application/json");
			okhttp3.RequestBody bodyNew = okhttp3.RequestBody.create(mediaTypeNew, reqBodyUrl);
			Request requestNew = new Request.Builder()
					.url("https://www.googleapis.com/urlshortener/v1/url?key=" + API_KEY).post(bodyNew)
					.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache")
					.addHeader("postman-token", "71e06008-92c4-c2a3-5c6a-e718f5bc3e5b").build();
			try {
				Response response = clientNew.newCall(requestNew).execute();
				System.out.println(response.toString());
				String jsonData = response.body().string();
				shortenerResponse = new JSONObject(jsonData);
				shortendUrl = shortenerResponse.getString("id");
				System.out.println("Shortened URL: " + shortendUrl);

			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(destOrganization);
			String authId = smsApiKeys.getAuthId();
			String authToken = smsApiKeys.getAuthToken();
			String sourceNumber = smsApiKeys.getSourceNumber();
			RestAPI api = new RestAPI(authId, authToken, "v1");

			LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
			parameters.put("src", sourceNumber);
			parameters.put("dst", phonenumber);
			parameters.put("text", "Hello from Lokacart! \nYou have been referred to " + destOrganization.getName()
					+ " by " + refUser.getName() + "\nClick here to accept and use: " + shortendUrl);
			parameters.put("method", "GET");
			try {
				MessageResponse msgResponse = api.sendMessage(parameters);
				System.out.println(msgResponse);
				System.out.println("Api ID : " + msgResponse.apiId);
				System.out.println("Message : " + msgResponse.message);
				if (msgResponse.serverCode == 202) {
					System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());

				} else {
					System.out.println(msgResponse.error);
				}
			} catch (PlivoException e) {
				System.out.println(e.getLocalizedMessage());
			}

			try {
				responseJsonObject.put("response", "success");
			} catch (JSONException e) {
				e.printStackTrace();
			}

		} else {
			int flag = 0;
			User user = userService.getUserFromEmail(email);
			UserPhoneNumber userPhoneNumber = userPhoneNumberService.getUserPhoneNumber(phonenumber);
			if (user == null || userPhoneNumber == null) {
				try {
					responseJsonObject.put("response", "Email ID and Phone number mismatch");
					responseJsonObject.put("error", "Email ID and Phone number mismatch");

				} catch (JSONException e) {
					e.printStackTrace();
				}
				return responseJsonObject.toString();
			}
			if (user == userPhoneNumber.getUser()) {
				// Existing user
				int details = 0;
				if (user.getEmail() == null || user.getLastname() == null || user.getName() == null
						|| user.getAddress() == null || user.getName() == null || user.getPincode() == null) {
					details = 1;
				}
				List<OrganizationMembership> list = user.getOrganizationMemberships();
				Iterator<OrganizationMembership> iterator = list.iterator();
				while (iterator.hasNext()) {
					OrganizationMembership organizationMembership = iterator.next();
					if (organizationMembership.getIsAdmin() == true)
						flag = 1;
				}
				if (flag == 1) {
					try {
						responseJsonObject.put("response", "failure");
						responseJsonObject.put("reason", "Admin accounts cannot be used on the consumer app");
						return responseJsonObject.toString();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (user.getPassToken() == null) {
					// Resetting password
					password = Integer.toString(random.nextInt(10));
					for (int count = 0; count < 9; ++count)
						password = password + random.nextInt(10);
					user.setPassToken(password);
					user.setSha256Password(passwordEncoder.encode(password));
					userService.addUser(user);
					System.out.println("pass token: " + password);
				}
				List<OrganizationMembership> memberList = user.getOrganizationMemberships();
				if (memberList.isEmpty() == false) {
					Iterator<OrganizationMembership> memberIterator = memberList.iterator();
					while (memberIterator.hasNext()) {
						OrganizationMembership organizationMembership = memberIterator.next();
						if (destOrganization == organizationMembership.getOrganization()) {
							try {
								responseJsonObject.put("response", "Already a member");
							} catch (JSONException e) {
								e.printStackTrace();
							}
							return responseJsonObject.toString();
						}
					}
				}
				User refUser = userService.getUserFromEmail(refreeEmail);
				Referral referral = new Referral();
				referral.setEmail(email);
				referral.setPhonenumber(phonenumber);
				referral.setOrganization(destOrganization);
				referral.setUser(refUser);
				String referralCode = String.valueOf(random.nextInt(999999));
				String android_url = "https://play.google.com/store/apps/details?id=com.mobile.ict.cart";
				String android_uri_scheme = "lokacart://";
				String android_package_name = "com.mobile.ict.cart";
				Referral tempReferral = referralService.getByReferralCode(referralCode);
				while (tempReferral != null) {
					referralCode = String.valueOf(random.nextInt(999999));
					tempReferral = referralService.getByReferralCode(referralCode);
				}
				referral.setReferralCode(referralCode);
				String BRANCH_KEY = "key_live_nam1TdTbBvxAcKO49YOKDnlhvzlFSU8z";
				String android_deeplink = "lokacart://";
				String reqBody = "{\n\"branch_key\":\"" + BRANCH_KEY + "\",\n\"data\":\"{\\\"phonenumber\\\":\\\""
						+ phonenumber + "\\\", \\\"email\\\":\\\"" + email + "\\\", \\\"referralCode\\\":\\\""
						+ referralCode + "\\\", \\\"details\\\":\\\"" + details + "\\\", \\\"token\\\":\\\""
						+ user.getPassToken() + "\\\"}\"\n    \n}";

				System.out.println("body: " + reqBody);

				OkHttpClient client = new OkHttpClient();
				MediaType mediaType = MediaType.parse("application/json");
				okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType, reqBody);
				Request request = new Request.Builder().url("https://api.branch.io/v1/url").post(body)
						.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache")
						.addHeader("postman-token", "c366fc2b-523b-bcba-0a8b-bda6e43a2118").build();
				/*
				 * MediaType mediaType = MediaType.parse("application/json");
				 * okhttp3.RequestBody body =
				 * okhttp3.RequestBody.create(mediaType,
				 * "{\n\"branch_key\":\"key_live_feebgAAhbH9Tv85H5wLQhpdaefiZv5Dv\",\n\"data\":\"{\\\"name\\\": \\\"Alex\\\"}\"\n    \n}"
				 * ); Request request = new Request.Builder()
				 * .url("https://api.branch.io/v1/url") .post(body)
				 * .addHeader("content-type", "application/json")
				 * .addHeader("cache-control", "no-cache")
				 * .addHeader("postman-token",
				 * "7a4da102-dc81-31b9-3028-aedee4a30025") .build();
				 */
				try {
					Response response = client.newCall(request).execute();
					System.out.println(response.toString());
					String jsonData = response.body().string();
					branchResponse = new JSONObject(jsonData);
					generatedUrl = branchResponse.getString("url");
					System.out.println("URL: " + generatedUrl);
					referral.setLink(generatedUrl);
					referralService.addReferral(referral);

				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (JSONException e) {
					e.printStackTrace();
				}

				String reqBodyUrl = "{\n    \"longUrl\": \"http://ruralict.cse.iitb.ac.in/ruralict/app/code/"
						+ referralCode + "\"\n\n}";
				OkHttpClient clientNew = new OkHttpClient();
				MediaType mediaTypeNew = MediaType.parse("application/json");
				okhttp3.RequestBody bodyNew = okhttp3.RequestBody.create(mediaTypeNew, reqBodyUrl);
				Request requestNew = new Request.Builder()
						.url("https://www.googleapis.com/urlshortener/v1/url?key=" + API_KEY).post(bodyNew)
						.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache")
						.addHeader("postman-token", "71e06008-92c4-c2a3-5c6a-e718f5bc3e5b").build();
				try {
					Response response = clientNew.newCall(requestNew).execute();
					System.out.println(response.toString());
					String jsonData = response.body().string();
					shortenerResponse = new JSONObject(jsonData);
					shortendUrl = shortenerResponse.getString("id");
					System.out.println("Shortened URL: " + shortendUrl);

				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (JSONException e) {
					e.printStackTrace();
				}

				SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(destOrganization);
				String authId = smsApiKeys.getAuthId();
				String authToken = smsApiKeys.getAuthToken();
				String sourceNumber = smsApiKeys.getSourceNumber();
				RestAPI api = new RestAPI(authId, authToken, "v1");

				LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
				parameters.put("src", sourceNumber);
				parameters.put("dst", phonenumber);
				parameters.put("text", "Hello from Lokacart! \nYou have been referred to " + destOrganization.getName()
						+ " by " + refUser.getName() + "\nClick here to accept and use: " + shortendUrl);
				parameters.put("method", "GET");
				System.out.println("http://ruralict.cse.iitb.ac.in/ruralict/app/code/" + referralCode);
				try {
					MessageResponse msgResponse = api.sendMessage(parameters);
					System.out.println(msgResponse);
					System.out.println("Api ID : " + msgResponse.apiId);
					System.out.println("Message : " + msgResponse.message);
					if (msgResponse.serverCode == 202) {
						System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());

					} else {
						System.out.println(msgResponse.error);
					}
				} catch (PlivoException e) {
					System.out.println(e.getLocalizedMessage());
				}

				try {
					responseJsonObject.put("response", "success");
				} catch (JSONException e) {
					e.printStackTrace();
				}

			} else {
				// send error
				try {
					System.out.println("here");
					responseJsonObject.put("response", "Email ID and Phone number mismatch");
					responseJsonObject.put("error", "Email ID and Phone number mismatch");
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}

		return responseJsonObject.toString();
	}

}
