package app.business.controllers.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import app.business.services.OrganizationMembershipService;
import app.business.services.OrganizationService;
import app.business.services.ProductService;
import app.business.services.ProductTypeService;
import app.business.services.UserService;
import app.data.repositories.OrganizationRepository;
import app.entities.Organization;
import app.entities.Product;
import app.entities.ProductType;
import app.entities.User;

@RestController
@RequestMapping("/api/products/search/byType")
public class ProductTypeRestController {
	
	@Autowired
	OrganizationService organizationService;
	@Autowired
	ProductTypeService productTypeService;
	@Autowired
	UserService userService;
	@Autowired
	OrganizationRepository organizationRepository;
	@Autowired
	ProductService productService;
	
	@Autowired
	OrganizationMembershipService organizationMembershipService;
	
	@Transactional
	@RequestMapping(value = "/map",method = RequestMethod.GET )
	public HashMap<String, List<HashMap<String, String>>> productListByType(@RequestParam(value="orgabbr") String orgabbr){
		List<ProductType> productTypeList=productTypeService.getAllByOrganisationSortedBySequence(organizationService.getOrganizationByAbbreviation(orgabbr));
		List<Product> productList = productService.getProductList(organizationService.getOrganizationByAbbreviation(orgabbr));
		HashMap<String, List<HashMap<String, String>>> productTypeMap = new HashMap<String, List<HashMap<String, String>>>();
		Boolean stockManagement = organizationService.getOrganizationByAbbreviation(orgabbr).getStockManagement();
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		User currentUser = userService.getCurrentUser();
		JSONObject responseJsonObject = new JSONObject();
		JSONArray details = new JSONArray();
		/*
		if (organizationMembershipService.getUserOrganizationMembership(currentUser, organization) == null){
			try {
				responseJsonObject.put("status", "Failure");	
				responseJsonObject.put("error", "You are no longer a member");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		
		for (ProductType ptype: productTypeList)
		{
			//List<Object[]> iter= new ArrayList<Object[]>();
			JSONArray jsonArray = new JSONArray();	
			
			for(Product product: productList) {
				JSONObject jsonObject = new JSONObject();
				if(ptype.getName().equals(product.getProductType().getName()) && (product.getStatus() == 1)) {
					System.out.println(ptype.getName());
					try {
					jsonObject.put("id", Integer.toString(product.getProductId()));
					jsonObject.put("name", product.getName());
					jsonObject.put("quantity", Integer.toString((product.getQuantity())));
					jsonObject.put("unitRate", Float.toString(product.getUnitRate()));
					jsonObject.put("imageUrl", product.getImageUrl());
					jsonObject.put("audioUrl", product.getAudioUrl());	
					jsonObject.put("stockManagement", stockManagement.toString());
					jsonObject.put("description", product.getDescription());
					jsonArray.put(jsonObject);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				
			}
			}
			try {
				JSONObject temp = new JSONObject();
				temp.put(ptype.getName(), jsonArray);
				details.put(temp);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		try {
			responseJsonObject.put("products", details);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
			return responseJsonObject.toString();   */
			
			
		 
		
		for (ProductType ptype: productTypeList)
		{
			//List<Object[]> iter= new ArrayList<Object[]>();
			System.out.println(ptype.getName());
			List<HashMap<String, String>> Listmap=new ArrayList<HashMap<String, String>>();
			HashMap<String, HashMap<String, String>> productmap=new HashMap<String, HashMap<String, String>>();
			for(Product product: productList)
			{
				HashMap<String, String> map=new HashMap<String, String>();
				if(ptype.getName().equals(product.getProductType().getName()) && (product.getStatus() == 1))
				{
					map.put("id", Integer.toString(product.getProductId()));
					map.put("name", product.getName());
					map.put("quantity", Integer.toString((product.getQuantity())));
					map.put("unitRate", Float.toString(product.getUnitRate()));
					map.put("imageUrl", product.getImageUrl());
					map.put("audioUrl", product.getAudioUrl());
					map.put("stockManagement", stockManagement.toString());
					if(product.getDescription() == null)
						map.put("description", "");
					else
						map.put("description", product.getDescription());
					Listmap.add(map);
				}
			}
			productTypeMap.put(ptype.getName(), Listmap);
		}
		return productTypeMap;
	
	}
	
	
	
	@Transactional
	@RequestMapping(value = "/mapnew",method = RequestMethod.GET )
	public String productListByTypeNew(@RequestParam(value="orgabbr") String orgabbr){
		List<ProductType> productTypeList=productTypeService.getAllByOrganisationSortedBySequence(organizationService.getOrganizationByAbbreviation(orgabbr));
		List<Product> productList = productService.getProductList(organizationService.getOrganizationByAbbreviation(orgabbr));
		HashMap<String, List<HashMap<String, String>>> productTypeMap = new HashMap<String, List<HashMap<String, String>>>();
		Boolean stockManagement = organizationService.getOrganizationByAbbreviation(orgabbr).getStockManagement();
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		User currentUser = userService.getCurrentUser();
		JSONObject responseJsonObject = new JSONObject();
		JSONArray details = new JSONArray();
		
		if (organizationMembershipService.getUserOrganizationMembership(currentUser, organization) == null){
			try {
				responseJsonObject.put("status", "Failure");	
				responseJsonObject.put("error", "You are no longer a member");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		
		for (ProductType ptype: productTypeList)
		{
			//List<Object[]> iter= new ArrayList<Object[]>();
			JSONArray jsonArray = new JSONArray();	
			
			for(Product product: productList) {
				JSONObject jsonObject = new JSONObject();
				if(ptype.getName().equals(product.getProductType().getName()) && (product.getStatus() == 1)) {
					System.out.println(ptype.getName());
					try {
					jsonObject.put("id", Integer.toString(product.getProductId()));
					jsonObject.put("name", product.getName());
					jsonObject.put("quantity", Integer.toString((product.getQuantity())));
					jsonObject.put("unitRate", Float.toString(product.getUnitRate()));
					jsonObject.put("imageUrl", product.getImageUrl());
					jsonObject.put("audioUrl", product.getAudioUrl());	
					jsonObject.put("stockManagement", stockManagement.toString());
					if(product.getDescription() == null)
						jsonObject.put("description", "");
					else
						jsonObject.put("description", product.getDescription());
					jsonArray.put(jsonObject);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				
			}
			}
			try {
				JSONObject temp = new JSONObject();
				temp.put(ptype.getName(), jsonArray);
				details.put(temp);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		try {
			responseJsonObject.put("products", details);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
			return responseJsonObject.toString();  
	}
	
	
	
	@Transactional
	@RequestMapping(value = "/byname",method = RequestMethod.POST ,produces="application/json" )
	public String updatedPrice(@RequestBody String requestBody)
	{
		JSONObject responseJsonObject = new JSONObject();
		JSONObject jsonObject=null;
		JSONArray productListJsonArray = null;
		JSONArray proArray= new JSONArray();
		int org_id = 0;
		int flag=0;
		try {
			jsonObject = new JSONObject(requestBody);
			org_id = jsonObject.getInt("org_id");
			productListJsonArray = jsonObject.getJSONArray("product_list");
			responseJsonObject.put("status", "updated");
			Organization organization= organizationRepository.findOne(org_id); //Finding organization of the product name
			Boolean stockManagement = organization.getStockManagement();
			responseJsonObject.put("stockManagement",stockManagement.toString());
			for (int i=0;i<productListJsonArray.length();i++)
			{
				 try {
					JSONObject productObj = productListJsonArray.getJSONObject(i);
					String product_name = productObj.getString("product_name");
					float price = Float.parseFloat((productObj.getString("price")));
					if(organization==null)
					{
						responseJsonObject.put("Status", "Failure");
						responseJsonObject.put("Error", "Organization with Id "+org_id+" does not exists");
						return responseJsonObject.toString();
					}
					else
					{
						Product product = productService.getProductByNameAndOrg(product_name, organization); 
						JSONObject up_product= new JSONObject();
						
							flag=1;
							up_product.put("product_name", product.getName());
							up_product.put("price", product.getUnitRate());
							up_product.put("quantity", Integer.toString(product.getQuantity()));
							up_product.put("audioUrl", product.getAudioUrl());
							up_product.put("imageUrl", product.getImageUrl());
							if(product.getDescription() == null)
								up_product.put("description", "");
							else
								up_product.put("description", product.getDescription());
							proArray.put(up_product);
							responseJsonObject.put("status", "updated");
							responseJsonObject.put("products", proArray);
						
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			if(flag==0)
				responseJsonObject.put("status", "no change");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	

	
	
	
}
