package app.business.controllers.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import javax.transaction.Transactional;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.plivo.helper.api.client.RestAPI;
import com.plivo.helper.api.response.message.MessageResponse;
import com.plivo.helper.exception.PlivoException;

import app.business.services.GcmTokensService;
import app.business.services.OrganizationMembershipService;
import app.business.services.OrganizationService;
import app.business.services.SmsApiKeysService;
import app.business.services.UserPhoneNumberService;
import app.entities.GcmTokens;
import app.entities.Organization;
import app.entities.OrganizationMembership;
import app.entities.SmsApiKeys;
import app.util.GcmRequest;

@RestController
@RequestMapping("/api")
public class MessagingRestController {

	@Autowired 
	UserPhoneNumberService userPhoneNumberService;
	
	@Autowired
	OrganizationMembershipService organizationMembershipService;
	
	@Autowired
	SmsApiKeysService smsApiKeysService;

	
	@Autowired
	GcmTokensService gcmTokensService;
	
	@Autowired
	OrganizationService organizationService;
	
	public List <String> getTargetConsumerDevices(Organization organization) {
		List <String>androidTargets = new ArrayList<String>();
		try{
			List <OrganizationMembership> membershipList = organizationMembershipService.getOrganizationMembershipList(organization);
			Iterator<OrganizationMembership> membershipIter = membershipList.iterator();
			while (membershipIter.hasNext()) {
				OrganizationMembership organizationMembership = membershipIter.next();
				String phonenumber = organizationMembership.getUser().getUserPhoneNumbers().get(0).getPhoneNumber();
				List<GcmTokens> gcmTokens = gcmTokensService.getListByPhoneNumber(phonenumber);
				Iterator <GcmTokens> iter = gcmTokens.iterator();
				while(iter.hasNext()) {
					androidTargets.add(iter.next().getToken());
				}	
			}
		}
		catch(Exception e) {
			System.out.println("error in token retreival");
		}
		return androidTargets;
	}
	
	
	
	
	public List <String> getTargetConsumerDevicesSMS(Organization organization) {
		List <String>androidTargets = new ArrayList<String>();
		try{
			List <OrganizationMembership> membershipList = organizationMembershipService.getOrganizationMembershipList(organization);
			Iterator<OrganizationMembership> membershipIter = membershipList.iterator();
			while (membershipIter.hasNext()) {
				OrganizationMembership organizationMembership = membershipIter.next();
				String phonenumber = organizationMembership.getUser().getUserPhoneNumbers().get(0).getPhoneNumber();
				
					androidTargets.add(phonenumber);
					
			}
		}
		catch(Exception e) {
			System.out.println("error in token retreival");
		}
		return androidTargets;
	}
	
	
	
	@Transactional
	@RequestMapping(value = "/sendbroadcast",method = RequestMethod.POST)
	public @ResponseBody String sendPromotionalBroadcast(@RequestBody String requestBody) {
		JSONObject jsonObject = null;;
		JSONObject responseJsonObject = new JSONObject();
		Organization organization =  null;
		String message = null;
		try {
			jsonObject = new JSONObject(requestBody);
			String orgabbr = jsonObject.getString("orgabbr");
			message = jsonObject.getString("message");
			organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		}
		catch(JSONException e) {
			e.printStackTrace();
			try {
				responseJsonObject.put("response", "JSON format error");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		List <String> androidTargets = getTargetConsumerDevices(organization);
		if (androidTargets.size() > 0) {
			GcmRequest gcmRequest = new GcmRequest();
			System.out.println("Message: "+message);
			gcmRequest.consumerPromoBroadcast(message,organization.getName(), androidTargets, "5");
		}
		try {
			responseJsonObject.put("response", "Message successfully broadcasted");
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	
	@Transactional
	@RequestMapping(value = "/sendsmsbroadcast",method = RequestMethod.POST)
	public @ResponseBody String sendSMSBroadcast(@RequestBody String requestBody) {
		JSONObject jsonObject = null;;
		JSONObject responseJsonObject = new JSONObject();
		Organization organization =  null;
		String message = null;
		try {
			jsonObject = new JSONObject(requestBody);
			String orgabbr = jsonObject.getString("orgabbr");
			message = jsonObject.getString("message");
			organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		}
		catch(JSONException e) {
			e.printStackTrace();
			try {
				responseJsonObject.put("response", "JSON format error");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		List <String> androidTargets = getTargetConsumerDevicesSMS(organization);
		System.out.println(""+androidTargets.size());
		
		SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(organization);
		String authId = smsApiKeys.getAuthId();
		String authToken = smsApiKeys.getAuthToken();
		String sourceNumber = smsApiKeys.getSourceNumber();
		RestAPI api = new RestAPI(authId, authToken, "v1");
		String numbersFormat="";
		int i=0;
		while(i<androidTargets.size()){
			numbersFormat=numbersFormat+androidTargets.get(i++).toString()+"<";
		}
		
		numbersFormat=numbersFormat+"919941375176";
		System.out.println(numbersFormat);
		LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
		parameters.put("src", sourceNumber);
		parameters.put("dst",numbersFormat );
		parameters.put("text",message+organization.getAbbreviation().toString());
		parameters.put("method"	, "GET");
		
		try {
			MessageResponse msgResponse = api.sendMessage(parameters);
			System.out.println(msgResponse);
			System.out.println("Api ID : " + msgResponse.apiId);
			System.out.println("Message : " + msgResponse.message);
			if (msgResponse.serverCode == 202) {
				//System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());

			} else {
				System.out.println(msgResponse.error);
			}
		} catch (PlivoException e) {
			System.out.println(e.getLocalizedMessage());
		}
		
		
		
		
		
		
		try {
			responseJsonObject.put("response", "Message successfully broadcasted");
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	
}
