package app.business.controllers.rest;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TimeZone;

import javax.transaction.Transactional;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.plivo.helper.api.client.RestAPI;
import com.plivo.helper.api.response.message.MessageResponse;
import com.plivo.helper.exception.PlivoException;

import app.business.services.BillLayoutSettingsService;
import app.business.services.GcmTokensService;
import app.business.services.OrderItemService;
import app.business.services.OrderService;
import app.business.services.OrdersStoreService;
import app.business.services.OrganizationMembershipService;
import app.business.services.OrganizationService;
import app.business.services.PresetQuantityService;
import app.business.services.ProductService;
import app.business.services.SmsApiKeysService;
import app.business.services.UserPhoneNumberService;
import app.business.services.UserService;
import app.business.services.message.MessageService;
import app.data.repositories.BillLayoutSettingsRepository;
import app.data.repositories.BinaryMessageRepository;
import app.data.repositories.GroupRepository;
import app.data.repositories.OrderItemRepository;
import app.data.repositories.OrderRepository;
import app.data.repositories.OrganizationRepository;
import app.entities.BillLayoutSettings;
import app.entities.GcmTokens;
import app.entities.Group;
import app.entities.Order;
import app.entities.OrderItem;
import app.entities.OrdersStore;
import app.entities.Organization;
import app.entities.OrganizationMembership;
import app.entities.PresetQuantity;
import app.entities.Product;
import app.entities.SmsApiKeys;
import app.entities.User;
import app.entities.message.BinaryMessage;
import app.entities.message.Message;
import app.util.GcmRequest;
import app.util.SendBill;
import app.util.SendMail;

@RestController
@RequestMapping("/api")
public class OrderRestController {
	
	@Autowired
	OrganizationService organizationService;
	
	@Autowired
	MessageService messageService;
	
	@Autowired
	PresetQuantityService presetQuantityService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	OrderRepository orderRepository;
	
	@Autowired
	OrganizationRepository organizationRepository;
	
	@Autowired
	OrderItemRepository orderItemRepository;
	
	@Autowired
	BinaryMessageRepository binaryMessageRepository;
	
	@Autowired
	GroupRepository groupRepository;

	@Autowired
	UserService userService;
	
	@Autowired
	OrderService orderService;
	
	@Autowired
	BillLayoutSettingsRepository billLayoutSettingsRepository;
	
	@Autowired 
	UserPhoneNumberService userPhoneNumberService;
	
	@Autowired
	OrganizationMembershipService organizationMembershipService;
	
	@Autowired
	GcmTokensService gcmTokensService;
	
	@Autowired
	BillLayoutSettingsService billLayoutSettingsService;
	
	@Autowired
	OrderItemService orderItemService;
	
	@Autowired
	SmsApiKeysService smsApiKeysService;
	
	@Autowired
	OrdersStoreService ordersStoreService; 
	
	
		public HashMap<String, Integer> dashBoardLocal(String orgabbr) throws ParseException {

		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		Group g= organizationService.getParentGroup(organization);
		List<Message> messageapppro=messageService.getMessageListByOrderStatus(g, "binary", "processed");
		List<Message> messageappnew=messageService.getMessageListByOrderStatus(g, "binary", "saved");
		List<Message> messageappcan=messageService.getMessageListByOrderStatus(g, "binary", "cancelled");
		HashMap<String, Integer> dashmap = new HashMap<String, Integer>();
		dashmap.put("saved", messageappnew.size());
		dashmap.put("processed", messageapppro.size());
		dashmap.put("cancelled", messageappcan.size());
		
		List<OrganizationMembership> membershipListpending = organizationMembershipService.getOrganizationMembershipListByStatus(organization, 0);
		List<OrganizationMembership> membershipListapproved = organizationMembershipService.getOrganizationMembershipListByStatus(organization, 1);
		dashmap.put("totalUsers", membershipListapproved.size());
		dashmap.put("pendingUsers", membershipListpending.size());
		int todayUsers=0;
		for(OrganizationMembership membership : membershipListpending)
		{

			User user = membership.getUser();
		
			try
			{
				Timestamp time = user.getTime();
				
				Calendar cal= Calendar.getInstance();
				cal.clear(Calendar.HOUR_OF_DAY);
				cal.clear(Calendar.AM_PM);
				cal.clear(Calendar.MINUTE);
				cal.clear(Calendar.SECOND);
				cal.clear(Calendar.MILLISECOND);
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");      
			    java.util.Date dateWithoutTime = sdf.parse(sdf.format(new java.util.Date()));
				if(time.after(dateWithoutTime))
				{
					todayUsers=todayUsers+1;
				}
			}
			catch(NullPointerException | ParseException e)
			{
				System.out.println("User name not having his timestamp recorded is: " + user.getName() + " having userID: " + user.getUserId());
			}
		}
		dashmap.put("newUsersToday",todayUsers);
		return dashmap;
	}
		
		
		
	public List <String> getTargetConsumerDevices(String phonenumber) {
		List <String>androidTargets = new ArrayList<String>();

		try{
			List<GcmTokens> gcmTokens = gcmTokensService.getListByPhoneNumber(phonenumber);
			Iterator <GcmTokens> iter = gcmTokens.iterator();
			while(iter.hasNext()) {
				androidTargets.add(iter.next().getToken());
			}
			}
			catch(Exception e){
				System.out.println("no token for number: "+phonenumber);
			}
		return androidTargets;
	}
	
	public List <String> getTargetDevices (Organization organization)  {
			
		List<OrganizationMembership> organizationMembership = organizationMembershipService.getOrganizationMembershipListByIsAdmin(organization, true);
		List<String> phoneNumbers = new ArrayList<String>();
		Iterator <OrganizationMembership> membershipIterator = organizationMembership.iterator();
		while (membershipIterator.hasNext()) {
			OrganizationMembership membership = membershipIterator.next();
			User user = membership.getUser();
			phoneNumbers.add(userPhoneNumberService.getUserPrimaryPhoneNumber(user).getPhoneNumber());
		}
		Iterator <String> iterator = phoneNumbers.iterator();
		List <String>androidTargets = new ArrayList<String>();
		while(iterator.hasNext()) {
			String number = iterator.next();
			try{
			List<GcmTokens> gcmTokens = gcmTokensService.getListByPhoneNumber(number);
			Iterator <GcmTokens> iter = gcmTokens.iterator();
			while(iter.hasNext()) {
			
			androidTargets.add(iter.next().getToken());
			}
			}
			catch(Exception e){
				System.out.println("no token for number: "+number);
			}
		}
		return androidTargets;
	}
	
	@Transactional
	
	@RequestMapping(value = "/orders/add",method = RequestMethod.POST )
	public @ResponseBody String addOrders(@RequestBody String requestBody){
	//	HashMap<String,String> response= new HashMap<String, String>();
		JSONObject jsonObject = null;
		JSONObject response = new JSONObject();
		String organizationabbr = null;
		String groupname=null;
		Organization organization= null;
		User currentUser = userService.getCurrentUser();
		float netAmount =0 ,total =0;
		int flag =0, countProd=0, ct = 0;
		ArrayList <String> errorProduct = new ArrayList<String> ();
		String remProduct = "";
		try {
			JSONArray orderProducts = null;
			jsonObject = new JSONObject(requestBody);
			organizationabbr=jsonObject.getString("orgabbr");
			groupname=jsonObject.getString("groupname");
			JSONArray updatePrice = new JSONArray();
			String prodName ="";
			organization= organizationRepository.findByAbbreviation(organizationabbr);
			Boolean stockManagement = organization.getStockManagement();
			if (organizationMembershipService.getUserOrganizationMembership(currentUser, organization) == null) {
				response.put("status", "Failure");
				response.put("error", "You are no longer a member");
				return response.toString();
			}
			//Quantity verification
			orderProducts = jsonObject.getJSONArray("orderItems");
			for (int i = 0; i < orderProducts.length(); i++) {
				  
				JSONObject row = orderProducts.getJSONObject(i);
			    String productname=row.getString("name");
			    int productQuantity =row.getInt("quantity");
			    float price = -1;
			    try {
			    price= (float) row.getDouble("price");
			    total += price * productQuantity;
			    System.out.println(price);
			    }
			    catch(Exception e) {
			    	//uncaught, but not untamed!
			    }
			    Product product=productService.getProductByNameAndOrg(productname,organization);
			    if (product == null) {
			    	if (ct != 0)
			    		remProduct += ", ";
			    	remProduct += (productname);
			    	++ct;
			    	continue;
			    }
			    if (product.getUnitRate() != price && price != -1) {
			    	if(countProd != 0)
			    		prodName += ", ";
			    	flag =1;
			    	++countProd;
			    	prodName += product.getName();
			    	JSONObject object = new JSONObject();
			    	object.put("id", Integer.toString(product.getProductId()));
			    	object.put("name", product.getName());
			    	object.put("quantity", Integer.toString((product.getQuantity())));
			    	object.put("unitRate", Float.toString(product.getUnitRate()));
			    	object.put("imageUrl", product.getImageUrl());
			    	object.put("audioUrl", product.getAudioUrl());
			    	object.put("stockManagement", stockManagement.toString());
			    	object.put("description", product.getDescription());
			    	updatePrice.put(object);
			    }
				if(organization.getStockManagement() == true) {

			    int currentQuantity = product.getQuantity();
			    if (currentQuantity < productQuantity) {
			    	errorProduct.add(product.getName() + " -Max: "+ currentQuantity+ " units");
			    }
				}
			}
			if (!remProduct.isEmpty()) {
				response.put("error", "Products have been removed by the supplier: "+ remProduct);
				response.put("status", "Failure");
				return response.toString();
			}
			
			if (flag == 1){
				response.put("status", "Price increased for "+countProd +" products: "+prodName);
				response.put("products", updatePrice);
				return response.toString();
			}
			if(!errorProduct.isEmpty()){
				throw new Exception();
			}
			if (total < organization.getOrderThreshold()) {
				response.put("error", "Minimum order for this organization is: Rs."+organization.getOrderThreshold());
				response.put("status", "Failure");
				return response.toString();
			}
			
			
		} 
			catch (JSONException e) {
			e.printStackTrace();
		}
		catch(Exception e1) {
			e1.printStackTrace();
			try {
				response.put("status", "Failure");
			} catch (JSONException e2) {
				e2.printStackTrace();
			}
			String error = new String();
			JSONArray errorArray = new JSONArray();
			Iterator <String> iterator = errorProduct.iterator();
			/*while(iterator.hasNext()) {
			//	error = error + iterator.next() +", ";
				errorArray.put(iterator.next());
			} */
			String output = "";
			int count =0;
			while(iterator.hasNext()) {
				if (count > 0)
					output += ", ";
				output = output + iterator.next();
				++count;
			}
			try {
				response.put("error", "Insufficient stock for: "+output);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return response.toString();
		}
		Order order = new Order();
		//Organization organization= organizationRepository.findByAbbreviation(organizationabbr);
		order.setOrganization(organization);
		order.setStatus("saved");
		order=orderRepository.save(order);
		List<OrderItem> orderItems= new ArrayList<OrderItem>();
		try {
			JSONArray orderItemsJSON = jsonObject.getJSONArray("orderItems");
			for (int i = 0; i < orderItemsJSON.length(); i++) {
			  
				JSONObject row = orderItemsJSON.getJSONObject(i);
			    String productname=row.getString("name");
			    int productQuantity =row.getInt("quantity");
			    Product product=productService.getProductByNameAndOrg(productname,organization);
			    int currentQuantity = product.getQuantity();
			    OrderItem orderItem= new OrderItem();
			    orderItem.setProduct(product);
			    orderItem.setQuantity(productQuantity);	
			    if (organization.getStockManagement() == true) {
			    	product.setQuantity(currentQuantity - productQuantity);
			    	productService.addProduct(product);
			    }
			    netAmount = netAmount + (productQuantity * product.getUnitRate());
			    orderItem.setUnitRate(product.getUnitRate());
			    orderItem.setOrder(order);
			    orderItem=orderItemRepository.save(orderItem);
			    orderItems.add(orderItem);
			}
			
			System.out.println(orderItemsJSON.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
	
		order.setOrderItems(orderItems);
		BinaryMessage bmessage= new BinaryMessage();
		bmessage.setTime(new Timestamp((new Date()).getTime()));
		bmessage.setOrder(order);
		bmessage.setGroup(groupRepository.findByNameAndOrganization(groupname, organization));
		bmessage.setUser( userService.getCurrentUser());
		bmessage.setMode("app");
		bmessage.setType("order");
		binaryMessageRepository.save(bmessage);
		order.setMessage(bmessage);
		orderRepository.save(order);
		try {
			response.put("orderId",new Integer(order.getOrderId()).toString());
			response.put("status", "Success");

		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		String email=order.getMessage().getUser().getEmail();
		String phonenumber = order.getMessage().getUser().getUserPhoneNumbers().get(0).getPhoneNumber();
		List <String> androidTargets = getTargetDevices(organization);
		if(androidTargets.size()>0) {
		GcmRequest gcmRequest = new GcmRequest();
		HashMap<String,Integer>dashData = null;
		try {
			dashData = dashBoardLocal(organizationabbr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		gcmRequest.broadcast(userService.getCurrentUser().getName()+" has placed an order", "New order", androidTargets,0);
		System.out.println(organizationabbr);
		gcmRequest.broadcast(androidTargets,organizationabbr,dashData);
		}
		String orderString = "";
		Iterator <OrderItem> itemsIterator = orderItems.iterator();
		orderString = "<ol>";
		while (itemsIterator.hasNext()) {
			OrderItem orderItem = itemsIterator.next();
			orderString += "<li>";
			orderString +=orderItem.getProduct().getName()+" --- "+String.valueOf(orderItem.getQuantity())+" units<br />";
			orderString += "</li>";
		}
		orderString+= "</ol>";
		SendMail.sendMail(email, "Lokacart: Order Placed Successfully" , "<p>Dear User <br /><br />Your order has been placed successfully with order ID: " + new Integer(order.getOrderId()).toString()+".<br />The items ordered were: </p><p style='text-align:center'>"+orderString+"</p><p><br />The total amount is: Rs."+String.format("%.2f", netAmount)+" .</p>");
		if(organization.getAbbreviation().equals("NatG"))
			SendMail.sendMail("vishalghodke@gmail.com", "Lokacart: Order Placed", "Dear Admin,\nCustomer "+order.getMessage().getUser().getName()+" has placed an order of order id "+order.getOrderId()+".\n\nThankyou\nLokacart Team\n");
		
		SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(organization);
		String authId = smsApiKeys.getAuthId();
		String authToken = smsApiKeys.getAuthToken();
		String sourceNumber = smsApiKeys.getSourceNumber();
		RestAPI api = new RestAPI(authId, authToken, "v1");

		LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
		parameters.put("src", sourceNumber);
		parameters.put("dst", phonenumber);
		parameters.put("text", "Hello from Lokacart! \nThank you for placing an order with "+organization.getName()+". The bill amount is Rs."+String.format("%.2f", netAmount)+".");
		parameters.put("method", "GET");
		try {
			MessageResponse msgResponse = api.sendMessage(parameters);
			System.out.println(msgResponse);
			System.out.println("Api ID : " + msgResponse.apiId);
			System.out.println("Message : " + msgResponse.message);
			if (msgResponse.serverCode == 202) {
				System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());
				response.put("text", "Confirmation sent");

			} else {
				System.out.println(msgResponse.error);
			}
		} catch (PlivoException e) {
			System.out.println(e.getLocalizedMessage());
		} catch (JSONException e) {
			e.printStackTrace();
		}		
		return response.toString();
	}
	
	@Transactional
	@RequestMapping(value = "{org}/viewbill/{orderId}",method = RequestMethod.GET)
	public String viewBill(@PathVariable String org, @PathVariable int orderId){
		
		Order order = orderRepository.findOne(orderId);
		List <OrdersStore> ordersStore = null;
		Organization organization= order.getOrganization();
		String organizationabbr = organization.getAbbreviation();
		BillLayoutSettings billLayoutSetting = billLayoutSettingsRepository.findByOrganization(organization);
		String msg = null;
		if (order.getStatus().equals("delivered")) {
			ordersStore = ordersStoreService.getStoredItems(order);
			msg = SendBill.returnDeliveredBill(order,ordersStore, organization, billLayoutSetting);	
		}
		else {
			msg = SendBill.returnBill(order, organization, billLayoutSetting);
		}
		System.out.println(msg);
		return msg;
	}
	
	
	
	@Transactional
	@RequestMapping(value = "{org}/viewbilldelivered/{orderId}",method = RequestMethod.GET)
	public String viewBillDelivered(@PathVariable String org, @PathVariable int orderId){
		
		Order order = orderRepository.findOne(orderId);
		List <OrdersStore> ordersStore = ordersStoreService.getStoredItems(order);
		Organization organization= order.getOrganization();
		String organizationabbr = organization.getAbbreviation();
		BillLayoutSettings billLayoutSetting = billLayoutSettingsRepository.findByOrganization(organization);
		String msg = SendBill.returnDeliveredBill(order,ordersStore, organization, billLayoutSetting);
		System.out.println(msg);
		return msg;
	}
	@Transactional
	@RequestMapping(value = "/orders/paid/{orderId}",method = RequestMethod.GET ,produces="application/json" )
	public String updatePaidOrder(@PathVariable int orderId) {
		JSONObject jsonResponseObject = new JSONObject();
		Order order = orderService.getOrder(orderId);
		if (order == null){
			try {
				jsonResponseObject.put("response", "Failure");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		order.setIsPaid(true);
		order.setText("Paid");
		orderService.addOrder(order);
		try {
			jsonResponseObject.put("response", "Success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	
	
	

	@Transactional
	@RequestMapping(value = "/orders/update/{orderId}",method = RequestMethod.POST ,produces="application/json" )
	public String updateOrders(@PathVariable int orderId,@RequestBody String requestBody)
	{
		JSONObject response= new JSONObject();
		JSONObject jsonObject = null;
		String status=null;
		String comments=null;
		String orgabr=null;
		//int flag = 0;
		List<String> updated = new ArrayList<String>();
		JSONArray orderItemsJSON = null;
		try {
			jsonObject = new JSONObject(requestBody);
			status=jsonObject.getString("status");
			comments=jsonObject.getString("comments");
			orderItemsJSON = jsonObject.getJSONArray("orderItems");

			//orgabr= jsonObject.getString("orgabbr");
		} catch (JSONException e) {
			//Uncaught but not untamed :-)			
			//return "Error";
		}
		System.out.println(status);

		if(orderRepository.findOne(orderId)==null)
		{
			try {
				response.put("status", "Error");
				response.put("error", "No Order of the following Id found");
				return response.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}	

		Order order = orderRepository.findOne(orderId);
		Organization organization= order.getOrganization();
		if (order.getStatus().equals("cancelled")){
			try {
				response.put("status", "error");
			response.put("error", "Order has been cancelled. Please sync app immediately");
			return response.toString();
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		//check to prevent double cancellation
		if (order.getStatus().equals("cancelled")) {
			try {
				response.put("status", "error");
				response.put("error", "Order already cancelled. Please sync app immediately");
				return response.toString();
			}
			catch(JSONException e) {
				e.printStackTrace();
			}
		}
		//delivered state procedure
		if (status.equals("delivered")) {
			if (order.getStatus().equals("delivered")) {
				try {
					response.put("status", "error");
					response.put("error", "Order has already been delivered");
					return response.toString();		
				}
				catch(Exception e) {
					e.printStackTrace();
				}
			} 
			else if (order.getStatus().equals("cancelled")) {
				try {
					response.put("status", "error");
					response.put("error", "Order has already been cancelled");
					return response.toString();		
				}
				catch(Exception e) {
					e.printStackTrace();
				}
			}
			System.out.println("Inside delivery part");
			order.setStatus("delivered");
			List <OrderItem> orderItemsNew = orderService.getOrder(orderId).getOrderItems();
			Iterator <OrderItem> itemsIterator = orderItemsNew.iterator();
			while(itemsIterator.hasNext()) {
			OrderItem orderItem = itemsIterator.next();
			OrdersStore ordersStore = new OrdersStore();
			ordersStore.setOrder(order);
			ordersStore.setProductName(orderItem.getProduct().getName());
			ordersStore.setQuantity(orderItem.getQuantity());
			ordersStore.setUnitRate(orderItem.getUnitRate());
			ordersStoreService.addOrdersStore(ordersStore);
			orderItem.setProduct(null);

			}
			
			orderService.addOrder(order);
			SendMail.sendMail(order.getMessage().getUser().getEmail(), "Lokacart: Order #"+order.getOrderId()+" has been Delivered", "<p>Dear User<br /><br />Your order with order ID "+order.getOrderId()+" has been delivered to your address.<<br /><br />We hope to serve you again!</p>");
			try {
				response.put("status", "Success");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return response.toString();
		}
		
		//cancellation procedure
		if (status.equals("cancelled")) {
			if (order.getStatus().equals("processed")) {
				try {	
				response.put("status", "error");
					response.put("error", "Order has already been processed by the organisation");
					return response.toString();

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if (order.getStatus().equals("delivered")) {
				try {	
				response.put("status", "error");
					response.put("error", "Order has already been delivered to your address");
					return response.toString();

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			try {
				response.put("status", "Success");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			order.setStatus(status);
			Message message = order.getMessage();
			message.setComments(comments);
			messageService.addMessage(message);
			String orderString = "";
			
			List <OrderItem> orderItemsNew = orderService.getOrder(orderId).getOrderItems();
			Iterator <OrderItem> itemsIterator = orderItemsNew.iterator();
			float netAmount=0;
			orderString ="<ol>";
			while (itemsIterator.hasNext()) {
				OrderItem orderItem = itemsIterator.next();
				OrdersStore ordersStore = new OrdersStore();
				ordersStore.setOrder(order);
				ordersStore.setProductName(orderItem.getProduct().getName());
				ordersStore.setQuantity(orderItem.getQuantity());
				ordersStore.setUnitRate(orderItem.getUnitRate());
				ordersStoreService.addOrdersStore(ordersStore);
				orderString += "<li>";
				orderString +=orderItem.getProduct().getName()+" --- "+String.valueOf(orderItem.getQuantity())+" units\n";
				orderString += "</li>";
				netAmount += (orderItem.getQuantity() * orderItem.getUnitRate());
			}
			orderString += "</ol>";
			List<OrderItem> orderItems = orderService.getOrder(orderId).getOrderItems();
			Iterator <OrderItem> iterator = orderItems.iterator();
			while (iterator.hasNext()) {
				OrderItem orderItem = iterator.next();
				Product product = orderItem.getProduct();
				product.setQuantity(product.getQuantity() + orderItem.getQuantity());
				productService.addProduct(product);
				System.out.println("Updating product on cancellation - "+product.getName());
				orderItem.setProduct(null);
			}
			SendMail.sendMail(order.getMessage().getUser().getEmail(), "Lokacart: Order Cancellation Acknowledgement", "<p>Dear User<br /><br />Your order with order ID "+order.getOrderId()+" has been successfully cancelled.<br />The items ordered were: </p><p style='text-align:center'>"+orderString+"</p><p><br />The total amount is: Rs."+String.format("%.2f", netAmount)+". <br /><br />We hope to serve you again!</p>");
			orderRepository.save(order);
			return response.toString();
		}
		
		
		String organizationabbr = organization.getAbbreviation();
		if( order.getStatus().equals("processed")) {
			try {
				response.put("status", "Failure");
				response.put("error","Order cannot be modified. Please sync app immediately");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return response.toString();
		}
		//Will be used later when comments are added while ordering.
		if(comments != null) {
		if(!comments.equals("null"))
		{
			BinaryMessage message=(BinaryMessage)order.getMessage();
			message.setComments(comments);
			binaryMessageRepository.save(message);
		}
		}
		System.out.println(order.getStatus());
		if( order.getStatus().equals("processed")) {
			try {
				System.out.println("inside");
				response.put("status", "Order cannot be modified. Please sync app immediately");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return response.toString();
		}
		else if ( order.getStatus().equals("delivered")) {
			try {
				System.out.println("inside");
				response.put("status", "Order cannot be modified. Please sync app immediately");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return response.toString();
		}
		if(orderItemsJSON!=null)
		{
			List <OrderItem> list = orderService.getOrder(orderId).getOrderItems();
			ArrayList <String> errorProduct = new ArrayList<String> ();
			
			try{
				if(organization.getStockManagement() == true) {
					//Quantity verification
					JSONArray orderProducts = jsonObject.getJSONArray("orderItems");
					for (int i = 0; i < orderProducts.length(); i++) {
						 OrderItem orderItem = null;
						
						JSONObject row = orderProducts.getJSONObject(i);
					    String productname=row.getString("name");
					    int productQuantity =row.getInt("quantity");
					    for(int j=0;j<list.size();++j) {
							if(list.get(j).getProduct().getName().equals(productname))
								orderItem = list.get(j);
						}
					    Product product=productService.getProductByNameAndOrg(productname,organization);
					    int currentQuantity = product.getQuantity();
					    if (currentQuantity < (productQuantity - orderItem.getQuantity())) {
					    	errorProduct.add(product.getName()+ " -Max: "+ (currentQuantity+ orderItem.getQuantity())+ " units");
					    }
					}
					if(!errorProduct.isEmpty()){
						throw new Exception();
					}
				}
			} catch (JSONException e2) {
				e2.printStackTrace();
			}
			catch(Exception e1) {
				e1.printStackTrace();
				try {
					response.put("status", "Failure");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				String error = new String();
				JSONArray errorArray = new JSONArray();
				Iterator <String> iterator = errorProduct.iterator();
				String output = "";
				int count =0;
				while(iterator.hasNext()) {
				//	error = error + iterator.next() +", ";
					//errorArray.put(iterator.next());
					if (count > 0)
						output += ", ";
					output = output + iterator.next();
					++count;
				}
				try {
					response.put("error",  "Insufficient stock for: "+output);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return response.toString();
			}	
			try {
			if(organization.getStockManagement() == true) {
				//Quantity verification
				JSONArray orderProducts = jsonObject.getJSONArray("orderItems");
				for (int i = 0; i < orderProducts.length(); i++) {
					 OrderItem orderItem = null;
					
					JSONObject row = orderProducts.getJSONObject(i);
				    String productname=row.getString("name");
				    int productQuantity =row.getInt("quantity");
				    for(int j=0;j<list.size();++j) {
						if(list.get(j).getProduct().getName().equals(productname))
						{
							orderItem = list.get(j);
							updated.add(list.get(j).getProduct().getName());
						}	
					}
				    
				  /*  if (orderProducts.length() < list.size() ) {
						Iterator <OrderItem> itemIter = list.iterator();
						while (itemIter.hasNext()) {
							OrderItem item = itemIter.next();
							f
						}
				    }
				    */
				    
				    
				    
				    Product product=productService.getProductByNameAndOrg(productname,organization);
				    int currentQuantity = product.getQuantity();
				   // if (currentQuantity < productQuantity) {
				   // 	errorProduct.add(product.getName());
				   // }
				    if (productQuantity < orderItem.getQuantity()){
				    	//reduce order qty
				    	int difference = orderItem.getQuantity() - productQuantity;
				    	product.setQuantity(product.getQuantity()+difference);
				    	productService.addProduct(product);
				    	System.out.println("increased qty of " + product.getName());
				    }
				    else if (productQuantity == orderItem.getQuantity()) {
				    	// Do nothing.. :P
				    	System.out.println("product qty is same:" + product.getName());
				    }
				    else if (currentQuantity >= (productQuantity- orderItem.getQuantity())) {
				    	//increase product order qty
				    	System.out.println("increased product order qty");
				    	int difference = productQuantity - orderItem.getQuantity();
				    	product.setQuantity(product.getQuantity() - difference);
				    	productService.addProduct(product);
				    }
				    else if (currentQuantity < (productQuantity - orderItem.getQuantity())) {
				    	errorProduct.add(product.getName()+" -Max: "+ (currentQuantity+ orderItem.getQuantity())+ " units");
				    }
				}
				// I don't want to modify working logic
				if (orderProducts.length() < list.size()) {
					Iterator <OrderItem> itemIter = list.iterator();
					while (itemIter.hasNext()) {
						OrderItem item = itemIter.next();
						Iterator <String> iterProd = updated.iterator();
						int flag =0;
						while (iterProd.hasNext()) {
							String updatedProd = iterProd.next();
							if (item.getProduct().getName().equals(updatedProd)) {
								flag =1;
							}
						}
						if (flag == 0) {
							Product prodUpdated = item.getProduct();
							prodUpdated.setQuantity(prodUpdated.getQuantity() + item.getQuantity());
							productService.addProduct(prodUpdated);
						}
					}			
				}
				if(!errorProduct.isEmpty()){
					throw new Exception();
				}
				}
			} catch (JSONException e2) {
				e2.printStackTrace();
			}
			catch(Exception e1) {
				e1.printStackTrace();
				try {
					response.put("status", "Failure");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				String error = new String();
				JSONArray errorArray = new JSONArray();
				Iterator <String> iterator = errorProduct.iterator();
				String output = "";
				int count =0;
				while(iterator.hasNext()) {
				//	error = error + iterator.next() +", ";
					//errorArray.put(iterator.next());
					if (count > 0)
						output += ", ";
					output = output + iterator.next();
					++count;
				}
				try {
					response.put("error",  "Insufficient stock for: "+output);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return response.toString();
			}
			float unitRates[] = new float [order.getOrderItems().size()];
			int count =0;
			
			List <OrderItem> oldOrdersList = order.getOrderItems();
			for( OrderItem orderitem : order.getOrderItems())
			{
				orderItemRepository.delete(orderitem);
			}
			List<OrderItem> orderItems= new ArrayList<OrderItem>();
			try {
				orderItemsJSON = jsonObject.getJSONArray("orderItems");
				for (int i = 0; i < orderItemsJSON.length(); i++) {
				    OrderItem orderItem= new OrderItem();
					JSONObject row = orderItemsJSON.getJSONObject(i);
				    String productName=row.getString("name");
				    int productQuantity =row.getInt("quantity");
				    System.out.println(productName + " ---- "+productQuantity);
				    Product product=productService.getProductByNameAndOrg(productName, organization);
				    orderItem.setProduct(product);
				    orderItem.setQuantity(productQuantity);	
				   float oldRate = 0;
				   Iterator <OrderItem> oldOrders = oldOrdersList.iterator();
				   while(oldOrders.hasNext()) {
					   OrderItem item = oldOrders.next();
					   if (item.getProduct().getName().equals(productName)) {
						   oldRate = item.getUnitRate();
					   }
				   }
				   orderItem.setUnitRate(oldRate);
				    orderItem.setOrder(order);
				    orderItem=orderItemRepository.save(orderItem);
				    orderItems.add(orderItem);
				}
				System.out.println(orderItemsJSON.toString());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			order.setOrderItems(orderItems);
		}
		String orderString = "";
		List <OrderItem> orderItemsNew = orderService.getOrder(orderId).getOrderItems();
		Iterator <OrderItem> itemsIterator = orderItemsNew.iterator();
		float netAmount=0;
		orderString +="<ol>";
		while (itemsIterator.hasNext()) {
			OrderItem orderItem = itemsIterator.next();
			orderString += "<li>";
			orderString +=orderItem.getProduct().getName()+" --- "+String.valueOf(orderItem.getQuantity())+" units<br />";
			orderString += "</li>";
			netAmount += (orderItem.getQuantity() * orderItem.getUnitRate());
		}
		orderString += "</ol>";
		try {
			// EditOrder
			response.put("status", "Success]");

			
			
			
			
			
			
			//JSONObject jsonResponseObject = new JSONObject();
		//	Organization organization =organizationService.getOrganizationByAbbreviation(orgabbr);
		//	List<Order> orderList = orderService.getOrderByOrganizationSavedSorted(organization);
		//	JSONArray orderArray = new JSONArray();
		//	Iterator<Order> iterator = orderList.iterator();
		//	Boolean stockManagement = organization.getStockManagement();
			//findByOrder_OrderId
			Order orderOne = orderRepository.findOne(orderId);
			List<OrderItem> oit = orderItemRepository.findByOrder_OrderId(orderId);
				JSONObject orderObject = new JSONObject();
				
				List<Integer> quantityList=new ArrayList<Integer>();
				JSONArray orderProducts = jsonObject.getJSONArray("orderItems");
				for (int i = 0; i < orderProducts.length(); i++) {
					 OrderItem orderItem = null;
					
					JSONObject row = orderProducts.getJSONObject(i);
				    
				    int productQuantity =row.getInt("quantity");
				    quantityList.add(productQuantity);
				    
				}
				
				
				Message message = messageService.getMessageFromOrder(order);
				if(message != null){
				try {
					orderObject.put("orderid",orderOne.getOrderId());
					orderObject.put("timestamp", message.getTime().toString());
					orderObject.put("username", userService.getUser(message.getUser().getUserId()).getName());
					orderObject.put("comment", message.getComments());
					List<OrderItem> OneOrder = orderOne.getOrderItems();
					int ordersCount=0;
					JSONArray items = new JSONArray();
					
					while(ordersCount<oit.size())
					{
						JSONObject orderObjects = new JSONObject();
					Product product = oit.get(ordersCount).getProduct();
					orderObjects.put("productname",(product.getName().toString()));
					orderObjects.put("quantity", quantityList.get(ordersCount).toString());
					orderObjects.put("rate", Float.toString(product.getUnitRate()));
					Product products = oit.get(ordersCount).getProduct();
					orderObjects.put("stockQuantity", Integer.toString(products.getQuantity()));
					items.put(orderObjects);
					ordersCount++;
					}	
					orderObject.put("items",items);
					response.put("order", orderObject);
					
				} catch (JSONException e) {
					e.printStackTrace();
				}
				}
			

			//return response.toString();
			
		

			
			
			
			
			
			
			
			
			
			
			
			
			
			
			order.setStatus(status);
	
			if (status.equals("cancelled")) {
				List<OrderItem> orderItems = orderService.getOrder(orderId).getOrderItems();
				Iterator <OrderItem> iterator = orderItems.iterator();
				while (iterator.hasNext()) {
					OrderItem orderItem = iterator.next();
					Product product = orderItem.getProduct();
					product.setQuantity(product.getQuantity() + orderItem.getQuantity());
					productService.addProduct(product);
					System.out.println("Updating product on cancellation - "+product.getName());
				}
			}
			if(status.equals("cancelled"))
			{
				SendMail.sendMail(order.getMessage().getUser().getEmail(), "Lokacart: Order Cancellation Acknowledgement", "<p>Dear User<br /><br />Your order with order ID "+order.getOrderId()+" has been successfully cancelled.<br />The items ordered were: <br /><br />"+orderString+"<br />We hope to serve you again.</p>");
				if(organization.getAbbreviation().equals("NatG"))
					SendMail.sendMail("vishalghodke@gmail.com", "Lokacart: Order Cancellation", "Dear Admin\n\nCustomer "+order.getMessage().getUser().getName()+" has cancelled the order of order ID "+order.getOrderId()+".<br />Thank you <br />Lokacart Team");
			}
			else {
				SendMail.sendMail(order.getMessage().getUser().getEmail(), "Lokacart: Order Modification Acknowledgement", "<p>Dear User<br /><br />Your order with order ID "+order.getOrderId()+" has been successfully modified on "+order.getMessage().getTime()+ ".<br />The items ordered were: </p><p style='text-align:center'>"+orderString+"</p><p><br />The total amount is: Rs."+String.format("%.2f", netAmount)+" .</p>");
				order.getMessage().setTime(new Timestamp((new Date()).getTime()));
				binaryMessageRepository.save((BinaryMessage)order.getMessage());
				if(organization.getAbbreviation().equals("NatG"))
					SendMail.sendMail("vishalghodke@gmail.com", "Lokacart: Order Modification", "Dear Admin ,<br /><br />Customer "+order.getMessage().getUser().getName()+" has modified the order of order ID "+order.getOrderId()+".<br />The items ordered were: </p><p style='text-align:center'>"+orderString+"</p><p><br />The total amount is: Rs."+String.format("%.2f", netAmount)+" .</p>");
			}
			orderRepository.save(order);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		List <String> androidTargets = getTargetDevices(organization);
		if(androidTargets.size() > 0) {
			GcmRequest gcmRequest = new GcmRequest();
	
			HashMap<String, Integer> dashData = null;
			try {
				dashData = dashBoardLocal(organizationabbr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			gcmRequest.broadcast(androidTargets,organizationabbr,dashData);			
			}
		return response.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/orders/updatestatus/{orderId}",method = RequestMethod.GET, produces="text/plain" )
	public String updateOrderStatus(@PathVariable int orderId)
	{
		System.out.println("In update status with order id = "+orderId);
		Order order = orderRepository.findOne(orderId);
		Organization organization= order.getOrganization();
		String organizationabbr = organization.getAbbreviation();
		if(order.getStatus().equals("processed"))
		{
			System.out.println("In processed");
			BillLayoutSettings billLayoutSetting = billLayoutSettingsRepository.findByOrganization(organization);
			SendBill.sendMail(order, organization, billLayoutSetting);
		}
		else if (order.getStatus().equals("saved"))
		{
			System.out.println("In saved");
			SendMail.sendMail(order.getMessage().getUser().getEmail(), "Lokacart: Order recieved", "<p>Dear User <br /><br />Organization "+organization.getName()+" has recieved your order. Order processing might take couple of days. Once your order is processed, you will receive an email confirming the same.<br /><br /> Thank you for shopping with us.</p>");
		}
		System.out.println("Mail sent");
		List <String> androidTargets = getTargetDevices(organization);
		if(androidTargets.size() > 0) {
			GcmRequest gcmRequest = new GcmRequest();

			HashMap<String, Integer> dashData = null;
			try {
				dashData = dashBoardLocal(organizationabbr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			gcmRequest.broadcast(androidTargets,organizationabbr,dashData);			}
		return "Success";
	}
	
	@Transactional
	@RequestMapping(value="/orders/get", method = RequestMethod.GET)
	public String displayAllOrders(@RequestParam(value="orgabbr") String orgabbr)
	{
		JSONObject jsonResponseObject = new JSONObject();
		JSONArray savedArray = new JSONArray();
		JSONArray processedArray = new JSONArray();
		JSONArray cancelledArray = new JSONArray();
		JSONArray deliveredArray = new JSONArray();

		Organization organization =organizationService.getOrganizationByAbbreviation(orgabbr);
		List<Order> orderList = orderService.getOrderByOrganizationSorted(organization);
		Iterator <Order> iterator = orderList.iterator();
		while(iterator.hasNext()) {
			Order order = iterator.next();
			Message message = messageService.getMessageFromOrder(order);
			JSONObject orderObject = new JSONObject();
			if(message != null){
				try {
					orderObject.put("orderid",order.getOrderId());
					orderObject.put("timestamp", message.getTime().toString());
					orderObject.put("username", userService.getUser(message.getUser().getUserId()).getName());
					orderObject.put("comment", message.getComments());
					if(order.getStatus().equals("saved")){
						savedArray.put(orderObject);
					}
					else if (order.getStatus().equals("processed")){
						processedArray.put(orderObject);
					}
					else if (order.getStatus().equals("cancelled")) {
						cancelledArray.put(orderObject);
					}
					else if (order.getStatus().equals("delivered")) {
						deliveredArray.put(orderObject);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				}
			
		}
		try{
		jsonResponseObject.put("saved", savedArray);
		jsonResponseObject.put("processed", processedArray);
		jsonResponseObject.put("cancelled", cancelledArray);
		jsonResponseObject.put("delivered", deliveredArray);
		jsonResponseObject.put("response", "success");
		}
		catch(JSONException e){
			e.printStackTrace();
			try {
				jsonResponseObject.put("response", "error");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return jsonResponseObject.toString();
		}
		return jsonResponseObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/orders/saved",method = RequestMethod.GET )
	public String displaySavedOrders(@RequestParam(value="orgabbr") String orgabbr)
	{
		JSONObject jsonResponseObject = new JSONObject();
		Organization organization =organizationService.getOrganizationByAbbreviation(orgabbr);
		List<Order> orderList = orderService.getOrderByOrganizationSavedSorted(organization);
		JSONArray orderArray = new JSONArray();
		Iterator<Order> iterator = orderList.iterator();
		Boolean stockManagement = organization.getStockManagement();
	
		while(iterator.hasNext())
		{
			JSONObject orderObject = new JSONObject();
			Order order = iterator.next();
			Message message = messageService.getMessageFromOrder(order);
			if(message != null){
			try {
				orderObject.put("orderid",order.getOrderId());
				orderObject.put("timestamp", message.getTime().toString());
				orderObject.put("username", userService.getUser(message.getUser().getUserId()).getName());
				orderObject.put("comment", message.getComments());
				JSONArray items = new JSONArray();
				List<OrderItem> orderItems = order.getOrderItems();
				Iterator<OrderItem> iter = orderItems.iterator();
				while(iter.hasNext()){
					OrderItem orderItem = iter.next();
					JSONObject item = new JSONObject();
					try {
				
						item.put("productname", orderItem.getProduct().getName());
						item.put("quantity", Integer.toString(orderItem.getQuantity()));
						Product product = orderItem.getProduct();
						item.put("stockQuantity", Integer.toString(product.getQuantity()));
						item.put("rate", Float.toString(orderItem.getUnitRate()));
						items.put(item);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					
				}
				orderObject.put("items", items);
				orderArray.put(orderObject);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			}
		}
		try {
			jsonResponseObject.put("orders",orderArray);
			jsonResponseObject.put("stockManagement", stockManagement.toString());
			jsonResponseObject.put("minimumBillOrder",organization.getOrderThreshold());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/orders/processed",method = RequestMethod.GET )
	public String displayProcessedOrders(@RequestParam(value="orgabbr") String orgabbr)
	{
		
		Organization organization =organizationService.getOrganizationByAbbreviation(orgabbr);
		List<Order> orderList = orderService.getOrderByOrganizationProcessedSorted(organization);
		JSONObject jsonResponseObject = new JSONObject();
		JSONArray orderArray = new JSONArray();
		Iterator<Order> iterator = orderList.iterator();
		while(iterator.hasNext())
		{
			JSONObject orderObject = new JSONObject();
			Order order = iterator.next();
			Message message = messageService.getMessageFromOrder(order);
			if(message != null){
			try {
				orderObject.put("orderid",order.getOrderId());
				orderObject.put("timestamp", message.getTime().toString());
				orderObject.put("username", userService.getUser(message.getUser().getUserId()).getName());
				orderObject.put("comment", message.getComments());
				JSONArray items = new JSONArray();
				List<OrderItem> orderItems = order.getOrderItems();
				Iterator<OrderItem> iter = orderItems.iterator();
				while(iter.hasNext()){
					OrderItem orderItem = iter.next();
					JSONObject item = new JSONObject();
					try {
						Product product = orderItem.getProduct();
						item.put("stockQuantity", Integer.toString(product.getQuantity()));
						item.put("productname", orderItem.getProduct().getName());
						item.put("quantity", Integer.toString(orderItem.getQuantity()));
						item.put("rate", Float.toString(orderItem.getUnitRate()));
						items.put(item);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					items.put(item);
				}
				orderObject.put("items", items);
				orderArray.put(orderObject);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			}
		}
		try {
			jsonResponseObject.put("orders",orderArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/orders/cancelled",method = RequestMethod.GET )
	public String displayCancelledOrders(@RequestParam(value="orgabbr") String orgabbr)
	{
		
		Organization organization =organizationService.getOrganizationByAbbreviation(orgabbr);
		

		
		
		List<Order> orderList = orderService.getOrderByOrganizationCancelledSorted(organization);
		JSONObject jsonResponseObject = new JSONObject();
		JSONArray orderArray = new JSONArray();
		Iterator<Order> iterator = orderList.iterator();
		while(iterator.hasNext())
		{
			JSONObject orderObject = new JSONObject();
			Order order = iterator.next();
			Message message = messageService.getMessageFromOrder(order);
			if(message != null){
			try {
				orderObject.put("orderid",order.getOrderId());
				orderObject.put("timestamp", message.getTime().toString());
				orderObject.put("username", userService.getUser(message.getUser().getUserId()).getName());
				orderObject.put("comment", message.getComments());
				JSONArray items = new JSONArray();
				List<OrdersStore> ordersStoreList = ordersStoreService.getStoredItems(order);
				Iterator <OrdersStore> ordersStoreIter = ordersStoreList.iterator();
				List<OrderItem> orderItems = order.getOrderItems();
				Iterator<OrderItem> iter = orderItems.iterator();
				while (ordersStoreIter.hasNext()) {
					OrdersStore ordersStore =  ordersStoreIter.next();
					JSONObject item = new JSONObject();
					try {
						item.put("productname", ordersStore.getProductName());
						item.put("quantity", Integer.toString(ordersStore.getQuantity()));
						item.put("rate", Float.toString(ordersStore.getUnitRate()));
						items.put(item);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					items.put(item);
					
					
				}
				
				
				/*	while(iter.hasNext()){
					OrderItem orderItem = iter.next();
					JSONObject item = new JSONObject();
					try {
						item.put("productname", orderItem.getProduct().getName());
						item.put("quantity", Integer.toString(orderItem.getQuantity()));
						item.put("rate", Float.toString(orderItem.getUnitRate()));
						items.put(item);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					items.put(item);
				} */
				orderObject.put("items", items);
				orderArray.put(orderObject);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			}
		}
		try {
			jsonResponseObject.put("orders",orderArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/orders/delivered",method = RequestMethod.GET )
	public String returnDeliveredOrders(@RequestParam(value="orgabbr") String orgabbr)
	{
		
		Organization organization =organizationService.getOrganizationByAbbreviation(orgabbr);
		List<Order> orderList = orderService.getOrderByOrganizatonDeliveredSorted(organization);
		JSONObject jsonResponseObject = new JSONObject();
		JSONArray orderArray = new JSONArray();
		Iterator<Order> iterator = orderList.iterator();
		while(iterator.hasNext())
		{
			JSONObject orderObject = new JSONObject();
			Order order = iterator.next();
			Message message = messageService.getMessageFromOrder(order);
			if(message != null){
			try {
				orderObject.put("orderid",order.getOrderId());
				orderObject.put("timestamp", message.getTime().toString());
				orderObject.put("username", userService.getUser(message.getUser().getUserId()).getName());
				orderObject.put("paid", String.valueOf(order.getIsPaid()));

				JSONArray items = new JSONArray();
				List<OrdersStore> ordersStoreList = ordersStoreService.getStoredItems(order);
				Iterator <OrdersStore> ordersStoreIter = ordersStoreList.iterator();
				List<OrderItem> orderItems = order.getOrderItems();
				Iterator<OrderItem> iter = orderItems.iterator();
				while (ordersStoreIter.hasNext()) {
					OrdersStore ordersStore =  ordersStoreIter.next();
					JSONObject item = new JSONObject();
					try {
						item.put("productname", ordersStore.getProductName());
						item.put("quantity", Integer.toString(ordersStore.getQuantity()));
						item.put("rate", Float.toString(ordersStore.getUnitRate()));
						items.put(item);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					items.put(item);
					
					
				}
				orderObject.put("items", items);
				orderArray.put(orderObject);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			}
		}
		try {
			jsonResponseObject.put("orders",orderArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	
	
	
	
	@Transactional
	@RequestMapping(value = "/orders/rejected",method = RequestMethod.GET )
	public String displayRejectedOrders(@RequestParam(value="orgabbr") String orgabbr)
	{
		
		Organization organization =organizationService.getOrganizationByAbbreviation(orgabbr);
		List<Order> orderList = orderService.getOrderByOrganizationRejectedSorted(organization);
		JSONObject jsonResponseObject = new JSONObject();
		JSONArray orderArray = new JSONArray();
		Iterator<Order> iterator = orderList.iterator();
		while(iterator.hasNext())
		{
			JSONObject orderObject = new JSONObject();
			Order order = iterator.next();
			Message message = messageService.getMessageFromOrder(order);
			if(message != null){
			try {
				orderObject.put("orderid",order.getOrderId());
				orderObject.put("timestamp", message.getTime().toString());
				orderObject.put("username", userService.getUser(message.getUser().getUserId()).getName());
				orderObject.put("comment", message.getComments());
				orderArray.put(orderObject);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			}
		}
		try {
			jsonResponseObject.put("orders",orderArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return jsonResponseObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/orders/changestate/processed/{orderId}",method = RequestMethod.GET )
	public String changeToProcessedState(@PathVariable int orderId) {
		System.out.println("Called processed");
		JSONObject responseJsonObject = new JSONObject();
		Order order = orderService.getOrder(orderId);
		if (order.getStatus().equals("processed")) {
			try {
			responseJsonObject.put("result", "failure");
			responseJsonObject.put("error", "order already processed");
			return responseJsonObject.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		else if (order.getStatus().equals("delivered")) {
			try {
			responseJsonObject.put("result", "failure");
			responseJsonObject.put("error", "order already delivered");
			return responseJsonObject.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		else if (order.getStatus().equals("cancelled")) {
			try {
			responseJsonObject.put("result", "failure");
			responseJsonObject.put("error", "order already cancelled");
			return responseJsonObject.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		int id = order.getOrderId();
		order.setStatus("processed");
		Organization organization = order.getOrganization();
		String organizationabbr = organization.getAbbreviation();
		String phonenumber = order.getMessage().getUser().getUserPhoneNumbers().get(0).getPhoneNumber();
		System.out.println("Phonenumber: "+phonenumber);
		try {
		orderService.processOrder(order);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			try {
				return responseJsonObject.put("result", "failure").toString();
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
	try {
		BillLayoutSettings billLayoutSetting = billLayoutSettingsRepository.findByOrganization(organization);
		SendBill.sendMail(order, organization, billLayoutSetting);
		System.out.println("mail sent");
		responseJsonObject.put("result", "success");
	} catch (JSONException e) {
		e.printStackTrace();
	}	
	List <String> androidTargets = getTargetDevices(organization);
	if(androidTargets.size() > 0) {
		GcmRequest gcmRequest = new GcmRequest();
		HashMap<String, Integer> dashData = null;
		try {
			dashData = dashBoardLocal(organizationabbr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		gcmRequest.broadcast(androidTargets,organizationabbr,dashData);		}
	
	List <String> androidConsumerTargets = getTargetConsumerDevices(phonenumber);
	if (androidConsumerTargets.size() > 0) {
		System.out.println("Broadcasts sent");
		GcmRequest gcmRequest = new GcmRequest();
		gcmRequest.consumerBroadcast(androidConsumerTargets, String.valueOf(id));
	}
	return responseJsonObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/orders/changestate/cancelled/{orderId}",method = RequestMethod.GET )
	public String changeToCancelledState(@PathVariable int orderId) {
		JSONObject responseJsonObject = new JSONObject();
		Order order = orderService.getOrder(orderId);
		order.setStatus("cancelled");
		Organization organization = order.getOrganization();
		String organizationabbr = organization.getAbbreviation();
		List <OrderItem> items = order.getOrderItems();
		Iterator <OrderItem> iterator = items.iterator();
		if(organization.getStockManagement()) {
		while(iterator.hasNext()) {
			OrderItem orderItem = iterator.next();
			Product product = orderItem.getProduct();
			int quantity = orderItem.getQuantity();
			int prodQty = product.getQuantity();
			product.setQuantity(quantity + prodQty);
			productService.addProduct(product);
		}
		}
		try {
		orderService.cancelOrder(order);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			try {
				return responseJsonObject.put("result", "failure").toString();
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
	try {
		SendMail.sendMail(order.getMessage().getUser().getEmail(), "Lokacart: Order Cancelled", "<p>Dear User <br /><br />Your order with "+organization.getName()+" has been cancelled.\n\nWe are looking forward to your next order. </p>");
		responseJsonObject.put("result", "success");
	} catch (JSONException e) {
		e.printStackTrace();
	}	
	List <String> androidTargets = getTargetDevices(organization);
	if(androidTargets.size() > 0) {
		GcmRequest gcmRequest = new GcmRequest();
	
		HashMap<String, Integer> dashData = null;
		try {
			dashData = dashBoardLocal(organizationabbr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		gcmRequest.broadcast(androidTargets,organizationabbr,dashData);
		}
	return responseJsonObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/orders/changestate/rejected/{orderId}",method = RequestMethod.GET )
	public String changeToRejectedState(@PathVariable int orderId) {
		JSONObject responseJsonObject = new JSONObject();
		Order order = orderService.getOrder(orderId);
		order.setStatus("rejected");
		Organization organization = order.getOrganization();
		String organizationabbr = organization.getAbbreviation();
		List <OrderItem> items = order.getOrderItems();
		Iterator <OrderItem> iterator = items.iterator();
		if(organization.getStockManagement()) {
		while(iterator.hasNext()) {
			OrderItem orderItem = iterator.next();
			Product product = orderItem.getProduct();
			int quantity = orderItem.getQuantity();
			int prodQty = product.getQuantity();
			product.setQuantity(quantity + prodQty);
			productService.addProduct(product);
		}
		}
		try {
		orderService.rejectOrder(order);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			try {
				return responseJsonObject.put("result", "failure").toString();
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
	try {
		responseJsonObject.put("result", "success");
	} catch (JSONException e) {
		e.printStackTrace();
	}	
	
	List <String> androidTargets = getTargetDevices(organization);
	if(androidTargets.size() > 0) {
	GcmRequest gcmRequest = new GcmRequest();
	HashMap<String, Integer> dashData = null;
	try {
		dashData = dashBoardLocal(organizationabbr);
	} catch (ParseException e) {
		e.printStackTrace();
	}
	gcmRequest.broadcast(androidTargets,organizationabbr,dashData);
	}
	return responseJsonObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/orders/details/{orderId}",method = RequestMethod.GET )
	public String getOrderDetails(@PathVariable int orderId) {
		JSONObject responseJsonObject = new JSONObject();
		JSONArray items = new JSONArray();
		Order order = orderService.getOrder(orderId);
		List<OrderItem> orderItems = order.getOrderItems();
		Iterator<OrderItem> iterator = orderItems.iterator();
		while(iterator.hasNext()) {
			OrderItem orderItem = iterator.next(); 
			JSONObject item = new JSONObject();
			try {
				item.put("productname", orderItem.getProduct().getName());
				item.put("quantity", Float.toString(orderItem.getQuantity()));
				item.put("rate", Float.toString(orderItem.getUnitRate()));
				items.put(item);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		try {
			responseJsonObject.put("items", items);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/presetquantities",method = RequestMethod.GET )
	public String getPresetQuantity(@RequestParam (value="orgabbr") String orgabbr) {
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		JSONObject jsonResponseObject = new JSONObject();
		JSONArray array = new JSONArray();
		List <PresetQuantity> presetQty = presetQuantityService.getPresetQuantityList(organization);
		Iterator <PresetQuantity> iterator = presetQty.iterator();
		while(iterator.hasNext()) {
			JSONObject qty = new JSONObject();
			PresetQuantity presetQuantity = iterator.next();
			try {
				qty.put("producttype",presetQuantity.getProductType().getName());
				qty.put("qty", presetQuantity.getQuantity());
				array.put(qty);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		try {
			jsonResponseObject.put("presetquantity", array);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
		
	}
	
	//Not worrying about voice now.. Must fix later. Time is short. 
	@Transactional
	@RequestMapping(value = "/savedorders",method = RequestMethod.GET )
	public String getSavedOrders(@RequestParam (value = "abbr")String abbr, @RequestParam(value = "phonenumber") String phonenumber) {
		Organization organization = organizationService.getOrganizationByAbbreviation(abbr);
		User user = userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser();

		JSONObject responseJsonObject = new JSONObject();
		JSONArray orderArray = new JSONArray();
		List <Message> messages = messageService.getMessagesForUser(user, "order", "binary");
		Iterator <Message> iterator = messages.iterator();
		while (iterator.hasNext()) {
			Message message = iterator.next();
			Order order = message.getOrder();
			if (order.getStatus().equals("saved") && (order.getOrganization() == organization)) {
				JSONObject object = new JSONObject();
				try {
					object.put("stockManagement",organization.getStockManagement());
					object.put("orderId", order.getOrderId());
					object.put("status",order.getStatus());
					
					DateFormat df = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss.SSS");
					df.setTimeZone(TimeZone.getTimeZone("IST"));
					String formattedDate8601 = df.format(message.getTime());
					//Cheap hack
					object.put("registeredTime", formattedDate8601+"+0000");

					List <OrderItem> items = order.getOrderItems();
					Iterator <OrderItem> orderIterator = items.iterator();
					JSONArray itemArray = new JSONArray();
					while (orderIterator.hasNext()) {
						JSONObject orderDetails = new JSONObject();
						OrderItem orderItem = orderIterator.next();
						orderDetails.put("productId", String.valueOf(orderItem.getProduct().getProductId()));
						orderDetails.put("quantity", String.valueOf(orderItem.getQuantity()));
						orderDetails.put("productname", orderItem.getProduct().getName());
						orderDetails.put("stockquantity",String.valueOf(orderItem.getProduct().getQuantity()));
						orderDetails.put("unitrate", String.valueOf(orderItem.getUnitRate()));
						itemArray.put(orderDetails);
					}
					object.put("orderItems", itemArray);
					orderArray.put(object);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			try {
				responseJsonObject.put("orders", orderArray);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} 		
		return responseJsonObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/processedorders",method = RequestMethod.GET )
	public String getProcessedOrders(@RequestParam (value = "abbr")String abbr, @RequestParam(value = "phonenumber") String phonenumber) {
		Organization organization = organizationService.getOrganizationByAbbreviation(abbr);
		User user = userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser();

		JSONObject responseJsonObject = new JSONObject();
		JSONArray orderArray = new JSONArray();
		List <Message> messages = messageService.getMessagesForUser(user, "order", "binary");
		Iterator <Message> iterator = messages.iterator();
		while (iterator.hasNext()) {
			Message message = iterator.next();
			Order order = message.getOrder();
			if (order.getStatus().equals("processed") && (order.getOrganization() == organization)) {
				JSONObject object = new JSONObject();
				try {
					object.put("stockManagement",organization.getStockManagement());
					object.put("orderId", order.getOrderId());
					object.put("status",order.getStatus());
					DateFormat df = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss.SSS");
					df.setTimeZone(TimeZone.getTimeZone("IST"));
					String formattedDate8601 = df.format(message.getTime());
					//Cheap hack
					object.put("registeredTime", formattedDate8601+"+0000");		
					List <OrderItem> items = order.getOrderItems();
					Iterator <OrderItem> orderIterator = items.iterator();
					JSONArray itemArray = new JSONArray();
					while (orderIterator.hasNext()) {
						JSONObject orderDetails = new JSONObject();
						OrderItem orderItem = orderIterator.next();
						orderDetails.put("productId", String.valueOf(orderItem.getProduct().getProductId()));
						orderDetails.put("quantity", String.valueOf(orderItem.getQuantity()));
						orderDetails.put("productname", orderItem.getProduct().getName());
						orderDetails.put("stockquantity",String.valueOf(orderItem.getProduct().getQuantity()));
						orderDetails.put("unitrate", String.valueOf(orderItem.getUnitRate()));
						itemArray.put(orderDetails);
					}
					object.put("orderItems", itemArray);
					orderArray.put(object);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			try {
				responseJsonObject.put("orders", orderArray);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} 		
		return responseJsonObject.toString();
	}
	
	
	@RequestMapping(value = "/deliveredorders",method = RequestMethod.GET )
	public String getDeliveredOrders(@RequestParam (value = "abbr")String abbr, @RequestParam(value = "phonenumber") String phonenumber) {
		Organization organization = organizationService.getOrganizationByAbbreviation(abbr);
		User user = userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser();

		JSONObject responseJsonObject = new JSONObject();
		JSONArray orderArray = new JSONArray();
		List <Message> messages = messageService.getMessagesForUser(user, "order", "binary");
		Iterator <Message> iterator = messages.iterator();
		while (iterator.hasNext()) {
			Message message = iterator.next();
			Order order = message.getOrder();
			if (order.getStatus().equals("delivered") && (order.getOrganization() == organization)) {
				JSONObject object = new JSONObject();
				try {
					object.put("stockManagement",organization.getStockManagement());
					object.put("orderId", order.getOrderId());
					object.put("status",order.getStatus());
					object.put("payment", String.valueOf(order.getIsPaid()));
					DateFormat df = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss.SSS");
					df.setTimeZone(TimeZone.getTimeZone("IST"));
					String formattedDate8601 = df.format(message.getTime());
					//Cheap hack
					object.put("registeredTime", formattedDate8601+"+0000");
					JSONArray items = new JSONArray();
					List<OrdersStore> ordersStoreList = ordersStoreService.getStoredItems(order);
					Iterator <OrdersStore> ordersStoreIter = ordersStoreList.iterator();
					List<OrderItem> orderItems = order.getOrderItems();
					Iterator<OrderItem> iter = orderItems.iterator();
					while (ordersStoreIter.hasNext()) {
						OrdersStore ordersStore =  ordersStoreIter.next();
						JSONObject item = new JSONObject();
						try {
							item.put("productname", ordersStore.getProductName());
							item.put("quantity", Integer.toString(ordersStore.getQuantity()));
							item.put("unitrate", Float.toString(ordersStore.getUnitRate()));
							items.put(item);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						items.put(item);	
					}
					object.put("orderItems", items);
					orderArray.put(object);
				}
			catch(JSONException e) {
				e.printStackTrace();
			}
			}
		}
			try {
				responseJsonObject.put("orders", orderArray);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
		return responseJsonObject.toString();
	
	}
	
	
	
	
	
	@Transactional
	@RequestMapping(value = "/order/delete",method = RequestMethod.GET )
	public String deleteOrderItem(@RequestParam(value="orderId") int orderId)
	{
		JSONObject jsonResponseObject = new JSONObject();
		List<OrderItem> orderitem= orderItemRepository.findByOrder_OrderId(orderId);
		Order copyOrder;
		Order order = orderService.getOrder(orderId);
		Message mes = messageService.getMessageFromOrder(order);
		messageService.removeMessage(mes);
		//orderService.removeOrder(order);
		//orderRepository.delete(order);
		int count=0;
		while(count<orderitem.size())
		{
		orderItemRepository.delete(orderitem.get(count++));
		}
		//List<Order> orderList = orderService.getOrderByOrganizationSavedSorted(organization);
		try {
			jsonResponseObject.put("Status", "Success");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return jsonResponseObject.toString();
	}
	@Transactional
	@RequestMapping(value = "/order/deletemsg",method = RequestMethod.GET )
	public String deleteOrder(@RequestParam(value="orderId") int orderId)
	{
		JSONObject jsonResponseObject = new JSONObject();
		
		Order order = orderService.getOrder(orderId);
		
		orderService.removeOrder(order);
		
		return jsonResponseObject.toString();
	}
	
	
	
	
	
	
	
	
	
	//Helper to resolve quantity issue.
	@Transactional
	@RequestMapping(value = "/fixqty",method = RequestMethod.GET )
	public @ResponseBody String fixQuantity() {
		List<OrderItem> items = orderItemRepository.findAll();
		Iterator <OrderItem> iterator = items.iterator();
		JSONObject responseJsonObject = new JSONObject();
		while (iterator.hasNext()) {
			OrderItem orderItem = iterator.next();
		//	orderItemRepository.delete(orderItem);
			int qty =(int) Math.ceil(orderItem.getQuantity());
			orderItem.setQuantity(qty);
			System.out.println(orderItem.getQuantity());
			orderItemRepository.save(orderItem);
		}
		try {
			responseJsonObject.put("response", "success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	
}