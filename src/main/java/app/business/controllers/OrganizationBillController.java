package app.business.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import app.business.services.BillLayoutSettingsService;
import app.business.services.GroupService;
import app.business.services.OrderService;
import app.business.services.OrganizationService;
import app.business.services.UserPhoneNumberService;
import app.data.repositories.UserRepository;
import app.entities.BillLayoutSettings;
import app.entities.Group;
import app.entities.Order;
import app.entities.OrderItem;
import app.entities.Organization;
import app.entities.User;
import app.entities.UserPhoneNumber;
import app.entities.message.Message;

@Controller
@RequestMapping("/web/{org}")
public class OrganizationBillController {

	@Autowired
	UserRepository userrepository;
	
	@Autowired
	OrderService orderservice;
	
	@Autowired
	OrganizationService organizationservice;
	
	@Autowired
	BillLayoutSettingsService billLayoutSettingsService;
	
	@Autowired
	GroupService groupservice;
	
	@Autowired
	UserPhoneNumberService userPhoneNumberService;
	
public static  class OrderWithPhoneNumber {
		
		private String phone;
		private Order order;
		private String timeInHoursAndMinutes;




	public OrderWithPhoneNumber(Order order, String phone,String timeInHoursAndMinutes)
	{
		
		this.order = order;
		this.phone = phone;
		this.timeInHoursAndMinutes = timeInHoursAndMinutes;
	}

	public Order getOrder()
	{
		return order;
	}

	public String getPhone()
	{
		return phone;
	}
	
	public String getTimeInHoursAndMinutes()
	{
		return timeInHoursAndMinutes;
	}
	
	public void setTimeInHoursAndMinutes(String timeInHoursAndMinutes)
	{
		this.timeInHoursAndMinutes = timeInHoursAndMinutes;
	}
	
	public void setOrder(Order order)
	{
		this.order = order;
	}

	public void setPhone(String phone)
	{
		this.phone = phone;
	}
	
	}

	@RequestMapping(value="/generateOrganizationBill")
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@Transactional
	public String generateBill(@PathVariable String org, Model model) {
		List<Order> orderList = orderservice.getOrderByOrganizationProcessed(organizationservice.getOrganizationByAbbreviation(org));
		//Removing orders with No message associated to it.
		List<Order> nullOrders=new ArrayList<Order>();
		List<Float> totalCost= new ArrayList<Float>();
 		for(Order order:orderList){
			if(order.getMessage()==null)
				nullOrders.add(order);
			else
			{
				for(OrderItem orderItem : order.getOrderItems()) {
					totalCost.add(orderItem.getUnitRate() * orderItem.getQuantity());
				}
			}
		}
		orderList.removeAll(nullOrders);
		
		BillLayoutSettings billLayoutSetting = billLayoutSettingsService.getBillLayoutSettingsByOrganization(organizationservice.getOrganizationByAbbreviation(org));
		model.addAttribute("billLayout",billLayoutSetting);
		List<Integer> indexes= new ArrayList<Integer>(orderList.size());
		for(int index=0;index<orderList.size();index++)
		{
			indexes.add(index);
		}
		model.addAttribute("total",totalCost);
		model.addAttribute("orders",orderList);
		return "generateBillMultiple";
	}
	
	@RequestMapping(value="/generateBillGroup/{groupId}")
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@Transactional
	public String generateBillGroup(@PathVariable String org, @PathVariable Integer groupId,Model model) {
		
		User user;
		UserPhoneNumber userPhoneNumber;
		Message message;
		List<Order> orderList = orderservice.getOrderByGroupProcessed(groupservice.getGroup(groupId));
		//Removing orders with No message associated to it.
		List<Order> nullOrders=new ArrayList<Order>();
		List<Float> totalCost= new ArrayList<Float>(0);
 		for(Order order:orderList){
			if(order.getMessage()==null)
				nullOrders.add(order);
			else
			{
				float sum=0;
				for(OrderItem orderItem : order.getOrderItems()) {
					sum+=orderItem.getUnitRate() * orderItem.getQuantity();
				}
				totalCost.add(sum);
			}
		}
		orderList.removeAll(nullOrders);
		
		BillLayoutSettings billLayoutSetting = billLayoutSettingsService.getBillLayoutSettingsByOrganization(organizationservice.getOrganizationByAbbreviation(org));
		model.addAttribute("billLayout",billLayoutSetting);
		List<Integer> indexes= new ArrayList<Integer>(orderList.size());
		for(int index=0;index<orderList.size();index++)
		{
			indexes.add(index);
		}
		List<OrderWithPhoneNumber> orderWithPhoneNumberList = new ArrayList<OrderWithPhoneNumber>(orderList.size());
		Iterator<Order> iterator = orderList.iterator();
	
		while(iterator.hasNext()) {
		   Order order = iterator.next();
		   message = order.getMessage();
		   user = message.getUser();
		   userPhoneNumber = userPhoneNumberService.getUserPrimaryPhoneNumber(user);
		   String timeInHoursAndMinutes = order.getMessage().getTime().toString().substring(0, 16);
		   OrderWithPhoneNumber orderWithPhoneNumber = new  OrderWithPhoneNumber(order,userPhoneNumber.getPhoneNumber(),timeInHoursAndMinutes);
		   orderWithPhoneNumberList.add(orderWithPhoneNumber);
				
		}	
	
		model.addAttribute("total",totalCost);
		model.addAttribute("orderWithPhoneNumbers",orderWithPhoneNumberList);
		return "generateBillMultiple";
	}
	@RequestMapping(value="/generateBill")	
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@Transactional
	public String generateBillPage(@PathVariable String org,Model model) {
		Organization organization=organizationservice.getOrganizationByAbbreviation(org);
		List<Group> groupList = organization.getGroups();
		model.addAttribute("groups", groupList);
		return "generateBillLandingPage";
	}
	
}
