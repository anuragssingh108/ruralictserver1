package app.business.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.data.repositories.OrdersStoreRepository;
import app.entities.Order;
import app.entities.OrdersStore;

@Service
public class OrdersStoreService {

	@Autowired 
	OrdersStoreRepository ordersStoreRepository;
	
	public void addOrdersStore(OrdersStore ordersStore) {
		ordersStoreRepository.save(ordersStore);
	}
	
	public List<OrdersStore> getStoredItems(Order order) {
		return ordersStoreRepository.findByOrder(order);
	}
	
}
