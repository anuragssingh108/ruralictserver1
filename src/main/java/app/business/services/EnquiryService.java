package app.business.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.data.repositories.EnquiryRepository;
import app.entities.Enquiry;

@Service
public class EnquiryService {
	@Autowired
	EnquiryRepository enquiryRepository;
	
	public void addEnquiry(Enquiry enquiry) {
		enquiryRepository.save(enquiry);
	}
	
	public Enquiry getEnquiry(int enquiryId) {
		return enquiryRepository.findOne(enquiryId);
	}

}
