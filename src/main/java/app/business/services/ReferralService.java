package app.business.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.data.repositories.ReferralRepository;
import app.entities.Organization;
import app.entities.Referral;

@Service

public class ReferralService {

	@Autowired
	ReferralRepository referralRepository;
	
	
	public Referral getByReferralCode(String referralCode){
		return referralRepository.findByReferralCode(referralCode);
	}
	
	public void addReferral(Referral referral){
		referralRepository.save(referral);
	}
	
	public List <Referral> getRemoveReferralsList(String phonenumber, Organization organization) {
		return referralRepository.findByPhonenumberAndOrganization(phonenumber, organization);
	}
	
	public void removeReferral(Referral referral) {
		referralRepository.delete(referral);
	}
}