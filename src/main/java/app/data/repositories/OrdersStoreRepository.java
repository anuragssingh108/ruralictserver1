package app.data.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import app.entities.Order;
import app.entities.OrdersStore;

public interface OrdersStoreRepository extends JpaRepository<OrdersStore, Integer> {
	/*
	 * Default functions
	 */
	@Override
	public OrdersStore findOne(Integer id);
	
	public List<OrdersStore> findByOrder(Order order);

}
