package app.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import app.entities.Organization;
import app.entities.SmsApiKeys;

public interface SmsApiKeysRepository extends JpaRepository<SmsApiKeys, Integer> {

	@Override
	public SmsApiKeys findOne(Integer id);
	
	public SmsApiKeys findByOrganization(Organization organization);
}