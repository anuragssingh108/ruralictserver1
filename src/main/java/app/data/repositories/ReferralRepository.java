package app.data.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import app.entities.Organization;
import app.entities.Referral;

public interface ReferralRepository extends JpaRepository<Referral, Integer> {
	@Override
	public Referral findOne(Integer id);

	public Referral findByReferralCode(String referralCode);
	
	public List<Referral> findByPhonenumberAndOrganization(String phonenumber, Organization organization);

}