package app.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import app.entities.Enquiry;

public interface EnquiryRepository extends JpaRepository<Enquiry, Integer> {
	
	@Override
	public Enquiry findOne(Integer id);
	
	
	
}
