package app.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="orders_store")
public class OrdersStore implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="store_id")
	private int storeId;
	
	@Column(name = "quantity")
	private int quantity;
	
	@Column(name = "unit_rate")
	private float unitRate;
	
	@Column(name = "product_name")
	private String productName;
	
	@ManyToOne
	@JoinColumn(name="order_id")
	private Order order;
	
	public OrdersStore() {
		
	}
	
	public OrdersStore(int quantity, float unitRate, String productName, Order order) {
		this.quantity = quantity;
		this.unitRate =  unitRate;
		this.productName = productName;
		this.order = order;
	}
	
	public void setStoreId(int cancelId) {
		this.storeId = cancelId;
	}
	
	public int getStoreId() {
		return this.storeId;
	}
	
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public int getQuantity() {
		return this.quantity;
	}
	
	
	public void setUnitRate(float unitRate) {
		this.unitRate = unitRate;
	}
	
	public float getUnitRate() {
		return this.unitRate;
	}
	
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public String getProductName() {
		return this.productName;
	}
	
	public void setOrder(Order order) {
		this.order = order;
	}
	
	public Order getOrder() {
		return this.order;
	}
	

}
