package app.jeff.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import app.business.services.OrganizationMembershipService;
import app.business.services.OrganizationService;
import app.business.services.ProductService;
import app.business.services.ProductTypeService;
import app.business.services.UserService;
import app.data.repositories.OrganizationRepository;
import app.entities.Organization;
import app.entities.Product;
import app.entities.ProductType;
import app.entities.User;

@RestController
@RequestMapping("/api/jeff")
public class SMSBroadcast {
	
	@Autowired
	OrganizationService organizationService;
	@Autowired
	ProductTypeService productTypeService;
	@Autowired
	UserService userService;
	@Autowired
	OrganizationRepository organizationRepository;
	@Autowired
	ProductService productService;
	
	@Autowired
	OrganizationMembershipService organizationMembershipService;
	
	
	
	@Transactional
	@RequestMapping(value = "/name",method = RequestMethod.GET )
	public String productListByTypeNew(@RequestParam(value="orgabbr") String orgabbr){
		
		JSONObject responseJsonObject = new JSONObject();
		JSONArray details = new JSONArray();
		Long some =organizationRepository.countByAbbreviation(orgabbr);
		
	
			try {
				responseJsonObject.put("status", "Success");	
				responseJsonObject.put("count", some);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		
			return responseJsonObject.toString();  
	}
	
	
	
	
	
	
}
