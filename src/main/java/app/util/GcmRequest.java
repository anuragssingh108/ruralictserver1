package app.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;

public class GcmRequest {

	public void broadcast(String userMessage, String title, List<String> androidTargets, int... params) {
		final long serialVersionUID = 1L;
		// List<String> androidTargets = new ArrayList<String>();
		// final String SENDER_ID = "AIzaSyBsUr9b5VT5_oH_0FSygF8us4AXKA1yvGw";
		System.out.println("called");
		final String SENDER_ID = "AIzaSyCij7ogLIlqqAGLLKDrvlgILaimYh1-3KU";
		Sender sender = new Sender(SENDER_ID);
		Message message = null;
		if (params.length == 1) {
			System.out.println("Order notif sent");
			message = new Message.Builder()
					// .timeToLive(30)
					.delayWhileIdle(false).addData("message", userMessage).addData("title", title)
					.addData("id", Integer.toString(params[0])).build();
		} else if (params.length == 2) {
			System.out.println("Memeber notif sent");
			message = new Message.Builder()
					// .timeToLive(30)
					.delayWhileIdle(false).addData("message", userMessage).addData("title", title)
					.addData("id", Integer.toString(params[0])).addData("userId", Integer.toString(params[1])).build();

		}
		

		try {
			System.out.println("In try");
			MulticastResult result = sender.send(message, androidTargets, 1);
			System.out.println("cleared");
			if (result.getResults() != null) {
				int canonicalRegId = result.getCanonicalIds();
				if (canonicalRegId != 0) {

				}
			} else {
				int error = result.getFailure();
				System.out.println("Broadcast failure: " + error);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Done");
	}

	public void broadcast(List<String> androidTargets, String orgabbr, HashMap <String, Integer> dashData) {
		final String SENDER_ID = "AIzaSyCij7ogLIlqqAGLLKDrvlgILaimYh1-3KU";
		Sender sender = new Sender(SENDER_ID);
		Message message = null;
		System.out.println("sending dash data");
//		HashMap<String, Integer> dashData = null;
//		try {
//
//			DashboardRestController dashboardRestController = new DashboardRestController();
//			dashData = dashboardRestController.dashBoard(orgabbr);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		System.out.println((dashData.get("newUsersToday").toString()) +" "+ (dashData.get("pendingUsers").toString()) +" "+ (dashData.get("totalUsers").toString()));
		message = new Message.Builder()
				// .timeToLive(30)
				.delayWhileIdle(false)
				.addData("id", "2")
				.addData("processed",(dashData.get("processed").toString()))
				.addData("cancelled", (dashData.get("cancelled").toString()))
				.addData("saved", (dashData.get("saved").toString()))
				.addData("newUsersToday", (dashData.get("newUsersToday").toString()))
				.addData("pendingUsers", (dashData.get("pendingUsers").toString()))
				.addData("totalUsers", (dashData.get("totalUsers").toString()))
				.collapseKey("2")
				.build();
		try {
			System.out.println("In try");
			MulticastResult result = sender.send(message, androidTargets, 1);
			System.out.println("cleared");
			if (result.getResults() != null) {
				int canonicalRegId = result.getCanonicalIds();
				if (canonicalRegId != 0) {

				}
			} else {
				int error = result.getFailure();
				System.out.println("Broadcast failure: " + error);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public void consumerBroadcast(List<String> androidTargets, String id) {
		final String SENDER_ID = "AIzaSyA8CNw7-TYvW6TN2lyrH4W59Ry3jkuUjiA";
		Sender sender = new Sender(SENDER_ID);
		Message message = null;
		message = new Message.Builder()
				.delayWhileIdle(false)
				.addData("id", "4")
				.addData("Message", "Order ID: "+id+" has been processed.")
				.addData("Title", "Order processed")
				.build();
		try {
			Iterator <String> iterator = androidTargets.iterator();
			while(iterator.hasNext()) {
				System.out.println("token: "+iterator.next());
			}
			MulticastResult result = sender.send(message, androidTargets, 1);
			System.out.println("cleared");
			if (result.getResults() != null) {
				int canonicalRegId = result.getCanonicalIds();
				if (canonicalRegId != 0) {

				}
			} else {
				int error = result.getFailure();
				System.out.println("Broadcast failure: " + error);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void consumerPromoBroadcast(String userMessage, String title, List<String> androidTargets, String id) {
		final String SENDER_ID = "AIzaSyA8CNw7-TYvW6TN2lyrH4W59Ry3jkuUjiA";
		Sender sender = new Sender(SENDER_ID);
		Message message = null;
		message = new Message.Builder()
				.delayWhileIdle(false).addData("Message", userMessage).addData("Title", title)
				.addData("id", id).build();
		System.out.println("message GCM: "+userMessage+ "\nTitle: "+title);
		try {
			MulticastResult result = sender.send(message, androidTargets, 1);
			if (result.getResults() != null) {
				int canonicalRegId = result.getCanonicalIds();
				if (canonicalRegId != 0) {

				}
			} else {
				int error = result.getFailure();
				System.out.println("Broadcast failure: " + error);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Done");
	}

}
