package app.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

class StreamGobbler extends Thread {
	InputStream is;
	String type;

	StreamGobbler(InputStream is, String type) {
		this.is = is;
		this.type = type;
	}

	public void run() {
		try {
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			while ((line = br.readLine()) != null)
				System.out.println(type + ">" + line);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
}

public class FFMPEGWrapper {

	String pathExecutable;
	public FFMPEGWrapper(String ffmpegPath) {
		String osname = System.getProperty("os.name");
		System.out.println(osname);
		
		System.out.println("Setting to Linux");
		pathExecutable = ffmpegPath;
		System.out.println(pathExecutable);
		
	}
	
	public boolean convertMp3ToWav(String mp3Path, String wavPath) {
		String[] command = new String[] { pathExecutable, "-i", mp3Path,
				"-acodec", "pcm_s16le", "-ac", "1", "-ar", "16000", wavPath };
		File file = new File(wavPath);
		if (file.exists())
			file.delete();
		Runtime run = Runtime.getRuntime();
		Process pr;
		try {
			pr = run.exec(command);
			pr.waitFor();
			StreamGobbler errorGobbler = new StreamGobbler(pr.getErrorStream(),
					"ERROR");
			StreamGobbler outputGobbler = new StreamGobbler(
					pr.getInputStream(), "OUTPUT");
			errorGobbler.start();
			outputGobbler.start();
			int exitVal = pr.waitFor();
			System.out.println("ExitValue: " + exitVal);
			System.out.println("Wait for has ended ");
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	
}
